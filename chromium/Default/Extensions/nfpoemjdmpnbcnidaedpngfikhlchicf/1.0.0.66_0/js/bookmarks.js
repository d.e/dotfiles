var main_port;
var treeView;
var listView;
var temp_saved_data;
var moved_node_id;
var draged_el;
var dragging = false;
var droped_pos = '';
var sidebar = true;
var dontSaveState = false;

function localize() {
	document.title = Local["bookmarkPageTitle"];
	$('header h1').text(Local["bookmarkPageTitle"]);
	$('header #term').attr('placeholder', Local["bookmarksSearch"]);
}

function checkUrl(url) {
	if (/^(https?|ftp|file|ws|wss|chrome):\/\//.test(url))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function open_link(data) {
	if (data.new_window)
	{
		chrome.windows.create({
			url: data.url
		});
	}
	else if (data.incognito)
	{
		chrome.windows.create({
			url: data.url,
			incognito: true
		});
	}
	else
	{
		chrome.tabs.create({url: data.url});
	}
}

function getCurationCounts(selectedId) {
	var folders_count = 0;
	var links_in_selected_folder = 0;
	function loop(node_id)
	{
		var node = treeView.get_node_by_id(node_id);
		if (!node) return;
		if (node.type == Types.curation_folder.id)
		{
			folders_count++;
		}
		else if (selectedId && node.type == Types.curation_link.id && node.original.parentId == selectedId)
		{
			links_in_selected_folder++;
		}
		if (node.children) for (var i = 0; i < node.children.length; i++)
		{
			loop(node.children[i]);
		}
	}
	
	loop(CurationFolders.curation_toolbar_id);
	return {
		folders_count: folders_count,
		links_in_selected_folder: links_in_selected_folder
	};
}

function checkHash() {
	var hash = document.location.hash;
	if (hash.length)
	{
		hash = hash.substring(1);
	}
	if (hash)
	{
		try
		{
			hash = hash.split('|');
			switch (hash[0])
			{
				case 'change':
					history.replaceState(null, '', '');
					var node = hash[1];
					node = treeView.get_node_by_id(node);
					view_controller.change(node);
					break;
				case 'select':
					var node = hash[1];
					var select_in_view = node.split('&');
					if (select_in_view.length == 2)
					{
						node = select_in_view[0];
						select_in_view = select_in_view[1];
					}
					var finded_by_id = treeView.get_node_by_id(node);
					if (finded_by_id)
					{
						node = finded_by_id;
					}
					else
					{
						node = treeView.get_node_by_guid(node);
					}
					select_in_view && (select_in_view = treeView.get_node_by_id(select_in_view));
					setTimeout(function() {
						if (!node) return;
						if (node.state && node.state.hidden)
						{
							dontSaveState = true;
							treeView.select_folder(node.parent);
							listView.selectById(node.id);
						}
						else
						{
							dontSaveState = true;
							treeView.select_folder(node);
						}
						setTimeout(function() {
							select_in_view && listView.selectById(select_in_view.id);
						}, 200);
					}, 10);
					if (hash.length < 3)
					{
						showSidebar(true);
					}
					else if (hash[2] == '1')
					{
						showSidebar(false);
					}
					break;
				case 'search':
					var searchString = hash[1];
					searchString && treeView.search(searchString);
					break;
			}
		}
		catch (e)
		{
			_log("error in checkHash", e);
		}
	}
}

function onNodeDrop(event) {
	var pos = droped_pos;
	droped_pos = null;
	separator.remove();
	var node;
	var selected_node;
	var folder_selected = false;
	if ($(event.target).is('a.jstree-anchor, i.jstree-icon'))
	{
		node = $(event.target).closest("li");
		if (node.length)
		{
			node = node[0].id;
			node = treeView.get_node_by_id(node);
			if (node.original.type == Types.curation_link.id || 
				node.original.type == Types.web_link.id || pos != 'into')
			{
				selected_node = treeView.get_node_by_id(node.parent);
			}
			else
			{
				selected_node = node;
			}
		}
	}
	else if ($(event.target).is('.dataTable th, .dataTable td'))
	{
		var table = $("#jstable_dom").dataTable().api();
		var data = table.row($(event.target)).data();
		if (data)
		{
			node = data[0];
			node = treeView.get_node_by_id(node);
			selected_node = treeView.get_node_by_id(node.parent);
		}
		else
		{
			node = treeView.get_selected();
			selected_node = node;
			folder_selected = true;
		}
	}
	else if ($(event.target).is('#list-pane'))
	{
		node = treeView.get_selected();
		selected_node = node;
		folder_selected = true;
	}
	if (!node || !selected_node)
	{
		return;
	}
	
	if (moved_node_id && moved_node_id.length)
	{
		for (var i = 0; i < moved_node_id.length; i++)
		{
			if (moved_node_id[i] == node.id)
			{
				moved_node_id = null;
				return;
			}
		}
		
		var need_popup = false;
		var moved_node = treeView.get_node_by_id(moved_node_id[0]);
		if (moved_node_id.length == 1 && moved_node && moved_node.type == Types.web_link.id)
		{
			need_popup = true;
		}
		view_controller.move(moved_node_id, selected_node, need_popup, {pos: pos, id: node.id});
		moved_node_id = null;
	}
	else if (node.original.source_id != CurationTree._id && event.originalEvent.dataTransfer)
	{
		var url = event.originalEvent.dataTransfer.getData('url');
		var title_html = event.originalEvent.dataTransfer.getData('text/html');
		var title;
		if (title_html)
		{
			title = title_html.match(/>(.*?)</g);
			if (title && title.length > 1)
			{
				title = title[title.length - 1];
				if (title[0] == '>')
				{
					title = title.substr(1, title.length - 2);
				}
			}
			else
			{
				title = undefined;
			}
		}
		var type = Types.curation_link.id;
		if (selected_node.type == Types.placeholder_folder.id)
		{
			type = Types.web_link.id;
		}
		view_controller.create(type, selected_node, {
			url: url,
			title: title
		}, null, {
			pos: pos,
			id: node.id
		});
	}
	moved_node_id = null;
}
function onNodeDragEnter(event) {
	var node = $(event.target).closest("li");
	if (node.length)
	{
		node = node[0].id
		var jstree = $(event.target).closest(".jstree");
		if (jstree.length)
		{
			var node = jstree.jstree(true).get_node(node);
			if (node.type != Types.curation_link.id && node.type != Types.web_link.id)
			{
				jstree.jstree(true).deselect_all();
				jstree.jstree(true).select_node(node);
			}
		}
	}
}

function onNodeDragOver(event, need_draged_el) {
	separator.remove();
	droped_pos = 'into';
	if (draged_el)
	{
		draged_el.css('top', event.pageY - 5);
		draged_el.css('left', event.pageX + 7);
	}
	
	if ($(event.target).is('a.jstree-anchor, i.jstree-icon, .dataTable td'))
	{
		var target = $(event.target).closest('li, tr');
	//	target.addClass('dragovered');
		var height =  $(event.target).outerHeight();
		var posY =  $(event.target).offset().top;
		if (event.pageY < posY + height / 8)
		{
			target.before(separator);
			droped_pos = 'before';
		}
		else if (event.pageY > posY + 7 * height / 8)
		{
			target.after(separator);
			droped_pos = 'after';
		}
		else
		{
			droped_pos = 'into';
		}
	}
}

function onNodeDragLeave(event) {
//	var node = $(event.target).closest("li");
//	if (node.length)
	{
	//	var id = node[0].id;
	//	var jstree = $(event.target).closest(".jstree");
	//	jstree.jstree(true).deselect_node(id);
	}
}

$(document).ready(function() {
	localize();
	setTreePanelSize();
	
	treeView = new TreeView();
	listView = new ListView();
	
	treeView.add_listeners('onSelected', function(node) {
		if (!node)
		{
			$('#table, #tree').hide();
			$('#info-pane-wrap').hide();
			$('#infoToggle').hide();
			$('#list-pane').addClass('full');
			return;
		}
		listView.update(node);
		pushHistoryState('#select|' + node.original.id);
		showInfobar();
		if (node.original.type == Types.curation_folder.id ||
			node.original.type == Types.curation_page.id)
		{
		//	$('#info-pane').html('');
			$('#info-pane-wrap').show();
			$('#infoToggle').show();
			$('#list-pane').removeClass('full');
		}
		else
		{
			$('#info-pane-wrap').hide();
			$('#infoToggle').hide();
			$('#list-pane').addClass('full');
		}
	});
	treeView.add_listeners('onSearch', function(node) {
		node = JSON.parse(JSON.stringify(node));
		for (var i = 0; i < node.children.length; i++)
		{
			var path = "";
			for (var j = node.children[i].parents.length - 2; j >= 0; j--)
			{
				var par = treeView.get_node_by_id(node.children[i].parents[j]);
				var node_id_to_select = j > 0 ? node.children[i].parents[j - 1] : node.children[i].id;
				path += '<a class="search_path_link" href="#select|' + par.id + '&' + node_id_to_select + '|0">' + par.original.title + '</a>/';
			}
			node.children[i].text = path + node.children[i].text;
		}
		listView.update(node);
		$('#info-pane-wrap').hide();
		$('#infoToggle').hide();
		$('#list-pane').addClass('full');
		
		setTimeout(function() {
			var path_links = $('a.search_path_link');
			path_links.on('click', function(e) {
				window.location = e.target.href;
			});
		}, 100);
	});
	treeView.add_listeners('onInitialized', function(node) {
		
		if (!localStorage.jstree_custom)
		{
			window.location.hash = "#select|FC0D11DB66BEC32A591F12600FF27A57";
		}
		
		checkHash();
	});
	listView.add_listeners('onDoubleSelected', function(node) {
		if (typeof(node) == "string")
		{
			node = treeView.get_node_by_id(node);
		}
		if (node.original.type == Types.web_link.id || node.original.type == Types.curation_link.id)
		{
			open_link({url: node.original.url});
		}
		else
		{
			treeView.select_folder(node);
			
		}
	});
	treeView.add_listeners('onDoubleSelected', function(node) {
		if (typeof(node) == "string")
		{
			node = treeView.get_node_by_id(node);
		}
		if (node.original.type == Types.curation_folder.id)
		{
			open_link({url: "chrome://bookmarks/#select|" + node.id + "|1"});
		}
	});
	
	listView.add_listeners('onSelected', function(node) {
		if (typeof(node) == "string")
		{
			node = treeView.get_node_by_id(node).original;
		}
		if (node.type == Types.curation_link.id)
		{
			$('#info-pane').html('<h4>' + node.title + '</h4>' + (node.description ? node.description : '') + '<a class="btn btn_default info_btn" target="_blank" href="' + node.url + '">' + Local["read_more"] + '</a>');
		}
		else
		{
			$('#info-pane').html('');
		}
	});
	listView.add_listeners('onDeselected', function(node) {
		$('#info-pane').html('');
	});
	
	main_port = chrome.runtime.connect();
	main_port.onMessage.addListener(function(msg) {
		_log(msg.data);
		switch (msg.type) 
		{
			case "onLoadingStart":
				$('.progress').addClass('active');
				break;
			case "onLoadingEnd":
				$('.progress.active').removeClass('active');
				break;
			case "all_nodes": 
				treeView.update(msg.data);
				showSidebar();
				break;
			case "onCreated":
			_log('create', msg.data.node);
				treeView.create(msg.data, true);
				break;
			case "onRemoved":
				treeView.remove(msg.data);
				break;
			case "onChanged":
			_log('change', msg.data.node)
				treeView.change(msg.data, true);
				break;
			case "onMoved":
				treeView.move(msg.data);
			//	main_port.postMessage({type: "get_all_nodes"});
				break;
			case "onChildrenReordered":
			//	treeView.childrenReorder(msg.data);
				main_port.postMessage({type: "get_all_nodes"});
				break;
			case "temp_saved_data":
				temp_saved_data = msg.data;
				break;
			case "allInAppGoods":
				var goods = msg.data;
				$('header .dropdown-menu .inapp').remove();
				for (var i = 0; i < goods.length; i++)
				{
					var item = $('<li class="inapp' + (goods[i].buyed ? ' disabled buyed' : '') + '"><a href="#" id="' + goods[i].sku + '">' + Local['buy'] + ' ' + goods[i].localeData[0].title + '</a></li>');
					$('header .dropdown-menu').append(item);
				}
				$('header .dropdown-menu .inapp a').on('click', function() {
					if ($(this).parent().is('.buyed')) return;
					main_port.postMessage({
						type: "buyInAppGoods",
						data: {
							goods_id: $(this).attr('id')
						}
					});
				});
				break;
			case "needLogin":
				$('header .dropdown-menu .inapp').remove();
				var item = $('<li class="inapp"><a href="#" id="needLogin">' + Local['loginToGetSubscription'] + '</a></li>');
				$('header .dropdown-menu').append(item);
				item.on('click', function() {
					open_link({url: "chrome://settings/"});
				});
				break;
			case "buyInAppGoods":
				if (msg.data.status == 'ok')
				{
					var goodsBtn = $('header .dropdown-menu .inapp a#' + msg.data.goods_id);
					goodsBtn.parent().addClass('disabled buyed');
				}
				else
				{
					
				}
				break;
			case "additional_data":
				listView.setFilters(msg.additional_data);
		}
	});
	main_port.postMessage({type: "get_all_nodes"});
	main_port.postMessage({type: "getAllInAppGoods"});
	$('.header_menu_btn').on("click", function() {
		main_port.postMessage({type: "getAllInAppGoods"});
		Sync.updateStorageData(function() {
			$('.sync_checkbox input').prop('checked', Sync.userOpinionForEnabling);
		});
	});
	
	$('.sync_checkbox input').on('change', function() {
		Sync.setUserOpinionForEnabling(this.checked);
	});

	$("header input#term").keyup(function() {
		var searchString = $(this).val();
		window.location = '#search|' + searchString;
    });
	
	window.onhashchange = checkHash;
	
	$(document).on('drop', function (event) {
		event.preventDefault();
		if ($(event.target).is('a.jstree-anchor, i.jstree-icon, #list-pane, .dataTable th, .dataTable td'))
		{
			onNodeDrop(event);
		}
	});
	$(document).on('dragstart', function (event) {
		if (!$('.ui-widget-overlay').length)
		{
			event.preventDefault();
		}
	});
	$(document).on('dragenter', function (event) {
		event.preventDefault();
		if ($(event.target).is('a.jstree-anchor, i.jstree-icon, #list-pane, .dataTable th, .dataTable td'))
		{
			onNodeDragEnter(event);
		}
	});
	$(document).on('dragover', function (event) {
		event.preventDefault();
		onNodeDragOver(event);
	});
	$(document).on('dragleave', function (event) {
		if ($(event.target).is('a.jstree-anchor, i.jstree-icon, #list-pane, .dataTable th, .dataTable td'))
		{
			onNodeDragLeave(event);
		}
	});
	
	var mouse_clicked = false;
	var tree_resizing = false;
	var info_resizing = false;
	var mause_downed = false;
	$(document).on("mousedown", function(event) {
		mause_downed = true;
		if (event.button != 0)
		{
			return;
		}
		if ($(event.target).is('#tree-pane') && event.pageX > $('#tree-pane').width() - 5)
		{
			tree_resizing = true;
		}
		else if ($(event.target).is('#info-pane-wrap') && event.pageY < $('#info-pane-wrap').position().top + 5)
		{
			info_resizing = true;
		}
		else if ($(event.target).is('a.jstree-anchor, i.jstree-icon, .dataTable td'))
		{
			moved_node_id = [];
			var moved_node_dom = [];
			mouse_clicked = true;
			if ($(event.target).is('a.jstree-anchor, i.jstree-icon'))
			{
				draged_el = $('<div></div>');
				node = $(event.target).closest("li");
				if (node.length)
				{
					if (node.find('.jstree-clicked').length && node.parent().find('.jstree-clicked').length > 1)
					{
						var selected = node.parent().find('.jstree-clicked');
						for (var i = 0; i < selected.length; i++)
						{
							var node_id = $(selected[i]).closest("li")[0].id;
							moved_node_id.push(node_id);
							moved_node_dom.push($(selected[i]).closest("li").clone(false));
						}
					}
					else
					{
						node = node[0].id;
						node = treeView.get_node_by_id(node);
						moved_node_id.push(node.id);
						moved_node_dom.push($(event.target).closest("li").clone(false));
					}
				}
			}
			else if ($(event.target).is('.dataTable th, .dataTable td'))
			{
				draged_el = $('<table></table>');
				var table = $("#jstable_dom").dataTable().api();
				var row = table.row($(event.target));
				if (row)
				{
					if ($(row.node()).is('.custom_selected'))
					{
						var selected = $(row.node()).parent().find('.custom_selected');
						for (var i = 0; i < selected.length; i++)
						{
							var node_id = table.row($(selected[i])).data()[0];
							moved_node_id.push(node_id);
							moved_node_dom.push($(selected[i]).clone(false));
						}
					}
					else
					{
						var data = table.row($(event.target)).data();
						if (data)
						{
							moved_node_id.push(data[0]);
							moved_node_dom.push($(event.target).closest("tr").clone(false));
						}
					}
				}
				
			}
			
			if (moved_node_id)
			{
				for (var i = 0; i < moved_node_dom.length; i++)
				{
					draged_el.append(moved_node_dom[i]);
				}
				$('body').append(draged_el);
				draged_el.css('position', 'fixed');
			}
			_log(moved_node_id);
		}
	});
	$(document).on("mousemove", function(event) {
		if (mause_downed)
		{
			listView && listView.refreshHeaderColumnWidths();
		}
		if (tree_resizing)
		{
			setTreePanelSize(event.pageX);
		}
		if (info_resizing)
		{
			setInfoPanelSize($(window).height() - event.pageY);
		}
		if (!mouse_clicked || $('.ui-widget-overlay').length)
		{
			return;
		}
		if (!dragging)
		{
			dragging = true;
			return;
		}
		event.preventDefault();
		onNodeDragOver(event, true);
	});
	$(document).on("mouseup", function(event) {
		mause_downed = false;
		if (draged_el)
		{
			draged_el.remove();
			draged_el = null;
		}
		mouse_clicked = false;
		tree_resizing = false;
		info_resizing = false;
		if (dragging)
		{
			dragging = false;
			onNodeDrop(event);
		}
		moved_node_id = null;
	});
	$('#sidebarToggle a').on('click', function() {
		if ($(this).is('.show_btn'))
		{
			showSidebar(true);
		}
		else
		{
			showSidebar(false);
		}
	});
	$('#infoToggle a').on('click', function() {
		if ($(this).is('.show_btn'))
		{
			showInfobar(true);
		}
		else
		{
			showInfobar(false);
		}
	});
	$('#imported_file_source').on('change', function(evt) {
		var files = evt.target.files;
		if (files.length <= 0)
		{
			current_selected_curation_folder = null;
			return;
		}
		var reader = new FileReader();
		reader.onload = function(theFile) {
			main_port && main_port.postMessage({
				type: "import", 
				data: {
					file_text: theFile.srcElement.result,
					parent: current_selected_curation_folder
				}
			});
			current_selected_curation_folder = null;
		};
		reader.readAsText(files[0]);
	});
	$('#imported_file_source').on('change', function() {
		this.value = null;
	});
	$(window).on('resize', function() {
		listView.refreshHeaderColumnWidths();
	});
});

function setTreePanelSize(size) {
	if (size === undefined)
	{
		size = localStorage.treePanelSize || 260;
		if (size < 10) size = 260;
	}
	else
	{
		localStorage.treePanelSize = size;
	}
	size = size * 1 + 2;
	if (size < 120)
	{
		size = 120;
	}
	$('#tree-pane').scrollLeft(0);
	$('#tree-pane').css('width', size + 'px');
	$('#right-pane').css('width', 'calc(100% - ' + (size) + 'px)');
	$('#sidebarToggle').css('left', (size - 32) + 'px');
}

function showSidebar(show) {
	var folder_mode = document.location.search.indexOf('folder_mode') >= 0;
	if (show === undefined)
	{
		if (folder_mode)
		{
			show = false;
		}
		else
		{
			show = !((localStorage.sidebar === false) || (localStorage.sidebar === "false"));
		}
	}
	else if (!folder_mode)
	{
		localStorage.sidebar = show;
	}
	sidebar = show;
	if (show)
	{
		$('.path').css('display', 'none');
		$('#sidebarToggle a.show_btn').hide();
		setTreePanelSize();
		document.title = Local["bookmarkPageTitle"];
	}
	else
	{
		$('#sidebarToggle a.show_btn').show();
		$('.path').css('display', 'inline-block');
		$('#tree-pane').css('width', '0px');
		$('#right-pane').css('width', '100%');
		$('#sidebarToggle').css('left', (-32) + 'px');
		var selected = treeView.get_selected();
		if (selected)
		{
			document.title = selected.text;
		}
	}
	$(window).trigger('resize');
}

function setInfoPanelSize(size) {
	var selected_id = treeView ? treeView.get_selected() : null;
	if (selected_id)
	{
		selected_id = selected_id.id;
	}
	else
	{
		selected_id = 'default';
	}
	if (size === undefined)
	{
		size = localStorage["infoPanelSize" + selected_id] || 260;
		if (size < 10) size = 260;
	}
	else
	{
		localStorage["infoPanelSize" + selected_id] = size;
	}
	size = size * 1 + 2;
	if (size < 120)
	{
		size = 120;
	}
	$('#info-pane').scrollTop(0);
	$('#info-pane-wrap').css('height', size + 'px');
	$('#list-pane').css('height', 'calc(100% - ' + (size) + 'px)');
	$('#infoToggle').css('bottom', (size - 32) + 'px');
}

function showInfobar(show) {
	var selected_id = treeView.get_selected();
	if (selected_id)
	{
		selected_id = selected_id.id;
	}
	else
	{
		selected_id = 'default';
	}
	if (show === undefined)
	{
		show = !((localStorage["infobar" + selected_id] === false) || (localStorage["infobar" + selected_id] === "false"));
	}
	else
	{
		localStorage["infobar" + selected_id] = show;
	}
	if (show)
	{
		$('#infoToggle a.show_btn').hide();
		setInfoPanelSize();
	}
	else
	{
		$('#infoToggle a.show_btn').show();
		$('#info-pane-wrap').css('height', '0px');
		$('#list-pane').css('height', '100%');
		$('#infoToggle').css('bottom', (-32) + 'px');
	}
}

function pushHistoryState(address) {
	if (dontSaveState)
	{
		dontSaveState = false;
		return;
	}
	history.pushState(null, '', address);
}