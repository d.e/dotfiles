function StandartBookmarks() {
	var _this = this;
	this.was_moved_flag = false;
	Base.apply(_this, arguments);

	_this.node_to_view_json = function(node) {
		node.source_id = StandartBookmarks._id;
		if (node.title)
		{
			node.text = node.title;
		}
		if (node.url)
		{
			node.type = Types.web_link.id;
			if (node.url.indexOf('http') == 0)
			{
				node.icon = 'https://www.google.com/s2/favicons?domain=' + node.url;
			}
			node.state = {};
			node.state.hidden = true;
		}
		else
		{
			node.type = Types.placeholder_folder.id;
		}
		node.li_attr = {'class': node.type};
		if (node.children && node.children.length)
		{
			var tmp_children = [];
			for (var i = 0; i < node.children.length; i++)
			{
				if (node.children[i].id != CurationFolders.curation_toolbar_id)
				{
					tmp_children.push(_this.node_to_view_json(node.children[i]));
				}
			}
			node.children = tmp_children;
		}

		return node;
	}
}
StandartBookmarks.prototype = Object.create(Base.prototype);
StandartBookmarks._id = 'StandartBookmarks';

StandartBookmarks.prototype.onCreated = function(id, bookmarkTreeNode) {
	this.run_listeners("onCreated", {node: this.node_to_view_json(bookmarkTreeNode)});
	if (this.was_moved_flag)
	{
		this.run_listeners("onMoved");
		this.was_moved_flag = false;
	}
}
StandartBookmarks.prototype.onRemoved = function(id, info) {
	info.node.parentId = info.parentId;
	this.run_listeners("onRemoved", {node: info.node});
}
StandartBookmarks.prototype.onChanged = function(id, info) {
	this.run_listeners("onChanged", {node: id, info: this.node_to_view_json(info)});
}
StandartBookmarks.prototype.onMoved = function(id, info) {
	if (id != CurationFolders.curation_toolbar_id)
	{
		this.run_listeners("onMoved", {node: id, parent: info.parentId, index: info.index});
	}
}
StandartBookmarks.prototype.onChildrenReordered = function(id, info) {
	this.run_listeners("onChildrenReordered", {node: id, info: info});
}

StandartBookmarks.prototype.get = function() {
	var _this = this;
	return new Promise(function(resolve, reject) {
		try
		{
			chrome.bookmarks.getTree(function(bookmarksNodes) {
				var view_json = _this.node_to_view_json(bookmarksNodes[0]);
				resolve(view_json.children);
			});
		}
		catch (e)
		{
			reject();
		}
	});
}
StandartBookmarks.prototype.create = function(new_node, pos) {
	if (!Array.isArray(new_node))
	{
		new_node = [new_node];
	}
	var _this = this;
	function bookmarks_create(node) {
		chrome.bookmarks.create({
			parentId: node.parentId,
			index: node.index,
			title: node.title,
			url: (node.type == Types.web_link.id || node.type == Types.curation_link.id) ? node.url : undefined
		}, function(bm_node) {
			if (node.children) for (var i = 0; i < node.children.length; i++)
			{
				node.children[i].parentId = bm_node.id;
				bookmarks_create(node.children[i]);
			}
		});
	}
    for (var i = 0; i < new_node.length; i++)
	{
		if (pos && pos.id != new_node[i].parentId)
		{
			chrome.bookmarks.get(pos.id, function(nodes) {
				if (nodes && nodes.length)
				{
					var index = nodes[0].index;
					if (pos.pos == 'before' && index > 0)
					{
					//	index--;
					}
					else if (pos.pos == 'after')
					{
						index++;
					}
					new_node[i].index = index;
					_this.was_moved_flag = true;
				}

				bookmarks_create(new_node[i]);
			});
		}
		else
		{
			bookmarks_create(new_node[i]);
		}
	}
}
StandartBookmarks.prototype.remove = function(node) {
	if (node.type == Types.curation_link.id || 
		node.type == Types.web_link.id)
	{
		chrome.bookmarks.remove(node.id);
	}
	else
	{
		chrome.bookmarks.removeTree(node.id);
	}
}
StandartBookmarks.prototype.change = function(node) {
	chrome.bookmarks.update(node.id, {
		title: node.title,
		url: node.url
	});
}
StandartBookmarks.prototype.move = function(node, pos) {
	chrome.bookmarks.getSubTree(pos.id, function(nodes) {
		if (nodes && nodes.length)
		{
			var index = nodes[0].index;
			if (pos.pos == 'after')
			{
				index++;
			}
			if (pos.pos == 'into')
			{
				index = nodes[0].children.length;
			}
			chrome.bookmarks.move(node.id, {
				parentId: pos.pos == 'into' ? nodes[0].id : nodes[0].parentId,
				index: index
			});
		}
	});
}