var Local = {
	"en": {
		"bookmarkPageTitle": "Bookmark Manager",
		"bookmarksSearch": "Bookmarks Search...",
		"curationFolderTitle": "Curation Folders",
		"curationTreeTitle": "Curation Tree",
		"create": "Create",
		"cut": "Cut",
		"copy": "Copy",
		"paste": "Paste",
		"edit": "Edit",
		"remove": "Remove",
		"open": "Open",
		"new": "New ",
		"web_link": "Web Link",
		"placeholder_folder": "Placeholder Folder",
		"curation_link": "Curation Link",
		"curation_folder": "Curation Folder",
		"curation_file": "Curation File",
		"curation_group": "Curation Group",
		"curation_page": "Curation Page",
		"curation_root": "Curation Tree",
		"open_new_tab": "Open in new tab",
		"open_new_window": "Open in new window",
		"open_incognito": "Open incognito",
		"export": "Export",
		"export_curation_file": "Export curation file",
		"title": "Name",
		"headline": "Headline",
		"url": "Url",
		"type": "Type",
		"tags": "Tags",
		"comment": "Comment",
		"publisher": "Publisher",
		"author": "Author",
		"link": "Link",
		"save": "Save",
		"cancel": "Cancel",
		"created": "Created",
		"pub_date": "Pub. Date",
		"rating": "Rating",
		"read_more": "Read more...",
		"folder": "Folder",
		"image_url": "Image Url",
		"description": "Description",
		"twitter": "Twitter",
		"scan_images": "Scan for images",
		"root": "Root",
		"links_count_error": "Limitations: " + Payment.config.free_links_per_folder + " links per folder. Buy full version to remove this limit.",
		"folders_count_error": "Limitations: " + Payment.config.free_folders_count + " folders. Buy full version to remove this limit.",
		"import": "Import",
		"buy": "Buy",
		"loginToGetSubscription": "Sign in to Google Chrome to get subscription",
		"subscriptionError": "You need a subscription for this functionality.",
		"subscriptionWarning": "You don't have a subscription, only one folder will be imported."
	}
}

var lang = chrome.i18n.getUILanguage();
if (!Local[lang])
{
	Local = Local['en'];
}
else
{
	Local = Local[lang];
}