var IndexedDB = function() {
	var dbName = "kuratordb";
	var tableName = "curationFolders";
	var self = this;
	var ReadType = {
		"array": "readAsArrayBuffer",
		"binary": "readAsBinaryString",
		"dataURL": "readAsDataURL",
		"text": "readAsText" 
	}

	self.db = null;
	self.inited = false;
	self.slice = Array.prototype.slice;

	function getDb(onsuccess, onerror) 
	{
		var onsuccess_flag = false;
		if (self.db)
		{
			!onsuccess_flag && onsuccess && (onsuccess(self.db), onsuccess_flag = true);
		}
		else
		{
			function bindEvents(db) {
				db.onabort = function() {
					self.db = null;
				}
				db.onclose = function() {
					self.db = null;
				}
				db.onerror = function() {
					self.db = null;
				}
			}
			var request = indexedDB.open(dbName, 1);
			request.onerror = function(event) {
				onerror && onerror();
			};
			request.onsuccess = function(event) {
				self.db = event.target.result;
				bindEvents(self.db);
				!onsuccess_flag && onsuccess && (onsuccess(self.db), onsuccess_flag = true);
			};
			request.onupgradeneeded = function(event) {
				self.db = event.target.result;
				bindEvents(self.db);
				if (!self.db.objectStoreNames.contains(tableName))
				{
					self.db.createObjectStore(tableName, { keyPath: "id" });
					var transaction = event.target.transaction;
					transaction.oncomplete = function() {
						!onsuccess_flag && onsuccess && (onsuccess(self.db), onsuccess_flag = true);
					}
				}
				else
				{
					!onsuccess_flag && onsuccess && (onsuccess(self.db), onsuccess_flag = true);
				}
			};
		}
	}

	this.clear = function(onsuccess, onerror) {
		getDb(function(db) {
			var request = db.transaction([tableName], "readwrite")
							.objectStore(tableName)
							.clear();
			request.onsuccess = onsuccess;
			request.onerror = onerror;
		});
	}

	this.remove = function(id, onsuccess, onerror) {
		getDb(function(db) {
			var request = db.transaction([tableName], "readwrite")
							.objectStore(tableName)
							.delete(id);
			request.onsuccess = onsuccess;
			request.onerror = onerror;
		});
	}

	this.set = function(id, str, onsuccess, onerror) {
		getDb(function(db) {
			var request = db.transaction([tableName], "readwrite")
							.objectStore(tableName)
							.put({
								id: id,
								str: str
							});
			request.onsuccess = onsuccess;
			request.onerror = onerror;
		});
	}

	this.get = function(id, onsuccess, onerror) {
		getDb(function(db) {
			var request = db.transaction([tableName], "readwrite")
							.objectStore(tableName)
							.get(id);
			request.onsuccess = function(event) {
				if (request.result && request.result.str)
				{
					onsuccess && onsuccess(request.result.str);
				}
				else
				{
					onerror && onerror();
				}
			};
			request.onerror = onerror;
		});
	}
}

var IndexedDB = new IndexedDB();