function CurationTree() {
	var _this = this;
	var syncTimeout;
	Base.apply(_this, arguments);
	
	this.root_node = {};
	this.nodes_add = function(node) {
		var parent = this.getNodeById(node.parentId);
		if (!node.id)
		{
			node.guid = this.guid();
			node.id = node.guid;
		}
		if (parent.children && parent.children[node.id])
		{
			var exist_node = parent.children[node.id];
			for (var key in node)
			{
				if (key != 'children')
					exist_node[key] = node[key];
			}
			node = exist_node;
		}
		else
		{
			parent.children[node.id] = node;
			node.parentId = parent.id;
		}
		if (node.index == undefined)
		{
			node.index = Object.keys(parent.children).length - 1;
		}
		this.save();
		this.saveOnServer();
		return node;
	}
	this.nodes_remove = function(node) {
		var parent = this.getParentNodeById(node.id);
		delete parent.children[node.id];
		this.save();
		this.saveOnServer();
	}
	this.nodes_move = function(id, parentId, index) {
		var parent = this.getParentNodeById(id);
		var cloned_node = JSON.parse(JSON.stringify(parent.children[id]));
		var last_index = cloned_node.index;
		cloned_node.index = index;
		cloned_node.parentId = parentId;
		var parent2 = this.getNodeById(parentId);
		for (var i in parent2.children)
		{
			if (parent2.children[i].index >= index)
			{
				parent2.children[i].index++;
			}
		}
		delete parent.children[id];
		for (var i in parent.children)
		{
			if (parent.children[i].index > last_index)
			{
				parent.children[i].index--;
			}
		}
		if (parent == parent2 && cloned_node.index > last_index)
		{
			cloned_node.index--;
		}
		parent2.children[id] = cloned_node;
		this.save();
		this.saveOnServer();
		return parent2.children[id].index;
	}
	
	this.getNodeById = function(id) {
		function getNodeByIdFromNode(id, node) {
			if (id == node.id)
			{
				return node;
			}
			else if (node.children)
			{
				for (var i in node.children)
				{
					var finded = getNodeByIdFromNode(id, node.children[i]);
					if (finded)
					{
						return finded;
					}
				}
			}
			else 
			{
				return undefined;
			}
		}
		
		return getNodeByIdFromNode(id, this.root_node);
	}
	this.getParentNodeById = function(id) {
		function getParentByIdInNode(id, node) {
			if (!node.children)
			{
				return undefined;
			}
			if (!node.children[id])
			{
				for (var i in node.children)
				{
					var finded = getParentByIdInNode(id, node.children[i]);
					if (finded)
					{
						return finded;
					}
				}
			}
			else 
			{
				return node;
			}
		}
		
		return getParentByIdInNode(id, this.root_node);
	}
	
	this.save = function() {
		var save_data = {};
		save_data[CurationTree._id] = JSON.stringify(this.root_node);
		chrome.storage.local.set(save_data);
	}
	this.saveOnServer = function(callback) {
		if (this.is_syncing)
		{
			callback && callback({'error': 'sync in process'});
			return;
		}
		if (!Sync.enabled)
		{
			callback && callback({'error': 'sync not enabled'});
			return;
		}
		Sync.Api.setall('tree', JSON.stringify(this.root_node));
	}
	
	this.mergeCurrentToSynced = function(current, synced) {
		for (var i in current)
		{
			if (!synced[i])
			{
				this.remove(current[i]);
			}
			else if (current[i].children)
			{
				this.mergeCurrentToSynced(current[i].children, synced[i].children || {});
			}
		}
	}
	this.mergeSyncedToCurrent = function(synced, current) {
		for (var i in synced)
		{
			if (!current[i])
			{
				var existed = this.getNodeById(i);
				if (existed)
				{
					this.move(existed, {'pos': 'into', 'id': synced[i].parentId});
				}
				else
				{
					this.create(synced[i]);
				}
			}
			else
			{
				this.change(synced[i]);
			}
			if (synced[i].children)
			{
				this.mergeSyncedToCurrent(synced[i].children, current[i].children || {});
			}
		}
	}
	
	this.startSync = function() {
		Sync.Api.getall('tree', function(result) {
			if (result && result.children)
			{
				_this.is_syncing = true;
				_this.mergeSyncedToCurrent(result.children, _this.root_node.children);
				_this.mergeCurrentToSynced(_this.root_node.children, result.children);
				_this.is_syncing = false;
			}
		});
		
		if (syncTimeout)
		{
			clearTimeout(syncTimeout);
		}
		syncTimeout = setTimeout(function() {
			_this.startSync();
		}, Sync.syncTimeoutDelayTree);
	}
	
	this.stopSync = function() {
		if (syncTimeout)
		{
			clearTimeout(syncTimeout);
		}
	}
	
	function init() {
	//	if (Sync.enabled)
		{
			_this.startSync();
		}
	}
	
	chrome.storage.local.get(CurationTree._id, function(res) {
		if (res[CurationTree._id])
		{
			_this.root_node = JSON.parse(res[CurationTree._id]);
		}
		if (!_this.root_node || Object.keys(_this.root_node).length <= 0)
		{
			_this.root_node = {
				source_id: CurationTree._id,
				text: CurationTree.title_for_bookmarks,
				li_attr: {'class': 'padding'},
				type: Types.curation_root.id,
				title: CurationTree.title_for_bookmarks,
				id: CurationTree.curation_toolbar_id,
				children: {}
			}
		}
		init();
	});
}
CurationTree.prototype = Object.create(Base.prototype);
CurationTree._id = 'CurationTree';
CurationTree.title_for_bookmarks = Local["curationTreeTitle"];
CurationTree.curation_toolbar_id = 'cur_tr_0';

CurationTree.prototype.get = function() {
	var _this = this;
	return new Promise(function(resolve, reject) {
		resolve([_this.objToArray(_this.root_node)]);
	});
}

CurationTree.prototype.create = function(new_node) {
	var _this = this;
	function treenode_create(node) {
		var n_node = _this.arrayToObj(node);
		n_node.children = {};
		n_node = _this.nodes_add(n_node);
		n_node = _this.objToArray(n_node);
		_this.run_listeners("onCreated", {node: n_node});
		
		if (node.children) for (var i = 0; i < node.children.length; i++)
		{
			node.children[i].parentId = n_node.id;
			treenode_create(node.children[i]);
		}
	}
	if (Array.isArray(new_node))
	{
		for (var i = 0; i < new_node.length; i++)
		{
			treenode_create(new_node[i]);
		}
	}
	else
	{
		treenode_create(new_node);
	}
}

CurationTree.prototype.remove = function(node) {
    node = this.arrayToObj(node);
	this.nodes_remove(node);
	this.run_listeners("onRemoved", {node: node});
}

CurationTree.prototype.change = function(node) {
	node = this.arrayToObj(node);
	var node = this.nodes_add(node);
	node = this.objToArray(node);
	this.run_listeners("onChanged", {node: node.id, info: node});
}

CurationTree.prototype.move = function(node, pos) {
	var pos_node = this.getNodeById(pos.id);
	var index = pos_node.index;
	var new_parent_id = pos_node.parentId;
	if (pos.pos == 'after')
	{
		index++;
	}
	if (pos.pos == 'into')
	{
		index = Object.keys(pos_node.children).length;
		new_parent_id = pos_node.id;
	}
	
	index = this.nodes_move(node.id, new_parent_id, index);
	this.run_listeners("onMoved", {node: node.id, parent: new_parent_id, index: index});
}

CurationTree.prototype.forEach = function(callback) {
	function loop(node) {
		callback(node);
		if (node.children)
		{
			for (var i in node.children)
			{
				loop(node.children[i]);
			}
		}
	}
		
	loop(this.root_node);
	this.save();
}

CurationTree.prototype.removeLinkedPages = function(folder_id) {
	var _this = this;
	this.forEach(function(node) {
		if (node.curation_folder_id == folder_id)
		{
			_this.remove(node);
		}
	});
}