var _debug = false;
function _log() {
	if (_debug)
	{
		console.log.apply(null, Array.prototype.slice.call(arguments, 0));
	}
}

var SendPostDataTo = function(url, data, headers, onload, onerror) {
	var xhttp = new XMLHttpRequest();
	var postdata = "";
	for (var key in data)
	{
		if (postdata.length > 0)
		{
			postdata += '&';
		}
		postdata += key + '=' + encodeURIComponent(data[key]);
	}
	xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	for (var key in headers)
	{
		xhttp.setRequestHeader(key, headers[key]);
	}
	xhttp.onload = function() {
		onload(xhttp.responseText);
	};
	xhttp.onerror = function() {
		onerror({'error': '-'});
	};
	if (navigator.onLine) {
		xhttp.send(postdata);
	}
	else {
		onerror({'error': '-'});
	}
}

var Payment = {
	get paid() { return false; },
	config: {
		free_folders_count: 3,
		free_links_per_folder: -1
	},
	setPaid: function (paid) {
		if (paid !== Payment.paid)
		{
			Sync.Api.sendSubscriptions();
		}
		Payment.__defineGetter__("paid", function() { return paid; });
		chrome.storage.sync.set({"payment_data": Payment.paid});
	},
	getPaid: function(callback) {
		callback && callback();
		return Payment.paid;
	},
	init: function(callback) {
		chrome.storage.sync.get("payment_data", function(res) {
			if (res["payment_data"] != undefined)
			{
				Payment.__defineGetter__("paid", function() { return res["payment_data"]; });
			}
			callback && callback(Payment.paid);
		});
		chrome.storage.onChanged.addListener(function(changes, areaName) {
			if (changes["payment_data"])
			{
				Payment.__defineGetter__("paid", function() { return changes["payment_data"].newValue; });
			}
		});
	}
}
Payment.init();

var Sync = {
	userOpinionForEnabling: true,
	enabled: true,
	email: "",
	syncServerUrl: "http://18.220.152.68:8888/",
	syncTimeoutDelayFolders: 60000,
	syncTimeoutDelayTree: 60000,
	enableSync: function(enable) {
		Sync.enabled = enable;
		chrome.storage.sync.set({"sync_status": Sync.enabled});
	},
	setUserOpinionForEnabling: function(enable) {
		Sync.userOpinionForEnabling = enable;
		chrome.storage.sync.set({"user_sync_status": Sync.userOpinionForEnabling});
	},
	updateStorageData: function(callback) {
		chrome.storage.sync.get({
			'sync_status': Sync.enabled,
			'user_sync_status': Sync.userOpinionForEnabling
		}, function(res) {
			if (res["sync_status"] != undefined)
			{
				Sync.enabled = res["sync_status"];
			}
			if (res["user_sync_status"] != undefined)
			{
				Sync.userOpinionForEnabling = res["user_sync_status"];
			}
			callback && callback();
		});
	},
	init: function(callback) {
	},
	authorize: function(callback) {
		chrome.identity.getProfileUserInfo(function(userInfo) {
			Sync.email = userInfo.email;
			if (Sync.email)
			{
				Sync.updateStorageData(function() {
					if (!Sync.enabled)
					{
						callback && callback({'error': 'Sync not enabled'});
					}
					else
					{
						chrome.identity.getAuthToken({'interactive': true}, function(token) {
							if (!chrome.runtime.lastError && token)
							{
								SendPostDataTo(Sync.syncServerUrl + 'authorize', {}, {
									'Authorization': 'Bearer ' + userInfo.id + ':' + token + ':' + userInfo.email
								}, function(result) {
									try
									{
										result = JSON.parse(result);
										if (result['error'] && Sync.enabled)
										{
											chrome.identity.removeCachedAuthToken({token: token}, function() {
												Sync.authorize(callback);
											});
											Sync.enabled = false;
										}
										callback && callback(result);
									}
									catch (e) {
										callback && callback({'error': 'server result error'});
									}
								}, function onerror(result) {
									callback && callback({'error': 'server error'});
								});
							}
							else
							{
								callback && callback({'error': 'token error'});
							}
						});
					}
				});
			}
			else
			{
				Sync.enabled = false;
				callback && callback({'error': 'Not logged'});
			}
		});
	},
	Api: {
		getall: function(type, callback, force) {
			Sync.updateStorageData(function() {
				if (!Sync.enabled || !Sync.userOpinionForEnabling)
				{
					callback && callback({error: 'disabled'}); 
					return;
				}

				SendPostDataTo(Sync.syncServerUrl + 'getall', {
					'email': Sync.email,
					'type': type,
					'lastupdated': force ? 0 : localStorage[type + 'OnServerlastModified']
				}, {}, function onload(result) {
					var ret;
					try
					{
						result = JSON.parse(result);
						if (result['error'] == 'access denied')
						{
							Sync.authorize(function(result) {
								if (result['success'])
								{
									Sync.Api.getall(type, callback, force);
								}
							});
							return;
						}
						if (result['status'] == 'OK') {
							ret = result;
						}
						if (result['modified'])
						{
							localStorage[type + 'OnServerlastModified'] = result['modified'];
						}
						if (result['data'])
						{
							result = JSON.parse(result['data']);
							ret = result;
						}
					}
					catch (e) {}
					callback && callback(ret);
				}, function onerror(result) {
					callback && callback();
				});
			});
		},
		setall: function(type, data, callback) {
			Sync.updateStorageData(function() {
				if (!Sync.enabled || !Sync.userOpinionForEnabling)
				{
					callback && callback({error: 'disabled'}); 
					return;
				}

				SendPostDataTo(Sync.syncServerUrl + 'setall', {
					'email': Sync.email,
					'type': type,
					'data': data
				}, {}, function onload(result) {
					try
					{
						result = JSON.parse(result);
						if (result['error'] == 'access denied')
						{
							Sync.authorize(function(result) {
								if (result['success'])
								{
									Sync.Api.setall(type, data, callback);
								}
							});
							return;
						}
						if (result['lastModified'])
						{
							localStorage[type + 'OnServerlastModified'] = result['lastModified'];
						}
						callback && callback(result);
					}
					catch(e) {

					}
				}, function onerror(result) {
					callback && callback(result);
				});
			});
		},
		modifyNodes: function(type, data, callback) {
			Sync.updateStorageData(function() {
				if (!Sync.enabled || !Sync.userOpinionForEnabling)
				{
					callback && callback({error: 'disabled'}); 
					return;
				}

				SendPostDataTo(Sync.syncServerUrl + 'modifyNodes', {
					'email': Sync.email,
					'type': type,
					'data': data
				}, {}, function onload(result) {
					try {
						result = JSON.parse(result);
						if (result['error'] == 'access denied')
						{
							Sync.authorize(function(result) {
								if (result['success'])
								{
									Sync.Api.modifyNodes(type, data, callback);
								}
							});
							return;
						}
						if (result['modified'])
						{
							localStorage[type + 'OnServerlastModified'] = result['modified'];
							callback && callback({success: true});
						}
						else 
						{
							callback && callback({error: 'cant modify'});
						}
					}
					catch(e) {
						callback && callback({error: true});
					}
				}, function onerror(result) {
					callback && callback({error: 'timeout'});
				});
			});
		},
		sendSubscriptions: function(callback) {
			if (!window.InAppPayments)
			{
				return;
			}
			Sync.updateStorageData(function() {
				if (!Sync.enabled || !Sync.userOpinionForEnabling)
				{
					callback && callback({error: 'disabled'}); 
					return;
				}

				InAppPayments.getAllGoods(function(goods, error) {
					var subscriptions = [];
					var paid = false;
					for (var i = 0; i < goods.length; i++)
					{
						if (goods[i].buyed)
						{
							subscriptions.push(goods[i]);
							paid = true;
						}
					}
					
					SendPostDataTo(Sync.syncServerUrl + 'stat', {
						'email': Sync.email,
						'subscriptions': JSON.stringify(subscriptions),
						'paid': paid
					}, {}, function onload(result) {
						try
						{
							result = JSON.parse(result);
							if (result['error'] == 'access denied')
							{
								Sync.authorize(function(result) {
									if (result['success'])
									{
										Sync.Api.sendSubscriptions(callback);
									}
								});
								return;
							}
							callback && callback(result);
						}
						catch(e) {

						}
					}, function onerror(result) {
						callback && callback(result);
					});
				});
			});
		}
	}
}