function normolizeUrl(url) {
	if (typeof url == 'string' && url.length > 0) {
		if (url[url.length - 1] == '/') url = url.substring(0, url.length - 1);
		return decodeURIComponent(url);
	}
	else {
		return url;
	}
}

function CurationFolders() {
	var _this = this;
	Base.apply(_this, arguments);
	
    this.is_chrome_syncing = false;
	this.is_syncing = false;
	this.is_server_syncing = false;
	this.flag_for_create = 0;
	this.asyncChanges = {};
	this.temp_urls_list = {};
	this.publishers = {'default': {}};
	this.authors = {'default': {}};
	this.twitters = {};
	this.tags = {};
	this.temp_node = [];
	this.create_temp_queue = [];
	this.temp_sync_nodes = [];
	this.temp_system_nodes = {};
	this.temp_system_nodes_titles = {};
	this.temp_remove_meta_info = {};
	var syncTimeout;
	this.nodes = {
		"FC0D11DB66BEC32A591F12600FF27A57": {
			"title": "Kurator",
			"type": "curation_folder",
			"guid": "FC0D11DB66BEC32A591F12600FF27A57",
			"source_id": "CurationFolders",
			"text": "Kurator",
			"children": {
				"8426379939367A904D94FD633D48BAD2": {
					"title": "Kurator Forum - Optimal Access",
					"url": "https://optimalaccess.com/forums/forum/kurator-forum/",
					"source_id": "CurationFolders",
					"text": "Kurator Forum - Optimal Access",
					"description": "Start here if you want answers about the Kurator and its features. &nbsp;As a new user you can add new links to this folder and see how it works. To add more folders, or to share folders and folder trees with other users, you need to buy a license.&nbsp;",
					"publisher": "Optimal Access",
					"twitter": "@kbavandi",
					"image_url": "https://optimalaccess.com/wp-content/uploads/2016/01/logo.png ",
					"author": "Karan Bavandi",
					"pub_date": 1499324400000,
					"article_type": "article",
					"tags": ["Kurator", "forum"],
					"rating": null,
					"type": "curation_link",
					"guid": "8426379939367A904D94FD633D48BAD2",
					"icon": "https://www.google.com/s2/favicons?domain=https://optimalaccess.com/forums/forum/kurator-forum/",
					"li_attr": {
						"class": "curation_link"
					},
				//	"view_index": 0,
					"state": {
						"hidden": true
					},
					"children": [],
					"parent": "FC0D11DB66BEC32A591F12600FF27A57"
				}
			}
		}
	};
	
	var saveToIndexedDB = true;

	this.addAdditionalData = function(node, from_empty) {
		if (node.type == Types.curation_link.id)
		{
			node.url && (_this.temp_urls_list[normolizeUrl(node.url)] = node.id);
		}
	
		if (!from_empty)
		{
			return;
		}
		
		if (node.parentId)
		{
			if (node.publisher)
			{
				if (!_this.publishers[node.parentId])
				{
					_this.publishers[node.parentId] = {};
				}
				_this.publishers[node.parentId][node.publisher] = true;
			}
			if (node.author)
			{
				if (!_this.authors[node.parentId])
				{
					_this.authors[node.parentId] = {};
				}
				if (_this.authors[node.parentId][node.author] === undefined)
				{
					_this.authors[node.parentId][node.author] = true;
				}
				if (node.twitter && _this.authors[node.parentId][node.author] != node.twitter)
				{
					_this.authors[node.parentId][node.author] = node.twitter;
					var p_node = this.getNodeById(node.parentId);
					for (var i in p_node.children)
					{
						if (p_node.children[i].author === node.author && p_node.children[i].twitter != node.twitter)
						{
							p_node.children[i].twitter = node.twitter;
							this.run_listeners("onChanged", {node: p_node.children[i].id, info: {twitter: node.twitter}, bg_changes: true});
						}
					}
				//	this.run_listeners("onChanged", {node: p_node.id, info: this.objToArray(p_node)});
				}
			}
			if (node.twitter)
			{
				if (!_this.twitters[node.parentId])
				{
					_this.twitters[node.parentId] = {};
				}
				_this.twitters[node.parentId][node.twitter] = true;
			}
			
			if (node.tags && Array.isArray(node.tags)) 
			{
				if (!_this.tags[node.parentId])
				{
					_this.tags[node.parentId] = {};
				}
				for (var i = 0; i < node.tags.length; i++)
				{
					_this.tags[node.parentId][node.tags[i]] = true;
				}
			}
		}
		if (node.children)
		{
			for (var ch_id in node.children)
			{
				this.addAdditionalData(node.children[ch_id], from_empty);
			}
		}
	}
	this.removeAdditionalData = function(node) {
		node.url && delete _this.temp_urls_list[normolizeUrl(node.url)];
		return;
		if (node.type == Types.curation_link.id)
		{
			var par = _this.getNodeById(node.parentId);
			var savePub, saveAut, saveTwi, saveTag = {};
			for (var ch in par.children)
			{
				if (ch == node.id) continue;
				if (node.publisher && par.children[ch].publisher == node.publisher)
				{
					savePub = true;
				}
				if (node.author && par.children[ch].author == node.author)
				{
					saveAut = true;
				}
				if (node.twitter && par.children[ch].twitter == node.twitter)
				{
					saveTwi = true;
				}
				if (par.children[ch].tags && node.tags) for (var i = 0; i < node.tags.length; i++)
				{
					if (par.children[ch].tags.indexOf(node.tags[i]) >= 0)
					{
						saveTag[node.tags[i]] = true;
					}
				}
			}
			
			!savePub && node.publisher && _this.publishers[par.id] && delete _this.publishers[par.id][node.publisher];
			!saveAut && node.author && _this.authors[par.id] && delete _this.authors[par.id][node.author];
			!saveTwi && node.twitter && _this.twitters[par.id] && delete _this.twitters[par.id][node.twitter];
			if (node.tags) for (var i = 0; i < node.tags.length; i++)
			{
				if (_this.tags[par.id] && !saveTag[node.tags[i]])
				{
					delete _this.tags[par.id][node.tags[i]];
				}
			}
		}
		else 
		{
			if (node.type == Types.curation_folder.id)
			{
				delete _this.publishers[node.id];
				delete _this.authors[node.id];
				delete _this.twitters[node.id];
				delete _this.tags[node.id];
			}
			if (node.children)
			{
				for (var ind in node.children)
				{
					this.removeAdditionalData(node.children[ind]);
				}
			}
		}
	}
	
	this.nodes_add = function(node, prior, no_callback) {
		!prior && this.node_to_view_json(node);
		var parent;
		var parentPath;
		if (!node.parentId)
		{
			parent = this.getParentNodeById(node.id, true);
			parentPath = parent.path;
			parent = parent.node;
		}
		if (!parent && node.parentId == CurationFolders.curation_toolbar_id)
		{
			parent = {children: this.nodes};
			parentPath = [];
		}
		if (!parent)
		{
			parent = this.getNodeById(node.parentId, true);
			parentPath = parent.path;
			parent = parent.node;
		}
		if (!parent)
		{
		//	console.log(node);
		}
		if (!parent.children[node.id])
		{
			for (var i in parent.children)
			{
				if (parent.children[i].index >= node.index)
				{
					parent.children[i].index++;
				}
			//	parent.children[i].view_index++;
			}
		//	node.view_index = 0;
			parent.children[node.id] = node;
			this.syncServer.add(parentPath.concat(node.title), node);
			parent.selected = 0;
			parent.sort = null;
//			this.run_listeners("onChanged", {node: parent.id, info: parent});
			this.addAdditionalData(node);
		}
		else
		{
			var exist_node = parent.children[node.id];
			var current_guid = exist_node.guid;
			var current_title = exist_node.title;
			this.removeAdditionalData(exist_node);
			if (prior)
			{
				for (var key in node)
				{
					if (key == 'text')
						exist_node['title'] = node[key];
					else if (key == 'title')
						exist_node['text'] = node[key];
					else if (key == 'twitter' && exist_node[key] != node[key])
					{
						for (var ch_id in parent.children)
						{
							if (parent.children[ch_id]['author'] && parent.children[ch_id]['author'] == node['author'])
							{
								parent.children[ch_id]['twitter'] = node['twitter'];
								this.run_listeners("onChanged", {node: ch_id, info: {twitter: node['twitter']}, bg_changes: true});
							}
						}
					}
					if (key != 'children')
						exist_node[key] = node[key];
				}
				node = exist_node;
			}
			else
			{
				for (var key in exist_node)
				{
					node[key] = exist_node[key];
				}
			}
			node.dateAdded = Date.now().toString();
			var new_guid = node.guid;
			this.addAdditionalData(node);
			!no_callback && this.syncServer.change(parentPath.concat(current_title), node);
			!no_callback && this.run_listeners("onChanged", {node: node.id, info: this.objToArray(node)});
			if (node.type == Types.curation_folder.id && new_guid != current_guid)
			{
				_this.addLinkToOpenFolder(node);
			}
		}
		if (node.type == Types.curation_link.id || node.type == Types.web_link.id)
		{
			node.state = {};
			node.state.hidden = true;
		}
		this.save();
	//	!no_callback && this.saveOnServer();
		return node;
	}
	this.nodes_remove = function(node) {
		var title = node.title;
		var parent = this.getParentNodeById(node.id, true);
		if (!parent)
		{
			return;
		}
		var parentPath = parent.path;
		parent = parent.node;
		this.removeAdditionalData(parent.children[node.id]);
	/*	for (var i in parent.children)
		{
			if (parent.children[i].view_index > parent.children[node.id].view_index)
			{
				parent.children[i].view_index--;
			}
		}*/
		delete parent.children[node.id];
		this.syncServer.remove(parentPath.concat(title));
		this.save();
	//	this.saveOnServer();
	}
	this.nodes_move = function(id, parentId, index) {
		var parent = this.getParentNodeById(id, true);
		if (!parent)
		{
			return;
		}
		var parentPath = parent.path;
		parent = parent.node;
		if (parent.children && parent.children[id] && parent.children[id].type == Types.curation_link.id)
        {
            this.removeAdditionalData(parent.children[id]);
        }
		var title = parent.children[id].title;
		var cloned_node = JSON.parse(JSON.stringify(parent.children[id]));
		var last_index = cloned_node.index;
		cloned_node.index = index;
		cloned_node.parentId = parentId;
		var parent2 = this.getNodeById(parentId, true);
		var parent2Path = parent2.path;
		parent2 = parent2.node;
		for (var i in parent2.children)
		{
			if (parent2.children[i].index > index)
			{
				parent2.children[i].index++;
			}
			else if (parent2.children[i].index == index)
			{
				if (parent.id != parent2.id || last_index > index)
				{
					parent2.children[i].index++;
				}
			}
		}
		delete parent.children[id];
		for (var i in parent.children)
		{
			if (parent.children[i].index > last_index)
			{
				parent.children[i].index--;
			}
		}
		if (parent == parent2 && cloned_node.index <= last_index)
		{
			cloned_node.index++;
		}
		parent2.children[id] = cloned_node;
		this.syncServer.move(parentPath.concat(title), parent2Path.concat(title));
		this.addAdditionalData(parent2.children[id]);
		this.save();
	//	this.saveOnServer();
	}
	
	this.node_to_view_json = function(node) {
		if (!node.guid)
		{
			node.guid = this.guid();
		}
		node.source_id = CurationFolders._id;
		if (node.title)
		{
			node.text = node.title;
		}
		if (node.url)
		{
			if (node.url.indexOf('http') == 0)
			{
				node.icon = 'https://www.google.com/s2/favicons?domain=' + node.url;
			}
		}
		else
		{
			if (!node.children)
			{
				node.children = {};
			}
		}
		node.li_attr = {'class': node.type};
		for (var i in node.children)
		{
			this.node_to_view_json(node.children[i]);
		}
	}
	
	this.getNodeById = function(id, path) {
		return this.getNodeBy('id', id, path);
	}
	this.getNodeBy = function(searcher, id, path) {
		function getNodeByIdFromNode(id, node, pathMas) {
			if (id == node[searcher])
			{
				pathMas.push(node.title);
				if (pathMas.length && pathMas[0] == CurationFolders.title_for_bookmarks)
				{
					pathMas.shift();
				}
				return path ? {node: node, path: pathMas} : node;
			}
			else if (node.children)
			{
				pathMas.push(node.title);
				for (var i in node.children)
				{
					var finded = getNodeByIdFromNode(id, node.children[i], Array.from(pathMas));
					if (finded)
					{
						return finded;
					}
				}
			}
			else 
			{
				return undefined;
			}
		}
		
		return getNodeByIdFromNode(id, {
			id: CurationFolders.curation_toolbar_id,
			last_id: CurationFolders.last_curation_toolbar_id,
			type: Types.placeholder_folder.id,
			title: CurationFolders.title_for_bookmarks,
			children: this.nodes,
			guid: CurationFolders.guid
		}, []);
	}
	this.getParentNodeById = function(id, path) {
		function getParentByIdInNode(id, node, pathMas) {
			if (!node.children)
			{
				return undefined;
			}
			if (!node.children[id])
			{
				pathMas.push(node.title);
				for (var i in node.children)
				{
					var finded = getParentByIdInNode(id, node.children[i], Array.from(pathMas));
					if (finded)
					{
						return finded;
					}
				}
			}
			else 
			{
				pathMas.push(node.title);
				if (pathMas.length && pathMas[0] == CurationFolders.title_for_bookmarks)
				{
					pathMas.shift();
				}
				return path ? {node: node, path: pathMas} : node;
			}
		}
		
		return getParentByIdInNode(id, {
			id: CurationFolders.curation_toolbar_id,
			children: this.nodes,
			last_id: CurationFolders.last_curation_toolbar_id,
			type: Types.placeholder_folder.id,
			title: CurationFolders.title_for_bookmarks,
			guid: CurationFolders.guid
		}, []);
	}
	
	var local_saving_timeout = null;
	this.save = function() {
		if (local_saving_timeout)
		{
			clearTimeout(local_saving_timeout);
			local_saving_timeout = null;
		}
		
		local_saving_timeout = setTimeout(function() {
			if (saveToIndexedDB)
			{
				IndexedDB.set(CurationFolders._id, JSON.stringify(_this.nodes), function() {}, function() {
					console.error('error saving in IndexedDB');
				});
			}
			else
			{
				var save_data = {};
				save_data[CurationFolders._id] = JSON.stringify(this.nodes);
				chrome.storage.local.set(save_data);
			}
		}, 1000);
	}
	this.saveOnServer = function(callback) {
		if (this.is_syncing || this.is_chrome_syncing || this.is_server_syncing)
		{
			return;
		}
		if (!Sync.enabled)
		{
			return;
		}
		function loop(nodeFrom, nodeTo) {
			for (var key in nodeFrom)
			{
				if (key != 'children')
				{
					nodeTo[key] = nodeFrom[key];
				}
			}
			if (nodeFrom.children)
			{
				nodeTo.children = {};
				for (var i in nodeFrom.children)
				{
					nodeTo.children[nodeFrom.children[i].title] = {};
					loop(nodeFrom.children[i], nodeTo.children[nodeFrom.children[i].title]);
				}
			}
		}
		var tempTree = {children: {}};
		loop({children:this.nodes}, tempTree);
		
		_log('setall');
		Sync.Api.setall('folders', JSON.stringify(tempTree));
	}
	
	var saved_stack = [];
	try {
		saved_stack = JSON.parse(localStorage['saved_stack'] || '[]');
	}
	catch(e) {}
	this.syncServer = {
		stack: saved_stack,
		sending: false,
		sendTimeout: null,
		clearStack: function() {
			_this.syncServer.stack = [];
			localStorage['saved_stack'] = JSON.stringify(_this.syncServer.stack);
		},
		add: function(path, node) {
			_log('server add | is_syncing=' + _this.is_syncing + ' | is_chrome_syncing=' + _this.is_chrome_syncing + ' | is_server_syncing=' + _this.is_server_syncing);
			if (_this.is_syncing || _this.is_chrome_syncing || _this.is_server_syncing)
			{
				return;
			}
			var node = JSON.parse(JSON.stringify(node));
			delete node.children;
			this.stack.push({
				action: 'add',
				info: {
					path: path,
					node: node
				}
			});
			this.startSend();
		},
		change: function(path, node) {
			_log('server change | is_syncing=' + _this.is_syncing + ' | is_chrome_syncing=' + _this.is_chrome_syncing + ' | is_server_syncing=' + _this.is_server_syncing);
			if (_this.is_syncing || _this.is_chrome_syncing || _this.is_server_syncing)
			{
				return;
			}
			var node = JSON.parse(JSON.stringify(node));
			delete node.children;
			this.stack.push({
				action: 'change',
				info: {
					path: path,
					node: node
				}
			});
			this.startSend();
		},
		remove: function(path) {
			_log('server remove | is_syncing=' + _this.is_syncing + ' | is_chrome_syncing=' + _this.is_chrome_syncing + ' | is_server_syncing=' + _this.is_server_syncing);
			if (_this.is_syncing || _this.is_chrome_syncing || _this.is_server_syncing)
			{
				return;
			}
			this.stack.push({
				action: 'remove',
				info: {
					path: path
				}
			});
			this.startSend();
		},
		move: function(pathFrom, pathTo) {
			_log('server move | is_syncing=' + _this.is_syncing + ' | is_chrome_syncing=' + _this.is_chrome_syncing + ' | is_server_syncing=' + _this.is_server_syncing);
			if (_this.is_syncing || _this.is_chrome_syncing || _this.is_server_syncing)
			{
				return;
			}
			this.stack.push({
				action: 'move',
				info: {
					pathFrom: pathFrom,
					pathTo: pathTo
				}
			});
			this.startSend();
		},
		startSend: function() {
			if (this.sendTimeout)
			{
				clearTimeout(this.sendTimeout);
			}
			this.sendTimeout = setTimeout(this.send, 1000);
		},
		send: function() {
			if (_this.syncServer.stack.length > 0 && !_this.syncServer.sending && !_this.is_loading && !_this.is_syncing && !_this.is_chrome_syncing && !_this.is_server_syncing)
			{
				var count = _this.syncServer.stack.length;
				_this.syncServer.sending = true;
				_log('start modifyNodes | is_syncing=' + _this.is_syncing + ' | is_chrome_syncing=' + _this.is_chrome_syncing + ' | is_server_syncing=' + _this.is_server_syncing);
				Sync.Api.modifyNodes('folders', JSON.stringify(_this.syncServer.stack), function(result) {
					_this.syncServer.sending = false;
					if (result.success)
					{
						_this.syncServer.stack.splice(0, count);
						localStorage['saved_stack'] = JSON.stringify(_this.syncServer.stack);
						_this.syncServer.startSend();
					}
					else if (result.error == 'cant modify')
					{
						_this.syncServer.clearStack();
						_this.saveOnServer();
					}
				});
			}
			else if (_this.syncServer.stack.length > 0 ) {
				_this.syncServer.startSend();
			}
		}
	};
	
	this.addLinkToOpenFolder = function(node) {
		if (!node)
		{
			return;
		}
		var _this = this;
		var exist = false;
		var url = 'chrome://bookmarks/#select|';
		if (node.id == CurationFolders.curation_toolbar_id || !node.guid)
		{
			url += node.id;
		}
		else 
		{
			url += node.guid;
		}
		chrome.bookmarks.getSubTree(node.id, function(b_node) {
			if (chrome.runtime.lastError)
			{
				console.error('error');
				return;
			}
			if (b_node && b_node[0] && b_node[0].children)
			{
				for (var i = 0; i < b_node[0].children.length; i++)
				{
					if (b_node[0].children[i].url && b_node[0].children[i].url.indexOf('chrome://bookmarks/#select|') === 0)
					{
						if (exist || b_node[0].children[i].url != url)
						{
							!_this.listeners_stoped && (_this.temp_system_nodes[b_node[0].children[i].id] = true);
							chrome.bookmarks.remove(b_node[0].children[i].id, function() {
								if (chrome.runtime.lastError)
								{
									_this.addLinkToOpenFolder(node);
								}
							});
						}
						else 
						{
							exist = true;
						}
					}
				}
			}
			if (!exist && (node.type == Types.curation_folder.id || node.parentId == undefined || node.parentId == '1'))
			{
				var cur_title = node.title;
				var match = cur_title.match(/(.+) \(\d+\)$/);
				if (match && match.length > 1)
				{
					cur_title = match[1];
				}
			// 	this.flag_for_create++;
				chrome.bookmarks.create({
					parentId: node.id,
					index: 0,
					title: '[ ' + Local['open'] + ' folder ]',
					url: url
				});
			}
		});
	}
	this.checkLinkToOpenFolder = function(url, id) {
		return (url && url.indexOf('chrome://bookmarks/#select|' + (id ? id : '')) == 0) || null;// + id;
	}
	
	this.uniquelyName = function(node) {
		var parent = this.getNodeById(node.parentId);
		if (parent)
		{
			var ind = 0;
			var cur_title = node.title;
			var match = cur_title.match(/(.+) \(\d+\)$/);
			if (match && match.length > 1)
			{
				cur_title = match[1];
			}
			var new_title = cur_title;
			var brake = false;
			while (!brake)
			{
				brake = true;
				for (var i in parent.children)
				{
					if (parent.children[i].title == new_title && parent.children[i].guid != node.guid && 
					   Types[parent.children[i].type].subtype == Types[node.type].subtype)
					{
						new_title = cur_title + ' (' + ind + ')';
						ind++;
						brake = false;
						break;
					}
				}
			}
			if (node.title != new_title)
			{
				node.title = new_title;
				node.text = new_title;
				chrome.bookmarks.update(node.id, {
					title: new_title
				});
			}
		}
	}

	this.updateInfoInTitle = function(node) {
		if (node.id)
		{
			var info_node = JSON.parse(JSON.stringify(node));
			delete info_node.children;
			delete info_node.parentId;
			delete info_node.id;
			delete info_node.title;
			delete info_node.url;
			delete info_node.source_id;
			this.temp_system_nodes_titles[node.id] = true;
			chrome.bookmarks.update(node.id, {
				title: node.text + " " + this.MyCodec.unicodeToQuinary(JSON.stringify(info_node))
			});
		}
	}
	
	this.temp_node_pop = function() {
		this.temp_node.pop();
		while (this.temp_node.length > 0)
		{
			latest_tmp_node = this.temp_node[this.temp_node.length - 1];
			latest_tmp_node.children.splice(0, 1);
			if (latest_tmp_node.children && latest_tmp_node.children.length > 0)
			{
				this.temp_node.push(latest_tmp_node.children[0]);
				this.bookmarks_create(latest_tmp_node.children[0]);
				break;
			}
			else
			{
				this.temp_node.pop();
			}
		}
		if (this.temp_node.length <= 0)
		{
			this.create();
		}
	}
	this.check_temp_node_is_empty = function() {
		for (var i = 0; i < this.temp_node.length; i++)
		{
			if (this.temp_node[i].id)
			{
				return false;
			}
		}
		this.temp_node = [];
		return true;
	}
	
	function removeUnnecessaryFromBookmarks(synced, bookmarks) {
		for (var i in bookmarks)
		{
			if (_this.checkLinkToOpenFolder(bookmarks[i].url))
			{
				delete bookmarks[i];
				continue;
			}
			var existing_source_node_id = null;
			if (bookmarks[i])
			{
				existing_source_node_id = i;
			}
			else
			{
				for (var s_id in bookmarks)
				{
					if (normolizeUrl(synced[i].url) == normolizeUrl(bookmarks[s_id].url) && synced[i].title == bookmarks[s_id].title)
					{
						existing_source_node_id = s_id
						break;
					}
				}
			}
			
			if (!existing_source_node_id)
			{
				chrome.bookmarks.remove(i);
				delete bookmarks[i];
			}
			else if (bookmarks[i].children)
			{
				removeUnnecessaryFromBookmarks(synced[existing_source_node_id] || {}, bookmarks[i].children);
			}
		}
	}
	
	function findByTitleIn(name, nodes) {
		
	}
	this.mergeCurrentToSynced = function(current, synced) {
		for (var i in current)
		{
			if (_this.checkLinkToOpenFolder(current[i].url))
			{
				delete current[i];
				continue;
			}
			var existing_source_node_id = null;
			if (synced[i])
			{
				existing_source_node_id = i;
			}
			else
			{
				for (var s_id in synced)
				{
					if ((current[i].guid && current[i].guid == synced[s_id].guid) || 
						(normolizeUrl(synced[s_id].url) == normolizeUrl(current[i].url) && synced[s_id].title == current[i].title))
					{
						existing_source_node_id = s_id
						break;
					}
				}
			}
			if (!existing_source_node_id)
			{
			//	this.increaseAsyncChanges(current[i].id);
			//	this.remove(current[i]);
				if (Types[current[i].type].subtype == 'link')
				{
					chrome.bookmarks.remove(current[i].id, function() {
						if (chrome.runtime.lastError)
						{
							console.error(chrome.runtime.lastError);
						}
					});
				}
				else
				{
					chrome.bookmarks.removeTree(current[i].id, function() {
						if (chrome.runtime.lastError)
						{
							console.error(chrome.runtime.lastError);
						}
					});
				}
			/*	var existed = findByTitleIn(current[i].title, synced[i]);
				if (existed)
				{
					existed.id = current[i].id;
					existed.index = current[i].index;
					if (existed.children)
					{
						for (var s_id in existed.children)
						{
							existed.children[s_id].parentId = existed.id;
						}
					}
					var oldDate = current[i].lastupdated || 0;
					var newDate = existed.lastupdated || 0;
					if (newDate !== oldDate || oldDate == 0)
					{
						this.increaseAsyncChanges(existed.id);
						this.change(existed);
					}
					if (existed.children)
					{
						this.mergeCurrentToSynced(existed.children, current[i].children || {});
					}
				}*/
			}
			else if (current[i].children)
			{
				this.mergeCurrentToSynced(current[i].children, synced[existing_source_node_id].children || {});
			}
		}
	}
	this.asyncChanges = 0;
	this.increaseAsyncChanges = function(id) {
	//	this.asyncChanges[id] = true;
		
	/*	if (_this.is_syncing)
		{
			this.asyncChanges++;
		}*/
	//	console.log(this.asyncChanges, _this.is_syncing);
	}
	this.decreaseAsyncChanges = function(id) {
	/*	if (this.asyncChanges[id])
		{
			delete this.asyncChanges[id]
			if (Object.keys(this.asyncChanges).length == 0)
			{
				_this.is_syncing = false;
			}
		}*/
		
	/*	this.asyncChanges--;
		if (this.asyncChanges <= 0)
		{
			this.asyncChanges = 0;
			_this.is_syncing = false;
			_this.run_listeners("onLoadingEnd");
		}*/
	//	console.log(this.asyncChanges, _this.is_syncing);
	}
	this.mergeSyncedToCurrent = function(synced, current) {
		for (var i in synced)
		{
			var existing_source_node_id = null;
			if (current[i])
			{
				existing_source_node_id = i;
			}
			else
			{
				for (var s_id in current)
				{
					if ((current[s_id].guid && current[s_id].guid == synced[i].guid) || 
						(normolizeUrl(synced[i].url) == normolizeUrl(current[s_id].url) && synced[i].title == current[s_id].title))
					{
						existing_source_node_id = s_id
						break;
					}
				}
			}
			if (!existing_source_node_id)
			{
			/*	var existed = this.getNodeBy('guid', synced[i].guid);
				if (existed)
				{
					this.increaseAsyncChanges(existed.id);
					this.move(existed, {'pos': 'into', 'id': synced[i].parentId});
				}
				else
				{
					synced[i].index = 1;
					this.create(synced[i]);
				}*/
			}
			else
			{
				synced[i].id = existing_source_node_id;
				synced[i].index = current[existing_source_node_id].index;
				if (synced[i].children)
				{
					for (var s_id in synced[i].children)
					{
						synced[i].children[s_id].parentId = existing_source_node_id;
					}
				}
				var oldDate = current[existing_source_node_id].lastupdated || 0;
				var newDate = synced[i].lastupdated || 0;
				
				var change_type = false;
				function applyRealTypeForLink(node) {
					if (synced[i].type == Types.web_link.id && node.rating !== undefined) {
						node.type = Types.curation_link.id;
						node.li_attr = {
							class: Types.curation_link.id
						};
						node.state = {};
						return true;
					}
					return false;
				}
				if (synced[i].type == Types.placeholder_folder.id && current[existing_source_node_id].type == Types.placeholder_folder.id) {
					for (var ch_id in synced[i].children) {
						if (synced[i].children[ch_id].rating !== undefined) {
							synced[i].type = Types.curation_folder.id;
							synced[i].li_attr = {
								class: Types.curation_folder.id
							};
							synced[i].state = {};
							change_type = true;
							break;
						}
					}
				}
				else if (current[existing_source_node_id].type == Types.web_link.id) {
					change_type = applyRealTypeForLink(synced[i]);
				}
				
				if (newDate !== oldDate || oldDate == 0 || change_type)
				{
					if (synced[i].article_type != current[existing_source_node_id].article_type ||
					   synced[i].author != current[existing_source_node_id].author ||
					   synced[i].description != current[existing_source_node_id].description ||
					   synced[i].guid != current[existing_source_node_id].guid ||
					   synced[i].icon != current[existing_source_node_id].icon ||
					   synced[i].image_url != current[existing_source_node_id].image_url ||
					   synced[i].pub_date != current[existing_source_node_id].pub_date ||
					   synced[i].publisher != current[existing_source_node_id].publisher ||
					   synced[i].rating != current[existing_source_node_id].rating ||
					   synced[i].text != current[existing_source_node_id].text ||
					   synced[i].title != current[existing_source_node_id].title ||
					   synced[i].twitter != current[existing_source_node_id].twitter ||
					   synced[i].comment != current[existing_source_node_id].comment ||
					   synced[i].url != current[existing_source_node_id].url ||
					   change_type ||
					   JSON.stringify(synced[i].tags) != JSON.stringify(current[existing_source_node_id].tags))
					{
						!change_type && applyRealTypeForLink(synced[i]);
						_this.increaseAsyncChanges(synced[i].id);
						_this.disable_is_server_syncing();
						_this.change(synced[i]);
					}
				}
				if (synced[i].children)
				{
					this.mergeSyncedToCurrent(synced[i].children, current[existing_source_node_id].children || {});
				}
			}
		}
	}
	
	this.dontsync_timeout;
	this.disable_is_server_syncing = function() {
		if (_this.dontsync_timeout)
		{
			clearTimeout(_this.dontsync_timeout);
		}
		_this.dontsync_timeout = setTimeout(function() {
			_this.is_server_syncing = false;
			_log('is_server_syncing = false');
		}, 500);
	}
	this.startSync = function(force, callback) {
		if (_this.is_syncing || _this.is_loading) return;
		
		_this.is_loading = true;
		_log('is_loading = true');
		Sync.Api.getall('folders', function(result) {
			_this.is_loading = false;
			_log('is_loading = false');
			if (result && result.children)
			{
				_this.is_server_syncing = true;
				_log('is_server_syncing = true');
				for (var i in result.children)
				{
					result.children[i].parentId = CurationFolders.curation_toolbar_id;
				}
			//	_this.run_listeners("onLoadingStart");
				_this.mergeSyncedToCurrent(result.children, _this.nodes);
				_this.disable_is_server_syncing();
				if (force)
				{
				//	_this.mergeCurrentToSynced(_this.nodes, result.children);
				}
				callback && callback({status: 'sync'});
			}
			else if (result && result.status == 'OK') {
				callback && callback(result);
			}
			
			if (syncTimeout)
			{
				clearTimeout(syncTimeout);
			}
			syncTimeout = setTimeout(function() {
				_this.startSync();
			}, Sync.syncTimeoutDelayFolders);
		}, force);
	}
	
	this.stopSync = function() {
        if (syncTimeout)
		{
			clearTimeout(syncTimeout);
		}
	}
    
	function mergeBookmarksToCurationNodes(bookmarkNodes, sourceNodes) {
		for (var i in bookmarkNodes)
		{
			if (_this.checkLinkToOpenFolder(bookmarkNodes[i].url))
			{
				delete bookmarkNodes[i];
				continue;
			}
			var existing_source_node_id = null;
			if (sourceNodes[i])
			{
				existing_source_node_id = i;
			}
			else
			{
				for (var s_id in sourceNodes)
				{
					if (normolizeUrl(bookmarkNodes[i].url) == normolizeUrl(sourceNodes[s_id].url) && sourceNodes[s_id].title == bookmarkNodes[i].title)
					{
						existing_source_node_id = s_id
						break;
					}
				}
			}
			if (!existing_source_node_id)
			{
				_this.node_to_view_json(bookmarkNodes[i], true);
				var tmp = JSON.parse(JSON.stringify(bookmarkNodes[i]));
				if (tmp.url) 
				{
				/*	var par = _this.getNodeById(tmp.parentId);
					if (par && par.type == Types.curation_folder.id)
					{
						tmp.type = Types.curation_link.id;
					}
					else*/
					{
						tmp.type = Types.web_link.id;
					}
					tmp.state = {};
					tmp.state.hidden = true;
				}
				else
				{
				/*	var is_curation = false;
					for (var tmp_ch in tmp.children)
					{
						if (_this.checkLinkToOpenFolder(tmp.children[tmp_ch].url))
						{
							is_curation = true;
							break;
						}
					}
					if (is_curation)
					{
						tmp.type = Types.curation_folder.id;
					//	_this.addLinkToOpenFolder(tmp);
					}
					else*/
					{
						tmp.type = Types.placeholder_folder.id;
					}
				}
				tmp.li_attr = {'class': tmp.type};
				tmp.children = {};
				sourceNodes[i] = tmp;
			}
			else if (sourceNodes[existing_source_node_id])
			{
				if (existing_source_node_id != i)
				{
					var tmp = JSON.parse(JSON.stringify(sourceNodes[existing_source_node_id]));
					delete sourceNodes[existing_source_node_id];
					sourceNodes[i] = tmp;
					sourceNodes[i].id = i;
				}
				if (sourceNodes[i].children) for (var ch_id in sourceNodes[i].children)
				{
					sourceNodes[i].children[ch_id].parentId = i;
				}
			}
			if (sourceNodes[i] && bookmarkNodes[i].children && sourceNodes[i].children)
			{
				mergeBookmarksToCurationNodes(bookmarkNodes[i].children, sourceNodes[i].children);
			}
		}
	}

	function createBookmarkAndChangeId(list, id, k, callback) {
		k.ind++;
		chrome.bookmarks.create({
			parentId: list[id].parentId,
		//	index: list[id].index,
			title: list[id].title,
			url: (list[id].type == Types.web_link.id || list[id].type == Types.curation_link.id) ? list[id].url : undefined
		}, function(node) {
			if (node)
			{
				list[id].last_id = list[id].id;
				list[id].id = node.id;
				list[id].index = node.index;
				list[node.id] = JSON.parse(JSON.stringify(list[id]));
				delete list[id];
				_this.addLinkToOpenFolder(list[node.id]);
				if (list[node.id].children)
				{
					for (var i in list[node.id].children)
					{
						list[node.id].children[i].parentId = list[node.id].id;
					}
					node.children = {};
					mergeCurationToBookmarksNodes(node.children, list[node.id].children, k, callback);
				}
			}
			callback();
		});
	}
	function mergeCurationToBookmarksNodes(bookmarkNodes, sourceNodes, k, callback) {
		var n = 0;
		for (var i in sourceNodes)
		{
			n++;
			if (!bookmarkNodes[i])
			{
				createBookmarkAndChangeId(sourceNodes, i, k, callback);
			}
			else 
			{
				k.ind++;
				chrome.bookmarks.getSubTree(i, function(node) {
					sourceNodes[node[0].id].index = node[0].index;
					if (sourceNodes[node[0].id].children && bookmarkNodes[node[0].id].children)
					{
						if (sourceNodes[node[0].id].type == Types.curation_folder.id)
						{
							_this.addLinkToOpenFolder(sourceNodes[node[0].id]);
						}
						mergeCurationToBookmarksNodes(bookmarkNodes[node[0].id].children, sourceNodes[node[0].id].children, k, callback);
					}
					callback();
				});
			}
		}
		if (n <= 0)
		{
			k.ind++;
			callback();
		}
	}

	function nodeWithTitle(nodes, title) {
		for (var i = nodes.length - 1; i >= 0; i--)
		{
			if (nodes[i].title == title && nodes[i].parentId == '1')
			{
				return nodes[i];
			}
			else if (nodes[i].children)
			{
				var node = nodeWithTitle(nodes[i].children, title);
				if (node)
				{
					return node;
				}
			}
		}
		return null;
	}

	function createCurationBookmark(bookmark, callback) {
		chrome.bookmarks.getTree(function(nodes) {
			var node = nodeWithTitle(nodes, bookmark.title)
			if (node)
			{
			//	chrome.bookmarks.removeTree(node.id);
				callback(node);
			}
			else
			{
				chrome.bookmarks.create(bookmark, callback);
			}
		});
	}

	function getCurationToolbarTree(callback) {
//		chrome.bookmarks.getSubTree(CurationFolders.curation_toolbar_id, function(nodes) {
//			if (!nodes)
//			{
		//		chrome.bookmarks.removeTree(nodes[0].id);
		//	}
				createCurationBookmark({
						title: CurationFolders.title_for_bookmarks,
						parentId: '1',
						index: 0
					}, function(node) {
						localStorage.curation_toolbar_id = node.id;
						for (var i in _this.nodes)
						{
							_this.nodes[i].parentId = node.id;
						}
						node.type = Types.curation_folder.id;
						_this.addLinkToOpenFolder(node);
						CurationFolders.last_curation_toolbar_id = CurationFolders.curation_toolbar_id;
						CurationFolders.curation_toolbar_id = localStorage.curation_toolbar_id;
						chrome.bookmarks.getSubTree(node.id, function(nodes) {
							callback(nodes[0]);
						});
					});
//			}
//			else
//			{
//				callback(nodes[0]);
//			}
//		});
	}

	function fillTempUrlsList() {
			function round(nodes)
			{
				for (var i in nodes)
				{
					_this.addAdditionalData(nodes[i]);
					nodes[i].children && round(nodes[i].children);
				}
			}
			round(_this.nodes);
		}

	this.init = function() {
		getCurationToolbarTree(function(curationTree) {
			var curationTree = _this.arrayToObj(curationTree.children);
			_this.nodes = _this.arrayToObj(_this.objToArray(_this.nodes));
			mergeBookmarksToCurationNodes(curationTree, _this.nodes);
			var k = {ind: 0};
			mergeCurationToBookmarksNodes(curationTree, _this.nodes, k, function() {
				k.ind--;
				if (k.ind == 0)
				{
					_this.listeners_stoped = false;
					fillTempUrlsList();
					_this.save();
					setTimeout(function(){
						_this.run_listeners("on_initalized");
					//	if (Sync.enabled)
						{
							_this.startSync(true, function(result) {
								if (result && result.status == 'OK') {
									_this.syncServer.startSend();
								}
								else {
									_this.syncServer.clearStack();
								}
							});
						}
					}, 200);
				}
			});
		});
	}
	
	this.start = function() {
		_log('start');
		this.is_chrome_syncing = false;
		this.is_syncing = false;
		this.is_server_syncing = false;
		this.is_loading = false;
		this.listeners_stoped = true;
		
		this.flag_for_create = 0;
		this.asyncChanges = {};
		this.temp_urls_list = {};
		this.publishers = {'default': {}};
		this.authors = {'default': {}};
		this.twitters = {};
		this.tags = {};
		this.temp_node = [];
		this.create_temp_queue = [];
		this.temp_sync_nodes = [];
		this.temp_system_nodes = {};
		this.temp_system_nodes_titles = {};
		this.temp_remove_meta_info = {};
		syncTimeout && clearTimeout(syncTimeout);
		syncTimeout = null;
		this.dontsync_timeout && clearTimeout(this.dontsync_timeout);
		this.dontsync_timeout = null;
		
		IndexedDB.get(CurationFolders._id, function(res) {
			_this.nodes = JSON.parse(res);
			_this.init();
		}, function() {
			chrome.storage.local.get(CurationFolders._id, function(res) {
				if (res[CurationFolders._id])
				{
					IndexedDB.set(CurationFolders._id, res[CurationFolders._id], function(e) {}, function() {
						saveToIndexedDB = false;
					});
					_this.nodes = JSON.parse(res[CurationFolders._id]);
				}
				_this.init();
			});
		});
	}
	
	this.start();
}
CurationFolders.prototype = Object.create(Base.prototype);
CurationFolders._id = 'CurationFolders';
CurationFolders.title_for_bookmarks = Local["curationFolderTitle"];
CurationFolders.curation_toolbar_id = localStorage.curation_toolbar_id || 'curation_toolbar_id';
CurationFolders.guid = 'BDADFC6568294AFFA32D8E904E733179';

window.canOpenChangePages = false;
setTimeout(function() {
	window.canOpenChangePages = true;
}, 3000);

CurationFolders.prototype.onCreated = function(id, bookmarkTreeNode, exist) {
	_log('onCreated | is_syncing=' + this.is_syncing + ' | is_chrome_syncing=' + this.is_chrome_syncing + ' | is_server_syncing=' + this.is_server_syncing);
	if (this.listeners_stoped)
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.is_chrome_syncing)
	{
		this.temp_sync_nodes.push({
			id: id,
			type: "create", 
			node: bookmarkTreeNode
		});
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.checkLinkToOpenFolder(bookmarkTreeNode.url))
	{
		this.decreaseAsyncChanges(id);
		this.addLinkToOpenFolder(this.getNodeById(bookmarkTreeNode.parentId));
		return;
	}
	//if (id == CurationFolders.curation_toolbar_id)
	if (bookmarkTreeNode.parentId == '1')
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	bookmarkTreeNode = this.arrayToObj(bookmarkTreeNode);
	var parent = this.getNodeById(bookmarkTreeNode.parentId);
	var prior;
	if (this.temp_node.length > 0)
	{
		var latest_tmp_node = this.temp_node[this.temp_node.length - 1];
		var need_update = true;
		if (latest_tmp_node.guid == bookmarkTreeNode.guid)
		{
			var curDate = bookmarkTreeNode.dateAdded;
			var newDate = latest_tmp_node.dateAdded;
			if (curDate && newDate && curDate < newDate)
			{
				prior = true;
			}
		}
		for (var key in latest_tmp_node)
		{
			if (key != 'id' && key != 'children' && (!exist || key != 'parentId'))
			{
				bookmarkTreeNode[key] = latest_tmp_node[key];
			}
		}
		if (parent && parent.type == Types.curation_folder.id)
		{
			bookmarkTreeNode.index++;
		}
		if (latest_tmp_node.children && latest_tmp_node.children.length > 0)
		{
			for (var i = 0; i < latest_tmp_node.children.length; i++)
			{
				latest_tmp_node.children[i].parentId = bookmarkTreeNode.id;
			}
			this.temp_node.push(latest_tmp_node.children[0]);
			this.bookmarks_create(latest_tmp_node.children[0]);
		} 
		else
		{
			this.temp_node_pop();
		}
	}

	function setType(node, parent) {
		if (!node.type)
		{
			if (parent && parent.type == Types.curation_folder.id)
			{
				if (node.url)
				{
					node.type = Types.curation_link.id;
				}
				else
				{
					node.type = Types.curation_folder.id;
				}
			}
			else
			{
				if (node.url)
				{
					node.type = Types.web_link.id;
				}
				else
				{
					node.type = Types.placeholder_folder.id;
				}
			}
		}
		if (node.children) for (var ch_id in node.children)
		{
			setType(node.children[ch_id], node);
		}
	}
	parent && setType(bookmarkTreeNode, parent);
	
	if (!bookmarkTreeNode.parentId)
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	
	this.uniquelyName(bookmarkTreeNode);
	var node = this.nodes_add(bookmarkTreeNode, prior);
	this.decreaseAsyncChanges(id);
	!exist && this.addLinkToOpenFolder(node);
	node = this.objToArray(node);
	this.run_listeners("onCreated", {node: node});
	var parent = this.getParentNodeById(node.id);
	this.run_listeners("onChanged", {node: parent.id, info:  this.objToArray(parent)});
	if (this.flag_for_create > 0)
	{
		this.flag_for_create--;
	}
	else if (bookmarkTreeNode.type == Types.curation_link.id && !this.is_chrome_syncing && !this.is_syncing && !this.is_server_syncing && !exist && window.canOpenChangePages)
	{
		this.showBookmarkPage("change|" + bookmarkTreeNode.id);
	}
}
CurationFolders.prototype.onRemoved = function(id, info) {
	_log('onRemoved | is_syncing=' + this.is_syncing + ' | is_chrome_syncing=' + this.is_chrome_syncing + ' | is_server_syncing=' + this.is_server_syncing);
	if (this.listeners_stoped)
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	
	if (this.is_chrome_syncing)
	{
		this.temp_sync_nodes.push({
			id: id,
			type: "remove", 
			info: info
		});
		this.decreaseAsyncChanges(id);
		return;
	}
//	if (id == CurationFolders.curation_toolbar_id)
	if (info.parentId == '1')
	{
		this.start();
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.temp_system_nodes[id])
	{
		delete this.temp_system_nodes[id];
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.checkLinkToOpenFolder(info.node.url))
	{
		var node = this.getNodeById(info.parentId);
		this.addLinkToOpenFolder(node);
		this.decreaseAsyncChanges(id);
		return;
	}
	
	info.node = this.arrayToObj(info.node);
	info.node.parentId = info.parentId;
	this.nodes_remove(info.node);
	this.decreaseAsyncChanges(id);
	this.run_listeners("onRemoved", {node: info.node, meta: this.temp_remove_meta_info[info.node.id]});
	delete this.temp_remove_meta_info[info.node.id];
}
CurationFolders.prototype.onChanged = function(id, info) {
	_log('onChanged | is_syncing=' + this.is_syncing + ' | is_chrome_syncing=' + this.is_chrome_syncing + ' | is_server_syncing=' + this.is_server_syncing);
	if (this.listeners_stoped)
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.is_chrome_syncing)
	{
		this.temp_sync_nodes.push({
			id: id,
			type: "change", 
			info: info
		});
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.temp_system_nodes_titles[id])
	{
		delete this.temp_system_nodes_titles[id];
		this.decreaseAsyncChanges(id);
		return;
	}
	var node_info = this.getNodeById(id);
	if (!node_info)
	{
		var _this = this;
		chrome.bookmarks.get(id, function(nodes) {
			if (nodes.length)
			{
				var tmp_node = _this.getNodeById(nodes[0].parentId);
				chrome.bookmarks.update(id, {
					title: '[ Open ' + tmp_node.title + ' ]',
					url: 'chrome://bookmarks/#select|' + tmp_node.id
				});
			}
		});
		this.decreaseAsyncChanges(id);
		return;
	}

	info.id = id;
	info.type = node_info.type;
	var node = this.nodes_add(info, true);
	this.decreaseAsyncChanges(id);
	node = this.objToArray(node);
	this.run_listeners("onChanged", {node: id, info: node});
	this.uniquelyName(node);
}
CurationFolders.prototype.onMoved = function(id, info) {
	_log('onMoved | is_syncing=' + this.is_syncing + ' | is_chrome_syncing=' + this.is_chrome_syncing + ' | is_server_syncing=' + this.is_server_syncing);
	if (this.listeners_stoped)
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	if (this.is_chrome_syncing)
	{
		this.temp_sync_nodes.push({
			id: id,
			type: "move", 
			info: info
		});
		this.decreaseAsyncChanges(id);
		return;
	}
	this.nodes_move(id, info.parentId, info.index);
	if (info.oldParentId == 1)
	{
		this.decreaseAsyncChanges(id);
		return;
	}
	this.decreaseAsyncChanges(id);
	this.run_listeners("onMoved", {node: id, parent: info.parentId, index: info.index});
	
	var _this = this;
	chrome.bookmarks.get(id, function(nodes) {
		if (chrome.runtime.lastError)
		{
			return;
		}
		if (nodes && nodes.length)
		{
			try
			{
				if (nodes[0].url && _this.checkLinkToOpenFolder(nodes[0].url))
				{
					chrome.bookmarks.remove(id, function() {
						if (chrome.runtime.lastError)
						{
							var node = _this.getNodeById(info.parentId);
							node && _this.addLinkToOpenFolder(node);
						}
					});
					if (info.oldParentId != info.parentId)
					{
						_this.addLinkToOpenFolder(_this.getNodeById(info.oldParentId));
					}
				}
				else
				{
					_this.uniquelyName(nodes[0]);
				}
			}
			catch(e){}
		}
	});
}
CurationFolders.prototype.onChildrenReordered = function(id, info) {
	_log('onChildrenReordered | is_syncing=' + this.is_syncing + ' | is_chrome_syncing=' + this.is_chrome_syncing + ' | is_server_syncing=' + this.is_server_syncing);
	if (this.listeners_stoped) return;
	if (this.is_chrome_syncing)
	{
		this.temp_sync_nodes.push({
			id: id,
			type: "reorder", 
			info: info
		});
		return;
	}
	this.run_listeners("onChildrenReordered", {node: id, info: info});
}
CurationFolders.prototype.onSyncBegan = function() {
	this.temp_sync_nodes = [];
	this.is_chrome_syncing = true;
	_log('is_chrome_syncing = true');
}
CurationFolders.prototype.onSyncEnded = function() {
	var _this = this;
	this.is_chrome_syncing = false;
	_log('is_chrome_syncing = false');
	this.is_syncing = true;
	_log('is_syncing = true');
	
	var n = 0;
	for (var i = 0; i < this.temp_sync_nodes.length; i++)
	{
		if ((this.temp_sync_nodes[i].type == "create" && this.temp_sync_nodes[i].node.parentId == 1) ||
		   (this.temp_sync_nodes[i].type == "remove" && this.temp_sync_nodes[i].info.parentId == 1))
		{
			chrome.bookmarks.removeTree(CurationFolders.curation_toolbar_id);
			_this.start();
			return;
		}
	}
	
	
	for (var i = 0; i < this.temp_sync_nodes.length; i++)
	{
		switch (this.temp_sync_nodes[i].type)
		{
			case "create":
				if (this.temp_sync_nodes[i].node.parentId == 1)
				{
					chrome.bookmarks.removeTree(CurationFolders.curation_toolbar_id);
					_this.start();
					return;
				/*	this.temp_sync_nodes.push({
						id: CurationFolders.curation_toolbar_id,
						type: "remove", 
						info: {
							parentId: '1'
						},
						custom: true
					});
					CurationFolders.curation_toolbar_id = this.temp_sync_nodes[i].id;
					for (var i in _this.nodes)
					{
						_this.nodes[i].parentId = CurationFolders.curation_toolbar_id;
					}*/
				}
				else
				{
					if (!this.checkLinkToOpenFolder(this.temp_sync_nodes[i].node.url))
					{
						_this.flag_for_create++;
						_this.onCreated(this.temp_sync_nodes[i].id, this.temp_sync_nodes[i].node, true);
					}
					else
					{
						chrome.bookmarks.remove(this.temp_sync_nodes[i].id);
					}
				}
				break;
			case "remove":
				if (this.temp_sync_nodes[i].info.parentId != '1')
				{
					_this.onRemoved(this.temp_sync_nodes[i].id, this.temp_sync_nodes[i].info);
				}
				else
				{
					CurationFolders.curation_toolbar_id = null;
					_this.start();
					return;
				}
			/*	else if (this.temp_sync_nodes[i].custom)
				{
					_this.onRemoved(this.temp_sync_nodes[i].id, this.temp_sync_nodes[i].info);
				}*/
				break;
			case "change":
				_this.onChanged(this.temp_sync_nodes[i].id, this.temp_sync_nodes[i].info);
				break;
			case "move":
				_this.onMoved(this.temp_sync_nodes[i].id, this.temp_sync_nodes[i].info);
				break;
			case "reorder":
				_this.onChildrenReordered(this.temp_sync_nodes[i].id, this.temp_sync_nodes[i].info);
				break;
			default:
				n++;
				break;
		}
	}
	this.is_syncing = false;
	_log('is_syncing = false');
	this.temp_sync_nodes = [];
	n > 0 && this.start();
}

CurationFolders.prototype.get = function() {
	var _this = this;
	return new Promise(function(resolve, reject) {
		resolve([{
			text: CurationFolders.title_for_bookmarks,
			title: CurationFolders.title_for_bookmarks,
			li_attr: {'class': 'padding'},
			type: Types.placeholder_folder.id,
		//	icon: "libs/images/curation.png",
			id: CurationFolders.curation_toolbar_id,
			children: _this.objToArray(_this.nodes),
			source_id: CurationFolders._id,
			guid: CurationFolders.guid
		}]);
	});
}

CurationFolders.prototype.getTree = function() {
	return {
		title: CurationFolders.title_for_bookmarks,
		id: CurationFolders.curation_toolbar_id,
		children: this.nodes,
		type: Types.placeholder_folder.id
	}
}

CurationFolders.prototype.bookmarks_create = function(node) {
	this.flag_for_create++;
	var finded_by_guid;
	if (node.guid)
	{
		finded_by_guid = this.getNodeBy('guid', node.guid);
	}
	if (finded_by_guid)
	{
		var _this = this;
		setTimeout(function() {
			_this.onCreated(finded_by_guid.id, finded_by_guid, true);
		}, 100);
	}
	else if (node.url && this.checkLinkToOpenFolder(node.url))
	{
		this.temp_node_pop();
	}
    else
	{
		var url = undefined;
		if (node.type == Types.web_link.id || node.type == Types.curation_link.id)
		{
			url = node.url;
			if (url.indexOf('http') != 0)
			{
				url = "http://" + url;
			}
		}
		this.increaseAsyncChanges();
		chrome.bookmarks.create({
			parentId: node.parentId,
			index: node.index,
			title: node.title,// + this.MyCodec.unicodeToQuinary("teststst"),
			url: url
		});
	}
}
CurationFolders.prototype.create = function(s_node, pos, meta) {
	if (this.temp_node.length > 0)
	{
		var queue_el = {
			node: JSON.parse(JSON.stringify(s_node)),
			pos: pos ? JSON.parse(JSON.stringify(pos)) : null
		};
		this.create_temp_queue.push(queue_el);
		if (this.check_temp_node_is_empty()) this.create();
		return;
	}
	else if (!s_node && !pos && this.create_temp_queue.length > 0)
	{
		s_node = this.create_temp_queue[0].node;
		pos = this.create_temp_queue[0].pos;
		this.create_temp_queue.splice(0, 1);
	}
	if (!s_node)
	{
		return;
	}
	
/*	var t = JSON.parse(JSON.stringify(s_node));
	delete t.children;
	delete t.id;
	delete t.parentId;
	console.log(JSON.stringify(t));*/
	var _this = this;
	if (Array.isArray(s_node))
	{
		this.temp_node = [JSON.parse(JSON.stringify({children: s_node}))];
	}
	else
	{
		this.temp_node = [JSON.parse(JSON.stringify(s_node))];
	}
	if (pos && pos.id != s_node.parentId)
	{
		chrome.bookmarks.get(pos.id, function(nodes) {
			if (nodes && nodes.length)
			{
				var index = nodes[0].index;
				if (pos.pos == 'before' && index > 0)
				{
				//	index--;
				}
				else if (pos.pos == 'after')
				{
					index++;
				}
				s_node.index = index;
			}
			
			if (Array.isArray(s_node))
			{
				this.temp_node.push(s_node[0]);
				s_node.length && _this.bookmarks_create(s_node[0]);
			}
			else
			{
				_this.bookmarks_create(s_node);
			}
		});
	}
	else
	{
		if (Array.isArray(s_node))
		{
			this.temp_node.push(s_node[0]);
			s_node.length && _this.bookmarks_create(s_node[0]);
		}
		else
		{
			_this.bookmarks_create(s_node);
		}
	}
}

CurationFolders.prototype.remove = function(node, meta) {
	this.nodes_remove(node, meta);
	this.temp_remove_meta_info[node.id] = meta;
	if (node.type == Types.curation_link.id || 
	   	node.type == Types.web_link.id)
	{
		chrome.bookmarks.remove(node.id);
	}
	else
	{
		chrome.bookmarks.removeTree(node.id);
	}
}

CurationFolders.prototype.change = function(node, no_callback) {
	node = this.nodes_add(node, true, no_callback);
	if (no_callback)
	{
		return;
	}
	if (node.title || ((node.type == Types.web_link.id || node.type == Types.curation_link.id) && node.url))
	{
		chrome.bookmarks.update(node.id, {
			title: node.title,
			url: (node.type == Types.web_link.id || node.type == Types.curation_link.id) ? node.url : undefined
		});
	}
	else
	{
		node = this.objToArray(node);
		this.run_listeners("onChanged", {node: node.id, info: node});
	}
}

CurationFolders.prototype.move = function(node, pos) {
	var _this = this;
	chrome.bookmarks.getSubTree(pos.id, function(nodes) {
		if (nodes && nodes.length)
		{
			var index = nodes[0].index;
			if (pos.pos == 'after')
			{
				index++;
			}
			if (pos.pos == 'into')
			{
				index = nodes[0].children.length;
			}
			chrome.bookmarks.move(node.id, {
				parentId: pos.pos == 'into' ? nodes[0].id : nodes[0].parentId,
				index: index
			});
		}
	});
}

CurationFolders.prototype.urlExist = function(url) {
	return this.temp_urls_list[normolizeUrl(url)] != undefined;
}

CurationFolders.prototype.getNodeByUrl = function(url) {
	var url = normolizeUrl(url);
	if (this.temp_urls_list[url])
	{
		return this.getNodeById(this.temp_urls_list[url]);
	}
}

CurationFolders.prototype.getAdditionalData = function(id) {
	var node;
	if (id) node = this.getNodeById(id);
	this.publishers = {'default': {}};
	this.authors = {'default': {}};
	this.twitters = {};
	this.tags = {};
	this.addAdditionalData({children: this.nodes}, true);
	return {
		publishers: this.publishers,
		authors: this.authors,
		twitters: this.twitters,
		tags: this.tags,
		some_node: node ? node : undefined
	};
}
CurationFolders.prototype.getAdditionalDataForNode = function(id) {
	this.publishers = {'default': {}};
	this.authors = {'default': {}};
	this.twitters = {};
	this.tags = {};
	var node = this.getNodeById(id);
	this.addAdditionalData(node, true);
	return {
		publishers: this.publishers[id] || {},
		authors: this.authors[id] || {},
		twitters: this.twitters[id] || {},
		tags: this.tags[id] || {}
	}
}