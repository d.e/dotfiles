function ToolbarInspector() {
	var _this = this;
	Base.apply(_this, arguments);
	var allInCurationFolders = {};
	var parents_collection = {};
	
	this.reinit = function() {
		allInCurationFolders = {};
		chrome.bookmarks.getSubTree("1", function(bookmarksNodes) {
		/*	function add_parent_for_childrens(node) {
				if (!parents_collection[node.id])
				{
					parents_collection[node.id] = [node.parentId || '#'];
				}
				if (node.children)
				{
					for (var i = node.children.length - 1; i >=0; i--)
					{
						if (!parents_collection[node.children[i].id])
						{
							parents_collection[node.children[i].id] = [];
						}
						parents_collection[node.children[i].id] = [node.id].concat(parents_collection[node.id]);
						add_parent_for_childrens(node.children[i]);
					}
				}
			}
			add_parent_for_childrens(bookmarksNodes[0]);*/

			function add_to_allInCurationFolders(node) {
				allInCurationFolders[node.id] = true;
				if (node.children)
				{
					for (var i = node.children.length - 1; i >=0; i--)
					{
						add_to_allInCurationFolders(node.children[i]);
					}
				}
			}

			if (bookmarksNodes && bookmarksNodes.length && bookmarksNodes[0].children)
			{
				var curationFolderRoot;
				for (var i = 0; i < bookmarksNodes[0].children.length; i++)
				{
					if (bookmarksNodes[0].children[i].title == CurationFolders.title_for_bookmarks)
					{
						curationFolderRoot = bookmarksNodes[0].children[i];
						break;
					}
				}
				if (curationFolderRoot)
				{
					add_to_allInCurationFolders(curationFolderRoot);
				}
			}
		});
	}
	this.reinit();
	chrome.bookmarks.onCreated.addListener(function onCreated(id, bookmarkTreeNode) {
		if (allInCurationFolders[bookmarkTreeNode.parentId] ||
			(bookmarkTreeNode.parentId == '1' && bookmarkTreeNode.title == CurationFolders.title_for_bookmarks))
		{
			allInCurationFolders[id] = true;
			_this.run_listeners("onCreated", CurationFolders._id, id, bookmarkTreeNode);
		}
		else
		{
			_this.run_listeners("onCreated", StandartBookmarks._id, id, bookmarkTreeNode);
		}
	});
	chrome.bookmarks.onRemoved.addListener(function onRemoved(id, info) {
		if (allInCurationFolders[id])
		{
			delete allInCurationFolders[id];
			_this.run_listeners("onRemoved", CurationFolders._id, id, info);
		}
		else
		{
			_this.run_listeners("onRemoved", StandartBookmarks._id, id, info);
		}
	});
	chrome.bookmarks.onChanged.addListener(function onChanged(id, info) {
		if (allInCurationFolders[id])
		{
			_this.run_listeners("onChanged", CurationFolders._id, id, info);
		}
		else
		{
			_this.run_listeners("onChanged", StandartBookmarks._id, id, info);
		}
	});
	chrome.bookmarks.onMoved.addListener(function onMoved(id, info) {
		var was_in_curation = allInCurationFolders[id] || false;
		var now_in_curation = allInCurationFolders[info.parentId] || false;
		if (id == CurationFolders.curation_toolbar_id)
		{
			now_in_curation = was_in_curation;
		}
		
		if (was_in_curation)
		{
			if (now_in_curation)
			{
				_this.run_listeners("onMoved", CurationFolders._id, id, info);
			}
			else
			{
				chrome.bookmarks.getSubTree(id, function(node) {
					if (chrome.runtime.lastError)
					{
						console.log(chrome.runtime.lastError);
						_this.run_listeners("onMoved", CurationFolders._id, id, info);
						return;
					}
					if (node && node.length)
					{
						node = node[0];
						_this.run_listeners("onRemoved", CurationFolders._id, id, {index: info.index, node: JSON.parse(JSON.stringify(node)), parentId: info.oldParentId});
						_this.run_listeners("onCreated", StandartBookmarks._id, id, node);
					}
				});
			}
		}
		else
		{
			if (now_in_curation)
			{
				chrome.bookmarks.getSubTree(id, function(node) {
					if (node && node.length)
					{
						node = node[0];
						_this.run_listeners("onRemoved", StandartBookmarks._id, id, {index: info.index, node: JSON.parse(JSON.stringify(node)), parentId: info.oldParentId});
						_this.run_listeners("onCreated", CurationFolders._id, id, node);
					}
				});
			}
			else
			{
				_this.run_listeners("onMoved", StandartBookmarks._id, id, info);
			}
		}
		if (now_in_curation)
		{
			allInCurationFolders[id] = true;
		}
		else
		{
			delete allInCurationFolders[id];
		}
	});
	chrome.bookmarks.onChildrenReordered.addListener(function onChildrenReordered(id, info) {
	//	_this.run_listeners("onChildrenReordered", StandartBookmarks._id, id, info);
	});
	
	this.is_syncing = undefined;
	chrome.bookmarks.onImportBegan.addListener(function () {
		_this.is_syncing = true;
		_this.run_listeners("onSyncBegan");
	});
	chrome.bookmarks.onImportEnded.addListener(function () {
		_this.is_syncing = false;
		_this.run_listeners("onSyncEnded");
	});
}
ToolbarInspector.prototype = Object.create(Base.prototype);