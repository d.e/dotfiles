window.selectInputControl = function(controlInput, mas, onchangeCallback, onSelectChangeCallback)
{
	var __mas = JSON.parse(JSON.stringify(mas));
	var _mas = mas;
	var controlSelect;
	if (!controlInput || !mas)
	{
		return;
	}
	
	function fillControlSelet() {
		controlSelect.children().remove();
		controlSelect.append("<option value='-1'></option>");
		for (var i = 0, l = _mas.length; i < l; i++)
		{
			controlSelect.append("<option value='" + i + "'>" + (_mas[i].label || _mas[i]) + "</option>");
		}
		controlSelect.val(-1);
	}

	controlInput.attr('style', 'position:absolute; width:calc(100% - 45px);outline:none');
	controlSelect = controlInput.parent().find('.custom-control-select');
	controlSelect.remove();
	controlSelect = $('<select class="custom-control-select" style="color: transparent;outline:none"></input>');
	controlInput.after(controlSelect);
	
	fillControlSelet();

	controlInput.on('focus', function() {
		controlInput.parent().addClass('focused');
	});
	controlInput.on('blur', function() {
		controlInput.parent().removeClass('focused');
	});
	
	controlSelect.on('change', function() {
		controlInput.val(controlSelect.find("option:selected").text());
		onValueChanged();
		onSelectChangeCallback && onSelectChangeCallback(controlInput.val(), controlSelect);
	});
	controlInput.on('change', onValueChanged);
	
	function onValueChanged() {
		var val = controlInput.val();
		var selected = false;
		for (var i = 0, l = _mas.length; i < l; i++)
		{
			if (val == _mas[i])
			{
				controlSelect.val(i);
				selected = true;
				break;
			}
		}
		if (!selected)
		{
			controlSelect.val(-1);
		}
		
		onchangeCallback && onchangeCallback(val);
	}
	onValueChanged();
	
	return {
		remove: function(element) {
			var ind = _mas.indexOf(element.trim());
			if (ind >= 0)
			{
				_mas.splice(ind, 1);
				fillControlSelet();
			}
		},
		add: function(element) {
			if (__mas.indexOf(element.trim()) >= 0)
			{
				_mas.push(element.trim());
				_mas.sort();
				fillControlSelet();
			}
		}
	}
}