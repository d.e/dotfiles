var separator = $('<div style="display:block;height: 0px;z-index:999"><div class="separator"></div></div>');
var current_selected_curation_folder;

function contextMenuItems(node) {
	var nodes = [];
	if (node.nodes)
	{
		nodes = node.nodes;
		node = node.node;
	}
	if (typeof node == 'string') node = treeView.get_node_by_id(node);
	if (node.original) node = node.original;
	for (var i = 0; i < nodes.length; i++)
	{
		if (typeof nodes[i] == 'string') nodes[i] = treeView.get_node_by_id(nodes[i]);
		if (nodes[i].original) nodes[i] = nodes[i].original;
	}
	var items = {
		curation_link: {
			"_disabled": true,
			"label": Local["new"] + " " + Local.curation_link,
			"separator_after": true,
			"action": function(data) {
				view_controller.create(Types.curation_link.id, node, {});
			}
		},
		open_in_tab: {
			"_disabled": true,
			"label": Local["open_new_tab"],
			"action": function(data) {
				for (var i = 0; i < nodes.length; i++)
				{
					var url = nodes[i].url;
					if (nodes[i].type == Types.curation_folder.id)
					{
						url = "chrome://bookmarks/?folder_mode#select|" + nodes[i].id + "|1";
					}
					if (url) open_link({url: url});
				}
				var url = node.url;
				if (node.type == Types.curation_folder.id)
				{
					url = "chrome://bookmarks/?folder_mode#select|" + node.id + "|1";
				}
				open_link({url: url});
			}
		},
		open_in_window: {
			"_disabled": true,
			"label": Local["open_new_window"],
			"action": function(data) {
				for (var i = 0; i < nodes.length; i++)
				{
					if (nodes[i].url) open_link({url: nodes[i].url, new_window: true});
				}
				open_link({url: node.url, new_window: true});
			}
		},
		open_incognito: {
			"_disabled": true,
			"label": Local["open_incognito"],
			"separator_after": true,
			"action": function(data) {
				for (var i = 0; i < nodes.length; i++)
				{
					if (nodes[i].url) open_link({url: nodes[i].url, incognito: true});
				}
				open_link({url: node.url, incognito: true});
			}
		},
		create: {
			"label": Local["create"],
			"separator_after": true,
			"_disabled": true
		},
		cut: {
			"_disabled": true,
			"label": Local["cut"],
			"action": function(data) {
				items.copy.action(data);
				items.remove.action(data);
			}
		},
		copy: {
			"_disabled": true,
			"label": Local["copy"],
			"action": function(data) {
				var copied_nodes = [treeView.get_subtree_json(node)];
				for (var i = 0; i < nodes.length; i++)
				{
					copied_nodes.push(treeView.get_subtree_json(nodes[i]));
				}
				main_port && main_port.postMessage({
					type: "temp_save_data", 
					data: JSON.stringify(copied_nodes)
				});
			}
		},
		paste: {
			"_disabled": true,
			"label": Local["paste"],
			"separator_after": true,
			"action": function(data) {
				if (!Payment.paid)
				{
					popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
							  $(popup).dialog("close");
						  }
					});
					return;
				}
				var copied_node_to_create = [];
				for (var i = 0; i < copied_node.length; i++)
				{
					if (Types[node.type].children.indexOf(copied_node[i].type) >= 0 &&
						!(copied_node[i].type == Types.web_link.id && node.source_id == CurationFolders._id))
					{
						copied_node[i].parentId = node.id;
						copied_node_to_create.push(copied_node[i]);
					}
				}
				main_port && main_port.postMessage({
					type: "create", 
					data: {
						source_id: node.source_id,
						node: copied_node_to_create
					}
				});
			}
		},
		edit: {
			"_disabled": true,
			"label": Local["edit"],
			"action": function(data) {
				view_controller.change(node);
			}
		},
		remove: {
			"_disabled": true,
			"label": Local["remove"],
			"action": function(data) {
				for (var i = 0; i < nodes.length; i++)
				{
					view_controller.remove(nodes[i]);
				}
				view_controller.remove(node);
			}
		}
	};
	
	items.cut.label = Local["cut"] + " " + Local[node.type];
	items.copy.label = Local["copy"] + " " + Local[node.type];
	var copied_node = temp_saved_data ? JSON.parse(temp_saved_data) : [];
	if (copied_node.length == 1)
	{
		items.paste.label = Local["paste"] + " " + Local[copied_node[0].type];
		if (Types[node.type].children.indexOf(copied_node[0].type) >= 0 &&
			!(copied_node[0].type == Types.web_link.id && node.source_id == CurationFolders._id))
		{
			enable(items.paste);
		}
	}
	else if (copied_node.length > 1)
	{
		items.paste.label = Local["paste"];
		enable(items.paste);
	}
	
	items.create.submenu = {};
	items.create.submenu[Types.web_link.id] = {
		"_disabled": true, "label": Local.web_link,
		"action": function(data) {view_controller.create(Types.web_link.id, node, {});}
	};
	items.create.submenu[Types.curation_folder.id] = {
		"_disabled": true, "label": Local.curation_folder,
		"action": function(data) {view_controller.create(Types.curation_folder.id, node, {});}
	};
	items.create.submenu[Types.placeholder_folder.id] = {
		"_disabled": true, "label": Local.placeholder_folder,
		"action": function(data) {view_controller.create(Types.placeholder_folder.id, node, {});}
	};
	items.create.submenu[Types.curation_file.id] = {
		"_disabled": true, "label": Local.curation_file,
		"action": function(data) {view_controller.create(Types.curation_file.id, node, {});}
	};
	items.create.submenu[Types.curation_group.id] = {
		"_disabled": true, "label": Local.curation_group,
		"action": function(data) {view_controller.create(Types.curation_group.id, node, {});}
	};
	items.create.submenu[Types.curation_page.id] = {
		"_disabled": true, "label": Local.curation_page,
		"action": function(data) {view_controller.create(Types.curation_page.id, node, {});}
	};
	
	function enable(item) {
		item._disabled = false;
	}

	enable(items.copy);
	if (node.id != '1' && node.id != '2' && node.id != CurationFolders.curation_toolbar_id && node.type != Types.curation_root.id)
	{
		enable(items.edit);
		enable(items.remove);
		enable(items.cut);
	}
	
	switch (node.type)
	{
		case Types.placeholder_folder.id:
			enable(items.create);
			enable(items.create.submenu[Types.placeholder_folder.id]);
			enable(items.create.submenu[Types.web_link.id]);
			enable(items.create.submenu[Types.curation_folder.id]);
			delete items.create.submenu[Types.curation_file.id];
			delete items.create.submenu[Types.curation_group.id];
			delete items.create.submenu[Types.curation_page.id];
			if (node.source_id == StandartBookmarks._id)
			{
				delete items.create.submenu[Types.curation_folder.id];
				delete items[Types.curation_link.id];
			}
			if (node.source_id == CurationFolders._id)
			{
				delete items.create.submenu[Types.web_link.id];
			}
			break;
		case Types.web_link.id:
			delete items.create.submenu[Types.curation_file.id];
			delete items.create.submenu[Types.curation_group.id];
			delete items.create.submenu[Types.curation_page.id];
			if (node.source_id == StandartBookmarks._id)
			{
				delete items.create.submenu[Types.curation_folder.id];
				delete items[Types.curation_link.id];
			}
			break;
		case Types.curation_folder.id:
			enable(items.create);
			enable(items.create.submenu[Types.placeholder_folder.id]);
			enable(items.create.submenu[Types.web_link.id]);
			enable(items.create.submenu[Types.curation_folder.id]);
			enable(items[Types.curation_link.id]);
			delete items.create.submenu[Types.curation_file.id];
			delete items.create.submenu[Types.curation_group.id];
			delete items.create.submenu[Types.curation_page.id];
			delete items.create.submenu[Types.web_link.id];
			break;
		case Types.curation_link.id:
			delete items.create.submenu[Types.curation_file.id];
			delete items.create.submenu[Types.curation_group.id];
			delete items.create.submenu[Types.curation_page.id];
			var selected = treeView.get_selected();
			if (selected && selected.original.type == Types.curation_page.id)
			{
				delete items.edit;
			}
			break;
		case Types.curation_file.id:
			enable(items.create);
			delete items.create.submenu[Types.placeholder_folder.id];
			delete items.create.submenu[Types.web_link.id];
			delete items.create.submenu[Types.curation_folder.id];
			delete items[Types.curation_link.id];
			enable(items.create.submenu[Types.curation_group.id]);
			items.export = {
				"label": Local["export_curation_file"],
				"separator_before": true,
				"action": function(data) {
					if (!Payment.paid)
					{
						popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
								  $(popup).dialog("close");
							  }
						});
						return;
					}
					main_port && main_port.postMessage({
						type: "export", 
						data: {
							file_id: node.id,
							file_name: node.text
						}
					});
				}
			};
			break;
		case Types.curation_group.id:
			enable(items.create);
			delete items.create.submenu[Types.placeholder_folder.id];
			delete items.create.submenu[Types.web_link.id];
			delete items.create.submenu[Types.curation_folder.id];
			delete items[Types.curation_link.id];
			enable(items.create.submenu[Types.curation_page.id]);
			break;
		case Types.curation_page.id:
			enable(items.create);
			delete items.create.submenu[Types.placeholder_folder.id];
			delete items.create.submenu[Types.web_link.id];
			delete items.create.submenu[Types.curation_folder.id];
			enable(items[Types.curation_link.id]);
			break;
		case Types.curation_root.id:
			enable(items.create);
			delete items.create.submenu[Types.placeholder_folder.id];
			delete items.create.submenu[Types.web_link.id];
			delete items.create.submenu[Types.curation_folder.id];
			delete items[Types.curation_link.id];
			enable(items.create.submenu[Types.curation_file.id]);
			break;
	}
	
	if (node.type == Types.curation_link.id ||
		node.type == Types.web_link.id)
	{
		enable(items.open_in_tab);
		enable(items.open_in_window);
		enable(items.open_incognito);
	}
	else
	{
		if (node.type == Types.curation_folder.id)
		{
			enable(items.open_in_tab);
		}
		else
		{
			delete items.open_in_tab;
		}
		delete items.open_in_window;
		delete items.open_incognito;
	}
	
	if (node.source_id == CurationFolders._id)
	{
		var import_btn = (node.type == Types.curation_folder.id) || (node.type == Types.placeholder_folder.id) || (node.id == CurationFolders.curation_toolbar_id);
		if (import_btn)
		{
			items.import = {
				"label": Local["import"],
				"separator_before": true,
				"action": function(data) {
				/*	if (!Payment.paid)
					{
						popup("Error", {"msg": {message: true, value: Local["subscriptionWarning"]}}, {'CANCEl': function(res, popup) { 
								$(popup).dialog("close");
							}, 'OK': function(res, popup) {
										current_selected_curation_folder = node;
										$('#imported_file_source').click();
										$(popup).dialog("close");
									}
							});
					}
					else*/
					{
						current_selected_curation_folder = node;
						$('#imported_file_source').click();
					}
				}
			}
		}
		items.export = {
			"label": Local["export"],
			"separator_before": !import_btn,
			"action": function(data) {
				if (!Payment.paid)
				{
					popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
							  $(popup).dialog("close");
						  }
					});
					return;
				}
				var fileName = node.text;
				if (nodes.length > 0)
				{
					try {
						fileName = treeView.get_node_by_id(node.parentId);
						fileName = fileName.text;
					} catch(e) {}
				}
				main_port && main_port.postMessage({
					type: "export", 
					data: {
						nodes: nodes.concat(node),
						file_name: fileName
					}
				});
			}
		};
	}
	
	return items;
}

function treeItemsToContext(items) {
	var menuMas = [];
	for (var item in items)
	{
		var menu_item = {
			title: items[item].label,
			disabled: items[item]._disabled,
			action: items[item].action
		};
		if (items[item].separator_before)
		{
			menuMas.push({title: "----"});
		}
		if (items[item].submenu)
		{
		//	continue;
			menu_item.children = treeItemsToContext(items[item].submenu);
			menu_item.className = 'more';
		}
		menuMas.push(menu_item);
		if (items[item].separator_after)
		{
			menuMas.push({title: "----"});
		}
	}
	return menuMas;
}