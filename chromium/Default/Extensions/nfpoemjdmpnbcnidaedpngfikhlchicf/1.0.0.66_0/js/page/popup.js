function popup(title, model, actions) {
	var modal_dom_string_inner = "";
	for (var i in model)
	{
		if (model[i].big_input)
		{
			modal_dom_string_inner += '<div class="row form-group"> \
			<div class="title col-lg-2 control-label">' + model[i].text + '</div> \
			<div class="data col-lg-10">\
				<textarea rows="6" class="input form-control resizable" id="id_' + i + '" type="text">' + model[i].value + '</textarea>\
			</div></div>';
		}
		else if (model[i].not_edit)
		{
			modal_dom_string_inner += '<div class="row form-group"> \
			<div class="title col-lg-2 control-label">' + model[i].text + '</div> \
			<div class="data col-lg-10">\
				<p class="title form-control-span">' + model[i].value + '</p>\
			</div></div>';
		}
		else if (model[i].select)
		{
			var options = '';
			modal_dom_string_inner += '<div class="row form-group"> \
			<div class="title col-lg-2 control-label">' + model[i].text + '</div> \
			<div class="data col-lg-10">\
				<span class="form-control-span" id="_id_' + i + '" style="position:absolute;pointer-events: none;width:calc(100% - 30px)">';
			for (var j = 0, l = model[i].select.length; j < l; j++)
			{
				var value = model[i].select[j].value;
				var key = model[i].select[j].key;
				var disabled = model[i].select[j].disabled;
				options += '<option ' + (value == model[i].value ? 'selected' : '') + ' value="' + value + '" ' + (disabled ? 'disabled' : '') + '>' + key + '</option>';
				if (value == model[i].value)
				{
					modal_dom_string_inner += key;
				}
			}
			modal_dom_string_inner += '</span><select style="color:transparent" class="input form-control" id="id_' + i + '">';
			modal_dom_string_inner += options + '</select></div></div>';
		}
		else if (model[i].message)
		{
			modal_dom_string_inner += '<div class="row info_mess">' +
							model[i].value +
						'</div>'
		}
		else if (model[i].text)
		{
			modal_dom_string_inner += '<div class="row form-group"> \
			<div class="title col-lg-2 control-label">' + model[i].text + '</div> \
			<div class="data col-lg-10">\
				<input class="input form-control" id="id_' + i + '" type="text" value="' + model[i].value + '">\
			</div></div>';
		}
	}
	var modal_dom = $("<div title='" + title + "'><div class='form-horizontal'><fieldset>" + modal_dom_string_inner + "<div class='row modal-footer'></fieldset></div></div>");
	
	var buttons = {};
	for (var name in actions)
	{
		var btn = $('<button class="btn">' + name + '</button>');
		if (name == Local['cancel'])
		{
			btn.addClass('btn-default');
			btn.click(function() {
				modal_dom.dialog("close");
			});
		}
		else
		{
			btn.addClass('btn-primary');
			btn.click(function(e) {
				var errors = 0;
				var res_model = {};
				for (var i in model)
				{
					if (!model[i].not_edit)
					{
						var val =  modal_dom.find('#id_' + i).val();
						if (model[i].check && !model[i].check(val))
						{
							modal_dom.find('#id_' + i).closest('.form-group').addClass('has-error');
							errors++;
						}
						else
						{
							res_model[i] = val;
						}
					}
				}
				if (errors <= 0)
				{
					actions[e.currentTarget.innerHTML](res_model, modal_dom);
				}
			});
		}
		modal_dom.find('.modal-footer').append(btn);
	}
	
	if (!modal_dom.find('.resizable').length)
	{
		modal_dom.find('.modal-footer').before('<div class="resizable"></div>');
	}
	
	var startWindowHeight, startHeight;
	modal_dom.addClass('overflowable');
	modal_dom.dialog({
		modal: true,
		width: 600,
		resizable: true,
		close: function(event, ui) {
			modal_dom.remove();
		},
		resize: function(event, ui) {
			if (startWindowHeight !== undefined && startHeight !== undefined)
			{
				var deltaH = ui.size.height - startWindowHeight;
				if (startHeight + deltaH > 50)
				$('.ui-dialog .resizable').attr('style', 'height: ' + (startHeight + deltaH) + 'px !important');
			}
		}
	});
	
	startWindowHeight = $('.ui-dialog').height();
	startHeight = $('.ui-dialog .resizable').height();
}