function TreeView() {
	var _this = this;
	this.jstree_div;
	this.searched_mas = [];
	Base.apply(this, arguments);
	
	this.dom;
	
	this.save_state = function() {
		var state_obj = {};
		state_obj.state = this.jstree_div.get_state();
		delete state_obj.state.sec;
		localStorage.jstree_custom = JSON.stringify(state_obj);
	}
	this.restore_state = function() {
		localStorage.jstree = localStorage.jstree_custom;
		this.jstree_div.restore_state();
	}
	
	this.get_node_info = function(node) {
		if (!node)
		{
			return;
		}
		if (typeof node == 'string')
		{
			node = this.jstree_div.get_node(node);
		}
		if (!node.original)
		{
			return null;
		}
		var node_info = {};
		node_info.original = node.original;
		if (node.original.type == Types.curation_page.id)
		{
		//	node = this.jstree_div.get_node(node.original.curation_folder_id);
			node = this.get_node_by_guid(node.original.curation_folder_guid);
		}
		node_info.children = [];
	/*	if (Array.isArray(node.original.children))
		{
			node_info.children = node.original.children;
		}
		else */if (node.children && node.children.length)
		{
			for (var i = 0, l = node.children.length; i < l; i++)
			{
				var tmp_info = this.get_node_by_id(node.children[i]);
				tmp_info.original.parents = tmp_info.parents;
				tmp_info = tmp_info.original;
				tmp_info.index = i;
				tmp_info.state && (tmp_info.state.hidden = false);
				delete tmp_info.children;
				node_info.children.push(tmp_info);
			}
		}
		return node_info;
	}
	
	this.get_selected = function() {
		var node = this.jstree_div.get_selected(true);
		if (node && node.length)
		{
			node = node[0];
		}
		else
		{
			node = null;
		}
		return node;
	}
	
	this.get_subtree_json = function(node, save_guid) {
		function get_json(n) {
			var node_json = _this.get_node_by_id(n);
			var node_json = JSON.parse(JSON.stringify(node_json));
			if (node_json.children)
			{
				var children = [];
				for (var i = 0; i < node_json.children.length; i++)
				{
					children.push(get_json(node_json.children[i]));
				}
				node_json.original.children = children;
			}
			node_json = node_json.original;
			delete node_json.id;
			!save_guid && delete node_json.guid;
			delete node_json.index;
			delete node_json.parentId;
			return node_json;
		}
		return get_json(node);
	}
	
	this.init = function(view_json) {
		var jstree_dom = $('<div></div>');
		$('#tree-pane').append(jstree_dom);
		
		var searched_mas = [];
		jstree_dom.jstree({
			'core': {
				'data': view_json ? view_json : [],
				'check_callback' : function(o, n, p, i, m) {
					if (m && m.dnd)
					{
						return false;
					}
					return true;
				},
				'multiple': false,
				'themes': {
					'dots': false
				}
			},
			'contextmenu' : {
				'items': function(node) {
					return contextMenuItems(node);
				}
			},
			'types' : Types,
			'search': {
				'show_only_matches': false,
				'search_callback': function(str, node) {
					for (var i = 0; i < node.children.length; i++)
					{
						str = str.toLowerCase();
						var ch_node = _this.get_node_by_id(node.children[i]);
						var confirm = false;
						if (ch_node.original.url && ch_node.original.url.toLowerCase().indexOf(str) >= 0)
						{
							confirm = true;
						}
						else if (ch_node.original.text && ch_node.original.text.toLowerCase().indexOf(str) >= 0)
						{
							confirm = true;
						}
						else if (ch_node.original.publisher && ch_node.original.publisher.toLowerCase().indexOf(str) >= 0)
						{
							confirm = true;
						}
						else if (ch_node.original.author && ch_node.original.author.toLowerCase().indexOf(str) >= 0)
						{
							confirm = true;
						}
						else if (Array.isArray(ch_node.original.tags))
						{
							for (var k = 0; k < ch_node.original.tags.length; k++)
							{
								if (ch_node.original.tags[k].toLowerCase().indexOf(str) >= 0)
								{
									confirm = true;
									break;
								}
							}
						}
						if (confirm)
						{
							_this.searched_mas.push(ch_node);
						}
					}
					return false;
				}
			},
			'plugins': [ "state", "contextmenu", "search", "types" ]
		}).on("changed.jstree", function(e, data) {
			if (data.action == "select_node")
			{
				if (data.node.original.type == Types.curation_page.id)
				{
					var show_curation = _this.get_node_by_guid(data.node.original.curation_folder_guid);
					if (!show_curation) {
						show_curation = _this.get_node_by_title(data.node.original.title);
						if (show_curation) {
							data.node.original.curation_folder_guid = show_curation.original.guid;
						}
					}
					_this.run_listeners("onSelected", _this.get_node_info(show_curation));
				}
				else
				{
					_this.run_listeners("onSelected", _this.get_node_info(data.node));
				}
			}
			_this.save_state();
		}).on("open_node.jstree", function(e, data) {
			_this.save_state();
		}).on("close_node.jstree", function(e, data) {
			_this.save_state();
		}).on("search.jstree", function(nodes, str) {
			_this.jstree_div.clear_search();
		//	_this.run_listeners("onSearch", _this.get_node_info({children: str.res, original: {}}));
			_this.run_listeners("onSearch", _this.get_node_info({children: _this.searched_mas, original: {}}));
		}).on("loaded.jstree", function() {
			_this.run_listeners("onInitialized");
		}).on("dblclick.jstree", function(event) {
			var node = $(event.target).closest("li");
			if (node.length)
			{
				var id = node[0].id;
				node = _this.get_node_by_id(id);
				_this.run_listeners("onDoubleSelected", node);
			}
		});
		this.jstree_div = jstree_dom.jstree(true);
		this.dom = jstree_dom[0];
		this.restore_state();
	}
}
TreeView.prototype = Object.create(Base.prototype);

TreeView.prototype.create = function(data, async_select) {
//	data.node.state = {};
//	data.node.state.hidden = true;
	var exist_node = treeView.get_node_by_id(data.node.id);
	if (exist_node) return;
	var index = data.node.index;
	if (data.node.parentId == 1 || this.get_node_by_id(data.node.parentId).type == Types.curation_folder.id)
	{
		index--;
	}
	var parent = this.get_node_by_id(data.node.parentId);
	this.jstree_div.create_node(parent, data.node, index);
	this.onSelectedTimeout && clearTimeout(this.onSelectedTimeout);
	this.onSelectedTimeout = null;
	if (!async_select)
	{
		this.run_listeners("onSelected", this.get_node_info(this.get_selected()));
	}
	else
	{
		var _this = this;
		this.onSelectedTimeout = setTimeout(function() {
			_this.run_listeners("onSelected", _this.get_node_info(_this.get_selected()));
		}, 300);
	}
	_log("created in tree view");
}
TreeView.prototype.remove = function(data) {
	var parent = this.get_node_by_id(data.node.parentId);
	if (parent.original.children)
	{
		for (var i = 0; i < parent.original.children.length; i++)
		{
			if (parent.original.children[i].id == data.node.id)
			{
				parent.original.children.splice(i, 1);
				break;
			}
		}
	}
	this.jstree_div.delete_node(data.node);
	this.run_listeners("onSelected", this.get_node_info(this.get_selected()));
	_log("removed in tree view");
}
TreeView.prototype.change = function(data, async_select) {
	var _this = this;
	function change(old_node, new_data) {
		for (var key in new_data)
		{
			if (key == 'type' && old_node.original[key] != new_data[key])
			{
				var node_dom = $('li#' + old_node.original.id);
				node_dom.removeClass(old_node.original.type);
				node_dom.addClass(new_data.type);
				old_node.li_attr.class = new_data.li_attr.class;
				old_node.type = new_data.type;
			}
			old_node.original[key] = new_data[key];
		}
		if (Array.isArray(new_data.children))
		{
			for (var i = 0; i < new_data.children.length; i++)
			{
				change(_this.get_node_by_id(new_data.children[i].id), new_data.children[i]);
			}
		}
	}
	if (data.info.text)
	{
		this.jstree_div.rename_node(data.node, data.info.text);
	}
	if (data.info.state && data.info.state.hidden)
	{
		this.jstree_div.hide_node(data.node);
	}
	if (typeof data.node === "string")
	{
		data.node = this.get_node_by_id(data.node);
	}
	if (data.node)
	{
		change(data.node, data.info);
	}
	if (data.bg_changes)
	{
		return;
	}
	this.onSelectedTimeout && clearTimeout(this.onSelectedTimeout);
	this.onSelectedTimeout = null;
	if (!async_select)
	{
		this.run_listeners("onSelected", this.get_node_info(this.get_selected()));
	}
	else
	{
		var _this = this;
		this.onSelectedTimeout = setTimeout(function() {
			_this.run_listeners("onSelected", _this.get_node_info(_this.get_selected()));
		}, 300);
	}
	_log("changed in tree view");
}
TreeView.prototype.move = function(data) {
	var node = this.get_node_by_id(data.node);
	var par_node = this.get_node_by_id(data.parent);
	if (!node || ! par_node)
	{
		return;
	}
	node.original.parentId = data.parent;
	var index = data.index;
	if ((data.parent == 1 || par_node.original.type == Types.curation_folder.id || par_node.original.id == CurationFolders.curation_toolbar_id) && data.index > 0)
	{
		index = data.index - 1;
	}
	var cur_index = $.inArray(node.id, par_node.children);
//	if (cur_index >= 0 && index > cur_index) index++;
	if (node.parent == par_node.original.id && index >= cur_index) index++;
	this.jstree_div.move_node(data.node, data.parent, index);
	this.run_listeners("onSelected", this.get_node_info(this.get_selected()));
	_log("moved in tree view");
}
TreeView.prototype.get_node_by_id = function(id) {
	return this.jstree_div.get_node(id);
}
TreeView.prototype.get_node_by_guid = function(id) {
	function loop(nodes) {
		if (Array.isArray(nodes))
		{
			for (var i = 0; i < nodes.length; i++)
			{
				var cur_node = treeView.get_node_by_id(nodes[i].id);
				if (cur_node.original.guid == id)
				{
					return cur_node;
				}
				else
				{
					var finded = loop(nodes[i].children);
					if (finded)
					{
						return finded;
					}
				}
			}
		}
		else
		{
			return null;
		}
	}
	
	var allTree = treeView.jstree_div.get_json(CurationFolders.curation_toolbar_id);
	var finded = loop([allTree])
	return finded;
}
TreeView.prototype.get_node_by_title = function(title) {
	function loop(nodes) {
		if (Array.isArray(nodes))
		{
			for (var i = 0; i < nodes.length; i++)
			{
				var cur_node = treeView.get_node_by_id(nodes[i].id);
				if (cur_node.original.title == title)
				{
					return cur_node;
				}
				else
				{
					var finded = loop(nodes[i].children);
					if (finded)
					{
						return finded;
					}
				}
			}
		}
		else
		{
			return null;
		}
	}
	
	var allTree = treeView.jstree_div.get_json(CurationFolders.curation_toolbar_id);
	var finded = loop([allTree])
	return finded;
}
TreeView.prototype.select_folder = function(node_id) {
	var node = this.get_node_by_id(node_id);
	if (!node)
	{
		return;
	}
	if (node.original.type != Types.curation_link.id ||
		node.original.type != Types.web_link.id)
	{
	/*	var state = JSON.parse(localStorage.jstree_custom);
		state.state.core.selected = [node_id];
		localStorage.jstree_custom = JSON.stringify(state);
		this.jstree_div.restore_state();*/
		this.jstree_div.deselect_all(true);
		this.jstree_div.select_node(node_id);
	}
}
TreeView.prototype.update = function(view_json) {
	if (this.jstree_div)
	{
		this.jstree_div.settings.core.data = view_json;
		this.jstree_div.refresh();
	}
	else
	{
		this.init(view_json);
	}
}
TreeView.prototype.search = function(str, callback) {
	this.jstree_div.clear_search();
	this.searched_mas = [];
	if (!str)
	{
		this.restore_state();
	}
	else
	{
		this.jstree_div.search(str);
	}
}