var view_controller = {
	move: function(node, parentTo, with_dialog, pos) {
		var nodes;
		if (Array.isArray(node))
		{
			nodes = node;
			for (var i = 0; i < nodes.length; i++)
			{
				nodes[i] = treeView.get_node_by_id(nodes[i]);
			}
			if (node.length)
			{
				node = node[0];
			}
		}
		else
		{
			node = treeView.get_node_by_id(node);
		}
		if (!node || node == parentTo || node.id == '1' || node.id == '2' || node.id == CurationFolders.curation_toolbar_id || node.type == Types.curation_root.id)
		{
			return;
		}
		parentTo = treeView.get_node_by_id(parentTo);
		if (parentTo.parents.indexOf(node.id) >= 0 || node.id == parentTo.id)
		{
			return;
		}
		if (Types[parentTo.type].children.indexOf(node.type) < 0)
		{
			if (parentTo.type == Types.curation_group.id && node.type == Types.curation_folder.id)
			{
				if (!Payment.paid)
				{
					popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
							  $(popup).dialog("close");
						  }
					});
					return;
				}
				main_port && main_port.postMessage({
					type: "create", 
					data: {
						source_id: parentTo.original.source_id,
						node: {
							source_id: parentTo.original.source_id,
							parentId: parentTo.original.id,
							type: Types.curation_page.id,
							title: node.original.title,
							text: node.original.title,
							tags: '',
							comment: '',
							curation_folder_id: node.original.id,
							curation_folder_guid: node.original.guid,
							dateAdded: new Date().getTime()
						}
					}
				});
			}
			
			return;
		}
		
		// изменение индекса Curation Link
		if (node.parent == parentTo.id && node.type == Types.curation_link.id)
		{
			listView.tableRowMove(nodes || [node], pos);
			
			return;
		}
			
		// перемещение внутри источника
		if (node.original.source_id == parentTo.original.source_id && (!with_dialog))// || node.original.type == Types.curation_link.id))
		{
			if (nodes)
			{
				for (var i = 0; i < nodes.length; i++)
				{
					if (nodes[i].original.source_id == parentTo.original.source_id)
					{
						main_port && main_port.postMessage({
							type: "move", 
							data: {
								source_id: nodes[i].original.source_id,
								node: nodes[i],
								pos: pos
							}
						});
					}
				}
			}
			else
			{
				main_port && main_port.postMessage({
					type: "move", 
					data: {
						source_id: node.original.source_id,
						node: node,
						pos: pos
					}
				});
			}
		}
		else // перемещение между источниками
		{
			var saved_node = treeView.get_subtree_json(node, false);
			saved_node.parentId = parentTo.original.id;
			saved_node.source_id = parentTo.original.source_id;
			if (with_dialog && (node.type == Types.curation_link.id || node.type == Types.web_link.id))
			{
				view_controller.create(Types.curation_link.id, parentTo, saved_node, function() {
					view_controller.remove(node);
				}, pos);
			}
			else
			{
				view_controller.remove(node, {"type": "move"});
				main_port && main_port.postMessage({
					type: "create", 
					data: {
						source_id: saved_node.source_id,
						node: saved_node,
						pos: pos,
						meta: {"type": "move"}
					}
				});
			}
		}
	},
	create: function(type, selected_node, sample_node, callback, pos) {
		if (selected_node.original) selected_node = selected_node.original;
		if (sample_node.original) sample_node = sample_node.original;

		var edit_mas = {};
		if (selected_node.source_id == StandartBookmarks._id)
		{
			edit_mas.title = {
				text: Local["title"],
				value: sample_node.title || ''
			};
			if (type == Types.web_link.id)
			{
				edit_mas.url = {
					text: Local["url"],
					value: sample_node.url || '',
					check: checkUrl,
					error_msg: 'input correct url'
				};
			}
		}
		else if (selected_node.source_id == CurationTree._id && type != Types.curation_link.id)
		{
			view_controller.tree_node(selected_node, type, "create", pos);
			return;
		}
		else
		{
			if (type == Types.curation_link.id)
			{
				if (!Payment.paid && getCurationCounts().folders_count > Payment.config.free_folders_count)
				{
					popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
							  $(popup).dialog("close");
						  }
					});
					return;
				}
				function closeFrame(event) {
					if (event.data == 'close')
					{
						window.removeEventListener("message", closeFrame);
						iframe_popup.dialog("close");
						iframe_popup.remove();
					}
					else if (event.data == 'done')
					{
						callback && callback();
					}
				}
				var parentId = selected_node.id;
				if (selected_node.source_id == CurationTree._id)
				{
				//	parentId = selected_node.curation_folder_id;
					var curation_folder_node = treeView.get_node_by_guid(selected_node.curation_folder_guid);
					parentId = curation_folder_node.original.id;
				}
				sample_node.pos = pos;
				var iframe_popup = $("<div title='" + Local["create"] + "'><iframe id='popup_frame_id' style='width: 100%;height: 100%;border:none' src='popup.html#" + 
									window.btoa(encodeURIComponent(JSON.stringify({parentId: parentId, url: sample_node.url, sample_node: sample_node, new: true})))
									+ "'></iframe></div></div>");
				iframe_popup.dialog({
					modal: true,
					width: 800,
					height: 605,
					resizable: true,
					close: function( event, ui ) {
						window.removeEventListener("message", closeFrame);
						iframe_popup.remove();
					}
				});
				
				window.addEventListener("message", closeFrame);
				
				return;
			}
			else
			{
				var types_mas = [
					{key: Local.curation_folder, value: Types.curation_folder.id},
					{key: Local.placeholder_folder, value: Types.placeholder_folder.id}
				];
				
				edit_mas = {
					type: {
						text: Local["type"],
						value: type,
						select: types_mas
					},
					url: {},
					title: {
						text: Local["title"],
						value: sample_node.title || ''
					},
					tags: {
						text: Local["tags"],
						value: sample_node.tags || ''
					},
					comment: {
						text: Local["comment"],
						value: sample_node.comment || '',
						big_input: true
					}
				}
				if (type == Types.web_link.id)
				{
					edit_mas.url.text = Local["url"];
					edit_mas.url.value = node.url || '';
					edit_mas.url.check = checkUrl;
				}
				else
				{
					delete edit_mas.url;
				}
			}
		}
		
		var buttons = {};
		buttons[Local["cancel"]] = function() {};
		buttons[Local["save"]] = function(result, popup) {
		//	Payment.getPaid(function() {
				if (!Payment.paid && result.type == Types.curation_folder.id && Payment.config.free_folders_count >= 0)
				{
					var counts = getCurationCounts(selected_node.id);
					if (counts.folders_count >= Payment.config.free_folders_count)
					{
						$('.error_mess').remove();
						$(popup).append($('<div class="row error_mess">' +
							Local['folders_count_error'] +
						'</div>'));
						return;
					}
				}
				$(popup).dialog("close");
				callback && callback();
				main_port && main_port.postMessage({
					type: "create", 
					data: {
						source_id: selected_node.source_id,
						node: {
							parentId: selected_node.id,
							title: result.title,
							url: result.url,
							tags: result.tags,
							comment: result.comment,
							type: result.type || type
						},
						pos: pos
					}
				});
		//	});
		}
		
		popup(Local["create"] + " " + Local[type], edit_mas, buttons);
	},
	tree_node: function(selected_node, type, action, pos) {
		if (!Payment.paid)
		{
			popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
					  $(popup).dialog("close");
				  }
			});
			return;
		}
		
		if (selected_node.original) 
		{
			selected_node.original.children = selected_node.children; 
			selected_node = selected_node.original;
		}
		var edit_mas = {
			type: {
				text: "Type",
				value: Local[type],
				not_edit: true
			},
			title: {
				text: Local["title"],
				value: action == "change" ? selected_node.title : ''
			},
			tags: {
				text: Local["tags"],
				value: action == "change" ? selected_node.tags : ''
			},
			comment: {
				text: Local["comment"],
				value: action == "change" ? selected_node.comment : '',
				big_input: true
			}
		};
		if (type == Types.curation_file.id)
		{
			edit_mas.publisher = {
				text: Local["publisher"],
				value: action == "change" ? selected_node.publisher : ''
			};
			edit_mas.author = {
				text: Local["author"],
				value: action == "change" ? selected_node.author : ''
			};
		}
		else if (type == Types.curation_page.id)
		{
			function add_item(prefix, node_item) {
				if (node_item.type == Types.curation_folder.id || 
				   node_item.type == Types.placeholder_folder.id)
				{
					nodes_mas.push({
						key: prefix + node_item.text,
						value: node_item.id,
						disabled: node_item.type === Types.placeholder_folder.id
					});
					for (var i in node_item.children)
					{
						add_item(prefix + '&nbsp;&nbsp;&nbsp;&nbsp;', node_item.children[i]);
					}
				}
			}
			var nodes_mas = [];
			var node = treeView.jstree_div.get_json(CurationFolders.curation_toolbar_id);
			add_item('', node);
			
			var selected_curation_folder_id;
			if (selected_node.curation_folder_guid)
			{
				var curation_folder_node = treeView.get_node_by_guid(selected_node.curation_folder_guid);
				if (!curation_folder_node) {
					curation_folder_node = treeView.get_node_by_title(selected_node.title);
				}
				selected_curation_folder_id = curation_folder_node && curation_folder_node.id;
			}
			else 
			{
				selected_curation_folder_id = selected_node.curation_folder_id;
			}
			
			edit_mas.curation_folder_id = {
				text: Local["link"],
				value: selected_curation_folder_id,
				select: nodes_mas
			};
		}
		
		var popup_actions = {};
		
		popup_actions[Local["cancel"]] = function() {};
		popup_actions[Local["save"]] = function(result, popup) {
		/*	var index = 0;
			var parent = selected_node;
			if (action == "change")
			{
				parent = treeView.get_node_by_id(selected_node.parentId);
			}
			index = parent.children ? parent.children.length : 0;*/
			$(popup).dialog("close");
			var selected_folder = {};
			if (result.curation_folder_id)
			{
				selected_folder = treeView.get_node_by_id(result.curation_folder_id);
			}
			main_port && main_port.postMessage({
				type: action, 
				data: {
					source_id: selected_node.source_id,
					node: {
						id: action == "change" ? selected_node.id : undefined,
						source_id: selected_node.source_id,
						parentId: action == "change" ? selected_node.parentId : selected_node.id,
						type: type,
						title: result.title,
						text: result.title,
						tags: result.tags,
						comment: result.comment,
						publisher: result.publisher,
						author: result.author,
						curation_folder_id: result.curation_folder_id,
						curation_folder_guid: selected_folder.original ? selected_folder.original.guid : undefined,
						children: [],
						dateAdded: new Date().getTime()
					}, 
					pos: pos
				}
			});
		}
		
		popup((action == "change" ? Local["edit"] : Local["create"]) + " " + Local[type], edit_mas, popup_actions);
		var strLength = $('#id_title').val().length;
		if ($('#id_title').length)
		{
			$('#id_title')[0].setSelectionRange(strLength, strLength);
		}
		
		if (type == Types.curation_page.id)
		{
			$('#_id_curation_folder_id').text($("#id_curation_folder_id option:selected").text().trim());
			if (action == "create")
			{
				$('#id_title').val($("#id_curation_folder_id option:selected").text().trim());
			}
		}
		$('#id_curation_folder_id').on('change', function() {
			$('#id_title').val($("#id_curation_folder_id option:selected").text().trim());
			$('#_id_curation_folder_id').text($("#id_curation_folder_id option:selected").text().trim());
		});
	},
	remove: function(node, meta) {
		if (node.original) node = node.original;
		main_port && main_port.postMessage({
			type: "remove", 
			data: {
				source_id: node.source_id,
				node: node,
				meta: meta
			}
		});
	},
	change: function(node) {
		if (node.original) node = node.original;

		var edit_mas = {};
		if (node.source_id == StandartBookmarks._id)
		{
			edit_mas.title = {
				text: Local["title"],
				value: node.title || ''
			};
			if (node.type == Types.web_link.id)
			{
				edit_mas.url = {
					text: Local["url"],
					value: node.url || '',
					check: checkUrl,
					error_msg: 'input correct url'
				};
			}
		}
		else if (node.source_id == CurationTree._id)
		{
			view_controller.tree_node(node, node.type, "change");
			return;
		}
		else
		{
			if (node.type == Types.curation_link.id)
			{
				if (!Payment.paid && getCurationCounts().folders_count > Payment.config.free_folders_count)
				{
					popup("Error", {"msg": {message: true, value: Local["subscriptionError"]}}, {'OK': function(res, popup) { 
							  $(popup).dialog("close");
						  }
					});
					return;
				}
				function closeFrame(event) {
					if (event.data == 'close')
					{
						window.removeEventListener("message", closeFrame);
						iframe_popup.dialog("close");
						iframe_popup.remove();
					}
					else if (event.data == 'done') {
						var parentNode = treeView.get_node_by_id(node.parentId);
						if (parentNode)
						{
							parentNode.original.sort = null;
							main_port && main_port.postMessage({
								type: "change", 
								data: {
									source_id: parentNode.original.source_id,
									node: {id: parentNode.id, sort: parentNode.original.sort},
									no_callback: true
								}
							});
						}
					}
				}
				var iframe_popup = $("<div title='" + Local["edit"] + "'><iframe id='popup_frame_id' style='width: 100%;height: calc(100% - 5px);border:none' src='popup.html#" + 
									window.btoa(encodeURIComponent(JSON.stringify({node: node})))
									+ "'></iframe></div></div>");
				iframe_popup.dialog({
					modal: true,
					width: 800,
					height: 605,
					resizable: true,
					close: function( event, ui ) {
						window.removeEventListener("message", closeFrame);
						iframe_popup.remove();
					}
				});
				window.addEventListener("message", closeFrame);
				
				return;
			}
			else
			{
				edit_mas = {
					type: {
						text: Local["type"],
						value: Local[node.type],
						not_edit: true
					},
					url: {},
					title: {
						text: Local["title"],
						value: node.title || ''
					},
					tags: {
						text: Local["tags"],
						value: node.tags || ''
					},
					comment: {
						text: Local["comment"],
						value: node.comment || '',
						big_input: true
					}
				}
				if (node.type == Types.web_link.id)
				{
					edit_mas.url.text = Local["url"];
					edit_mas.url.value = node.url || '';
					edit_mas.url.check = checkUrl;
				}
				else
				{
					delete edit_mas.url;
				}
			}
		}
		
		var buttons = {};
		buttons[Local["cancel"]] = function() {};
		buttons[Local["save"]] = function(result, popup) {
			node.title = result.title;
			node.text = result.title;
			node.url = result.url;
			node.tags = result.tags;
			node.comment = result.comment;
			$(popup).dialog("close");
			main_port && main_port.postMessage({
				type: "change", 
				data: {
					source_id: node.source_id,
					node: node
				}
			});
		};
		
		popup(Local["edit"], edit_mas, buttons);
	}
}