function ListView() {
	var _this = this;
	this.tree;
	this.table;
	this.header_table;
	this.mode;
	this.currentParentNode;
	var mySelectList = {};
	var lastSelectedRow;
	Base.apply(this, arguments);
	
	var columns = {};
/*	columns[StandartBookmarks._id] = {
		titles: [{id: "id", visible: false}, {title: Local["title"]}, {title: Local["url"]}, {title: Local["created"]}],
		keys: ["id", "text", "url", "dateAdded"]
	}*/
	columns[CurationFolders._id] = {
		titles: [
			{id: "id", visible: false}, 
			{index: "index", visible: false}, 
			{view_index: "view_index", visible: false}, 
			{title: Local["headline"]}, 
			{title: Local["pub_date"], "sType": "date-uk", class: "filter_date"}, 
			{title: Local["publisher"], class: "filter_publishers"}, 
			{title: Local["author"], class: "filter_authors"}, 
			{title: Local["tags"], class: "filter_tags"}, 
			{title: Local["rating"], class: "filter_rating"}
		],
		keys: ["id", "index", "view_index", "text", "pub_date", "publisher", "author", "tags", "rating"]
	};
/*	columns[CurationTree._id] = {
		titles: [{id: "id", visible: false}, {title: Local["title"]}, {title: Local["tags"]}, {title: Local["comment"]}, {title: Local["created"]}],
		keys: ["id", "text", "tags", "comment", "dateAdded"]
	};*/
	
	this.initTable = function(node) {
		this.currentParentNode = node;
		$('#list-pane #table').show();
		$('#list-pane #tree').hide();
		try {
			if (this.table)
			{
				this.table.destroy();
			}
		} catch(e){}
		try {
			if (this.header_table)
			{
				this.header_table.destroy();
			}
		} catch(e){}
		var table_dom = $("#jstable_dom");
		var fixed_header_dom = $("#fixed_header_dom");
		if (table_dom)
		{
			table_dom.remove();
		}
		if (fixed_header_dom)
		{
			fixed_header_dom.remove();
		}
		table_dom = $('<table class="display compact" width="100%" height="100%" id="jstable_dom"></table>');
		$('#list-pane #table').append(table_dom);
		fixed_header_dom = $('<table class="display compact" width="100%" height="100%" id="fixed_header_dom"></table>');
		$('#list-pane #table').append(fixed_header_dom);
		var clmns = {titles: [{title: ''}], keys: []};
		var data = [];
		var sortArray = {};
		var use_saved_sort = true;
		try
		{
			sortArray = JSON.parse(localStorage['sorting_arr_' + node.original.id]);
		}
		catch (e) { }
		if (node)
		{
			clmns = columns[node.original.source_id];
			if (node.original.type == Types.curation_page.id)
			{
				clmns = columns[CurationFolders._id];
			}
			for (var i = 0, l = node.children.length; i < l; i++)
			{
				if (node.children[i].type == Types.curation_link.id)
				{
					var mas = [];
					for (var j = 0; j < clmns.keys.length; j++)
					{
						var value = node.children[i][clmns.keys[j]];
						if (Array.isArray(value))
						{
							value = value.join(', ');
						}
						if (clmns.keys[j] == "dateAdded" || clmns.keys[j] == "pub_date")
						{
							value = new Date(value).toLocaleDateString('en-US');
						//	value = ("000" + (value.getMonth() + 1)).slice(-1) + '/' + ("000" + value.getDate()).slice(-2) + '/' + value.getFullYear();
						}
						if (clmns.keys[j] == "rating")
						{
							var tmp_val = '';
							if (value)
							{
								for (var k = 0; k < value; k++) tmp_val += '*';
							}
							value = tmp_val;
						}
						if (clmns.keys[j] == "view_index")
						{
							if (sortArray[node.children[i].id] !== undefined)
							{
								value =  sortArray[node.children[i].id];
								if (!use_saved_sort)
								{
									value++;
								}
							}
							else
							{
								value = 0;// node.children[i].index;
								for (var k = 0; k < data.length; k++)
								{
									data[k][2]++;
								}
								use_saved_sort = false;
								delete localStorage['filters_' + node.original.id]
							}
						}
						if (value == undefined)
						{
							value = '';
						}
						mas.push(value);
					}
					data.push(mas);
				}
			}
		}
		
		var sort;
		try
		{
			sort = JSON.parse(localStorage['sorting_type_' + node.original.id]);
		}
		catch (e)
		{
			sort = [2, "asc"];
		}
		var selected = localStorage[node.original.id + '_selected_item_id'] || 0;
		this.table = table_dom.DataTable({
			"data": data,
			"columns": clmns.titles,
			"paging": false,
			"info": false,
			"searching": true,
			"select": false,
			"order": use_saved_sort ? [sort] : [[2, "asc"]],
			"autoWidth": false
		});
		this.header_table = fixed_header_dom.DataTable({
			"data": [],
			"columns": clmns.titles,
			"paging": false,
			"info": false,
			"searching": false,
			"select": false,
			"order": [sort],
			"autoWidth": false
		});
		$('#fixed_header_dom tbody').remove();
		
		var columns_widths = {};
		if (localStorage.columns_widths)
		{
			try
			{
				columns_widths = JSON.parse(localStorage.columns_widths);
			}
			catch(e) {}
		}
		var columns_dom = table_dom.find("th");
		var tableWidth = table_dom.width();
		
		if (!columns_widths[node.original.id])
		{
			columns_widths[node.original.id] = [];
			var ind = 0;
			columns_dom.each(function() { 
				var thWidth = $(this).width() + 21;
				if (ind == 0)
				{
					if (thWidth > tableWidth - 500)
					{
						thWidth = tableWidth - 500;
					}
				}
				else
				{
					if (thWidth < 100)
					{
						thWidth = 100;
					}
				}
				ind++;
				var tmp_width = thWidth / tableWidth * 100;
				columns_widths[node.original.id].push(tmp_width);
				$(this).attr('style', 'width: ' + (tmp_width) + '%');
			});
			localStorage.columns_widths = JSON.stringify(columns_widths);
		}
		else if (columns_widths[node.original.id])
		{
			var ind = 0;
			columns_dom.each(function() { 
				$(this).attr('style', 'width: ' + (columns_widths[node.original.id][ind]) + '%');
				ind++;
			});
		}
		
		table_dom.colResizable({disable: true});
		table_dom.colResizable({
			liveDrag: true, 
			minWidth: 100,
			headerOnly: true, 
			onResize: function(e) {
				tableWidth = table_dom.width();
				columns_widths[node.original.id] = [];
				columns_dom.each(function() { 
					var thWidth = $(this).width() + 21;
					columns_widths[node.original.id].push(thWidth / tableWidth * 100); 
					$(this).attr('style', 'width: ' + (thWidth / tableWidth * 100) + '%');
				});
				localStorage.columns_widths = JSON.stringify(columns_widths);
				_this.refreshHeaderColumnWidths();
			},
			onDrag: function() {
				_this.refreshHeaderColumnWidths();
			}
		});
		if (columns_widths[node.original.id])
		{
			var ind = 0;
			columns_dom.each(function() { 
				$(this).attr('style', 'width: ' + (columns_widths[node.original.id][ind]) + '%');
				ind++;
			});
		}
		this.refreshHeaderColumnWidths();
		setTimeout(function() {
			_this.refreshHeaderColumnWidths();
		}, 100);
		
		this.table.select.style('single');
		this.table.on('dblclick', function(e) {
			var data = _this.table.row(e.target).data();
			data && _this.run_listeners("onDoubleSelected", data[0]);
		});
		this.table.on('click', function(e) {
			if (e.originalEvent.ctrlKey || e.originalEvent.metaKey)
			{
				_this.toggleRow(_this.table.row(e.target));
			}
			else if (e.originalEvent.shiftKey)
			{
				var tmpLastSelectedRow = lastSelectedRow;
				_this.selectRow(lastSelectedRow);
				var startIndex = $(lastSelectedRow.node()).index();
				var endIndex = $(_this.table.row(e.target).node()).index();
				var index = startIndex;
				while (index != endIndex)
				{
					if (startIndex > endIndex) {
						index--;
					}
					else if (startIndex < endIndex) {
						index++;
					}
					var tr_dom = $('#jstable_dom tbody tr:nth-child(' + (index + 1) + ')');
					_this.toggleRow(_this.table.row(tr_dom));
				}
				lastSelectedRow = tmpLastSelectedRow;
			}
			else
			{
				_this.selectRow(_this.table.row(e.target));
			}
		});
		this.table.on('order', function(e, dif, edit) {
			var rows = _this.table.rows();
			var view_indexes = {};
			if (rows[0])
			{
				for (var i = 0; i < rows[0].length; i++)
				{
					view_indexes[_this.table.row(rows[0][i]).data()[0]] = i;
				}
			}
			localStorage['sorting_arr_' + node.original.id] = JSON.stringify(view_indexes);
			localStorage['sorting_type_' + node.original.id] = JSON.stringify([edit[0].col, edit[0].dir]);
		});
		this.header_table.on('order', function(e, dif, edit) {
			_this.table.order([edit[0].col, edit[0].dir]).draw();;
		});
		
		if (data.length)
		{
			if (selected == 0 || !use_saved_sort)
			{
				_this.selectRow(_this.table.row(':eq(0)'));
			}
			else 
			{
				for (var ind = 0; ind < data.length; ind++)
				{
					if (selected == data[ind][0])
					{
						_this.selectRow(_this.table.row(':eq(' + data[ind][2] + ')'));
						break;
					}
				}
			}
		}
		else
		{
			$('#info-pane').html('');
		}
	}
	this.refreshHeaderColumnWidths = function() {
		var columns_dom = $("#jstable_dom thead th");
		var header_columns_dom = $("#fixed_header_dom thead th");
		var tableWidth = $("#jstable_dom thead").width();
	//	$("#fixed_header_dom").attr('style', 'width:' + tableWidth + 'px');
		if (columns_dom.length > 0 && columns_dom.length == header_columns_dom.length)
		{
			for (var i = 0, l = columns_dom.length; i < l; i++)
			{
				var wdth = $(columns_dom[i]).width();
				header_columns_dom[i].setAttribute('style', 'width:' + wdth + 'px');
			}
		}
		else
		{
			$("#fixed_header_dom").hide();
		}
	}
	this.selectRow = function(row) {
		if (!row) return;
		var row_id = row.data();
		if (Array.isArray(row_id) && row_id.length)
		{
			row_id = row_id[0];
		}
		else
		{
			return;
		}
		$('.custom_selected').removeClass('custom_selected');
		mySelectList = {};
		mySelectList[row_id] = true;
		lastSelectedRow = row;
		$(row.node()).toggleClass('custom_selected');
		_this.run_listeners("onSelected", row_id);
		localStorage[_this.currentParentNode.original.id + '_selected_item_id'] = row_id;
	}
	this.toggleRow = function(row) {
		if (!row) return;
		var row_id = row.data();
		if (Array.isArray(row_id) && row_id.length)
		{
			row_id = row_id[0];
		}
		else
		{
			return;
		}
		
		if (mySelectList[row_id])
		{
			delete mySelectList[row_id];
		}
		else
		{
			mySelectList[row_id] = true;
		}
		$(row.node()).toggleClass('custom_selected');
		lastSelectedRow = row;
	}
	this.getSelectedRows = function() {
		var mySelectMas = [];
		for (var i in mySelectList)
		{
			mySelectMas.push(i);
		}
		return mySelectMas;
	}
	
	this.initTree = function(node) {
		$('#list-pane #table').hide();
		$('#list-pane #tree').show();
		var jslist_dom = $('<div></div>');
		$('#list-pane #tree').append(jslist_dom);
		
		jslist_dom.jstree({
			'core': {
				'data': [],
				'check_callback': function(o, n, p, i, m) {
					if (m && m.dnd)
					{
						return false;
					}
					return true;
				},
				themes: {
					dots: false
				}
			},
			'contextmenu': {
				'items': function(node) {
					var selected = _this.tree.get_selected(true);
					for (var i = 0; i < selected.length; i++)
					{
						if (selected[i].id == node.id)
						{
							selected.splice(i, 1);
							break;
						}
					}
					return contextMenuItems({
						node: node.id,
						nodes: selected
					});
				}
			},
			"types": Types,
			"plugins": [ "state", "contextmenu", "types" ]
		}).on("dblclick.jstree", function(event) {
			var node = $(event.target).closest("li");
			if (node.length)
			{
				var id = node[0].id;
				node = _this.tree.get_node(id);
				_this.run_listeners("onDoubleSelected", node);
			}
		}).on("changed.jstree", function(e, data) {
			if (data.action == "select_node")
			{
				_this.run_listeners("onSelected", data.node.original);
			}
		});

		_this.tree = jslist_dom.jstree(true);
	}
	this.updateTree = function(data) {
		_this.tree.settings.core.data = data.children;
		_this.tree.refresh();
		$('#list-pane #table').hide();
		$('#list-pane #tree').show();
		
	//	$( "#tree .jstree-container-ul" ).sortable();
    //	$( "#tree .jstree-container-ul" ).disableSelection();
		
		
	}
	
	var contextmenu = $(document).contextmenu({
		delegate: '#list-pane',
		menu: [],
		select: function(event, ui) {},
		show: false,
		hide: false,
		open: function() {
			var submenus = $('.ui-menu.ui-widget li>ul');
			if (submenus.length)
			{
				submenus.each(function(i, el) {
					$(el).parent().addClass('more');
				});
			}
		},
		beforeOpen: function(event, ui) {
			var node;
			if ($(ui.target).is('#tree a, #tree i'))
			{
				event.preventDefault();
			}
			if ($(ui.target).is($('.dataTable td')))
			{
				node = _this.table.row(ui.target);
				node = node.data();
				if (!mySelectList[node[0]])
				{
					_this.selectRow(_this.table.row(ui.target));
				}
				node = treeView.get_node_by_id(node[0]);
			}
			else
			{
				node = treeView.get_selected();
			}
			if (node)
			{
				var selected = _this.getSelectedRows();
				for (var i = 0; i < selected.length; i++)
				{
					if (selected[i] == node.id)
					{
						selected.splice(i, 1);
						break;
					}
				}
				var items = contextMenuItems({
					node: node, 
					nodes: selected
				});
				contextmenu.contextmenu("replaceMenu", treeItemsToContext(items));
			}
			else
			{
				event.preventDefault();
			}
		}
	});
	
	this.initTree();
}
ListView.prototype = Object.create(Base.prototype);

ListView.prototype.update = function(node) {
	if (!node)
	{
		return;
	}
	this.mode = null;
	setInfoPanelSize();
	if (node.original.type == Types.curation_folder.id ||
		node.original.type == Types.curation_page.id)
	{
		this.initTable(node);
		this.mode = 'table';
		main_port && main_port.postMessage({
			type: 'get_additional_data',
			page: true,
			id: node.original.id
		});
	}
	else
	{
		this.updateTree(node);
		this.mode = 'tree';
	}
	
	$('.path').text('');
	node = treeView.get_node_by_id(node.original.id);
	if (node)
	{
		for (var i = node.parents.length - 2; i >= 0; i--)
		{
			var par = treeView.get_node_by_id(node.parents[i]);
			$('.path').append($('<a href="#select|' + node.parents[i] + '|0">' + par.text + '</a>'));
		}
		$('.path').append($('<a href="#select|' + node.id + '|0">' + node.text + '</a>'));
		if (!sidebar)
		{
			document.title = node.text;
		}
	}
}

function objToMas(obj) {
	var res = [];
	for (var key in obj)
	{
		if (obj[key]) res.push(key.trim());
	}
	
	res.sort(function(a, b) {
		return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
	});
	return res;
}

ListView.prototype.setFilters = function(additional_data) {
	if (this.mode != 'table')
	{
		return;
	}
	$('thead .filter_btn').remove();
	
	$('thead .filter_date').append("<div type='button' class='filter_btn' data='date'></div>");
	$('thead .filter_publishers').append("<div type='button' class='filter_btn' data='publishers'></div>");
	$('thead .filter_authors').append("<div type='button' class='filter_btn' data='authors'></div>");
	$('thead .filter_tags').append("<div type='button' class='filter_btn' data='tags'></div>");
	$('thead .filter_rating').append("<div type='button' class='filter_btn' data='rating'></div>");
	
	var filters = {};
	try
	{
		var saved = localStorage['filters_' + this.currentParentNode.original.id];
		filters = JSON.parse(saved);
	}
	catch (e) {}
	
	// date
	$('.filter_tooltip.date #date1').datepicker({
		dateFormat: "m/d/yy"
	}).on('change', function() {
		var dateFrom = new Date($(this).val() || 0).getTime();
		var dateTo = new Date($('.filter_tooltip.date #date2').val() || 0).getTime();
		if (dateTo < dateFrom)
		{
			$('.filter_tooltip.date #date2').val($(this).val());
		}
	});
	$('.filter_tooltip.date #date2').datepicker({
		dateFormat: "m/d/yy"
	}).on('change', function() {
		var dateFrom = new Date($('.filter_tooltip.date #date1').val() || 0).getTime();
		var dateTo = new Date($(this).val() || 0).getTime();
		if (dateTo < dateFrom)
		{
			$('.filter_tooltip.date #date1').val($(this).val());
		}
	});
	if (filters['date'])
	{
		$('.filter_tooltip.date #date1').val(filters['date']['dateFrom']);
		$('.filter_tooltip.date #date2').val(filters['date']['dateTo']);
	}
	else
	{
		$('.filter_tooltip.date #date1').val('');
		$('.filter_tooltip.date #date2').val('');
	}
	// rating
	$('.filter_tooltip.rating #rate1').on('change', function() {
		var rateFrom = $(this).val() || 0;
		if (rateFrom > 5)
		{
			$('.filter_tooltip.rating #rate1').val(5);
			return;
		}
		if (rateFrom < 0)
		{
			$('.filter_tooltip.rating #rate1').val(0);
			return;
		}
		var rateTo = $('.filter_tooltip.rating #rate2').val() || 0;
		if (rateTo < rateFrom)
		{
			$('.filter_tooltip.rating #rate2').val($(this).val());
		}
	});
	$('.filter_tooltip.rating #rate2').on('change', function() {
		var rateFrom = $('.filter_tooltip.rating #rate1').val() || 0;
		var rateTo = $(this).val() || 0;
		if (rateTo > 5)
		{
			$('.filter_tooltip.rating #rate2').val(5);
			return;
		}
		if (rateTo < 0)
		{
			$('.filter_tooltip.rating #rate2').val(0);
			return;
		}
		if (rateTo < rateFrom)
		{
			$('.filter_tooltip.rating #rate1').val($(this).val());
		}
	});
	if (filters['rate'])
	{
		$('.filter_tooltip.rating #rate1').val(filters['rate']['rateFrom']);
		$('.filter_tooltip.rating #rate2').val(filters['rate']['rateTo']);
	}
	else
	{
		$('.filter_tooltip.rating #rate1').val('');
		$('.filter_tooltip.rating #rate2').val('');
	}
	
	$('.filter_tooltip.publishers .checkbox label').remove();
	$('.filter_tooltip.authors .checkbox label').remove();
	$('.filter_tooltip.tags .checkbox label').remove();
	var publishers_parent = $('.filter_tooltip.publishers .checkbox');
	var authors_parent = $('.filter_tooltip.authors .checkbox');
	var tags_parent = $('.filter_tooltip.tags .checkbox');
	
	publishers_parent.append('<label>All <input checked type="checkbox" class="all"></label>');
	authors_parent.append('<label>All <input checked type="checkbox" class="all"></label>');
	tags_parent.append('<label>All <input checked type="checkbox" class="all"></label>');
	
	var all_publishers = true;// = filters['publishers'] ? (filters['publishers'].length == 0) : true;
	var all_authors = true;// = filters['authors'] ? (filters['authors'].length == 0) : true;
	var all_tags = true;// = filters['tags'] ? (filters['tags'].length == 0) : true;
	
	additional_data['publishers'] = objToMas(additional_data['publishers']);
	for (var i = 0; i < additional_data['publishers'].length; i++)
	{
		var checked = 'checked';
		if (filters['publishers'] && filters['publishers'][additional_data['publishers'][i]] === false)
		{
			all_publishers = false;
			checked = '';
		}
		publishers_parent.append('<label>\
				' + additional_data['publishers'][i] + ' <input ' + checked + ' type="checkbox" data="' + additional_data['publishers'][i] + '">\
			  </label>');
	}
	additional_data['authors'] = objToMas(additional_data['authors']);
	for (var i = 0; i < additional_data['authors'].length; i++)
	{
		var checked = 'checked';
		if (filters['authors'] && filters['authors'][additional_data['authors'][i]] === false)
		{
			all_authors = false;
			checked = '';
		}
		authors_parent.append('<label>\
				' + additional_data['authors'][i] + ' <input ' + checked + ' type="checkbox" data="' + additional_data['authors'][i] + '">\
			  </label>');
	}
	additional_data['tags'] = objToMas(additional_data['tags']);
	for (var i = 0; i < additional_data['tags'].length; i++)
	{
		var checked = 'checked';
		if (filters['tags'] && filters['tags'][additional_data['tags'][i]] === false)
		{
			all_tags = false;
			checked = '';
		}
		tags_parent.append('<label>\
				' + additional_data['tags'][i] + ' <input ' + checked + ' type="checkbox" data="' + additional_data['tags'][i] + '">\
			  </label>');
	}
		
	$('.filter_tooltip input.all').on('change', function() {
		var tooltip = $(this).parents('.filter_tooltip');
		if (this.checked)
		tooltip.find('input[type="checkbox"]').prop('checked', true);
	});
	$('.filter_tooltip input:not(.all)').on('change', function() {
		var tooltip = $(this).parents('.filter_tooltip');
		var not_checked = tooltip.find('input:not(.all)').filter(function() {
			return !this.checked;
		});
		tooltip.find('input.all').prop('checked', not_checked.length <= 0);
	});
	$('thead .filter_btn').on('click', function(e) {
		e.stopPropagation();
		var pos = this.getBoundingClientRect();
		$('.filter_tooltip').hide();
		var tooltip = $('.filter_tooltip.' + $(this).attr('data'));
		if (!tooltip.length)
		{
			return;
		}
		tooltip.show();
		var tooltipPos = tooltip[0].getBoundingClientRect();
		tooltip.css('top', (pos.top + pos.height + 5) + 'px');
		tooltip.css('left', (pos.left - tooltipPos.width + 14) + 'px');
		tooltip.show();
	});
	
	$('.filter_tooltip.publishers input.all').prop('checked', all_publishers);
	$('.filter_tooltip.authors input.all').prop('checked', all_authors);
	$('.filter_tooltip.tags input.all').prop('checked', all_tags);
	
	this.table.search();
	listView.table.draw();
}

ListView.prototype.getFilters = function(onlyValues) {
	var filters = {};
	var values = {};
	
	// date
	var dateFrom = new Date($('.filter_tooltip.date #date1').val() || 0).getTime();
	var dateTo = new Date($('.filter_tooltip.date #date2').val() || 0).getTime();
	filters['date'] = {
		selector: '.filter_date',
		dataIndex: 4,
		func: function(date) {
			date = new Date(date).getTime();
			return (dateFrom == 0 && dateTo == 0) || (date >= dateFrom && date <= dateTo);
		},
		applyed: dateFrom != 0 || dateTo != 0
	};
	values['date'] = {
		dateFrom: $('.filter_tooltip.date #date1').val(),
		dateTo: $('.filter_tooltip.date #date2').val()
	};
	
	// rating
	var rateFrom = $('.filter_tooltip.rating #rate1').val() || 0;
	var rateTo = $('.filter_tooltip.rating #rate2').val() || 0;
	filters['rate'] = {
		selector: '.filter_rating',
		dataIndex: 8,
		func: function(rate) {
			rate = rate.length;
			return (rateFrom == 0 && rateTo == 0) || (rate >= rateFrom && rate <= rateTo);
		},
		applyed: rateFrom != 0 || rateTo != 0
	};
	values['rate'] = {
		rateFrom: rateFrom,
		rateTo: rateTo
	};
	
	// publishers
	var publishers = {};
	var publishers_inputs = $('.filter_tooltip.publishers input');
	var publishers_applyed = true;
	var publishers_count = 0;
	for (var i = 0; i < publishers_inputs.length; i++)
	{
		var key = publishers_inputs[i].getAttribute('data');
		var val = publishers_inputs[i].checked;
		if (val)
		{
			publishers_count++;
			if (publishers_inputs[i].classList.contains('all'))
			{
				publishers_applyed = false;
			}
		}
		publishers[key] = val;
	}
	filters['publishers'] = {
		selector: '.filter_publishers',
		dataIndex: 5,
		func: function(publisher) {
			return !publishers_applyed || (!publisher && publishers_count == 0) || publishers[publisher];
		},
		applyed: publishers_applyed
	};
	values['publishers'] = publishers;
	
	// authors
	var authors = {};
	var authors_inputs = $('.filter_tooltip.authors input');
	var authors_applyed = true;
	var authors_count = 0;
	for (var i = 0; i < authors_inputs.length; i++)
	{
		var key = authors_inputs[i].getAttribute('data');
		var val = authors_inputs[i].checked;
		if (val)
		{
			authors_count++;
			if (authors_inputs[i].classList.contains('all'))
			{
				authors_applyed = false;
			}
		}
		authors[key] = val;
	}
	filters['authors'] = {
		selector: '.filter_authors',
		dataIndex: 6,
		func: function(author) {
			return !authors_applyed || (!author && authors_count == 0) || authors[author];
		},
		applyed: authors_applyed
	};
	values['authors'] = authors;
	
	// tags
	var tags = {};
	var tags_inputs = $('.filter_tooltip.tags input');
	var tags_applyed = true;
	var tags_count = 0;
	for (var i = 0; i < tags_inputs.length; i++)
	{
		var key = tags_inputs[i].getAttribute('data');
		var val = tags_inputs[i].checked;
		if (val)
		{
			tags_count++;
			if (tags_inputs[i].classList.contains('all'))
			{
				tags_applyed = false;
			}
		}
		tags[key] = val;
	}
	filters['tags'] = {
		selector: '.filter_tags',
		dataIndex: 7,
		func: function(tags_arr) {
			if (!tags_applyed)
			{
				return true;
			}
			if (tags_count == 0 && !tags_arr) return true;
			tags_arr = tags_arr.split(',');
			for (var i = 0; i < tags_arr.length; i++)
			{
				if (tags[tags_arr[i].trim()])
				{
					return true;
				}
			}
			return false;
		},
		applyed: tags_applyed
	};
	values['tags'] = tags;
	
	return onlyValues ? values : filters;
}

ListView.prototype.tableRowMove = function(nodes, pos) {
	if (this.mode != 'table')
	{
		return;
	}
	var rows = this.table.rows();
	var view_indexes = {};
	if (!rows[0])
	{
		return;
	}
	rows = rows[0];
	for (var i = 0; i < rows.length; i++)
	{
		view_indexes[this.table.row(rows[i]).data()[0]] = i;
	}
	
	var index = view_indexes[pos.id];
	if (index === undefined)
	{
		return;
	}
	if (pos.pos == 'after')
	{
		index++;
	}
	for (var i = 0; i < nodes.length; i++)
	{
		var old_index = view_indexes[nodes[i].original.id];
		for (var k in view_indexes)
		{
			if (old_index < view_indexes[k] && view_indexes[k] < index)
			{
				view_indexes[k]--;
			}
			else if (index <= view_indexes[k] && view_indexes[k] < old_index)
			{
				view_indexes[k]++;
			}
		}
		if (old_index < index)
		{
			view_indexes[nodes[i].original.id] = index - 1;
		}
		else
		{
			view_indexes[nodes[i].original.id] = index;
			index++;
		}
	}
	
	for (var i = 0; i < rows.length; i++)
	{
		var row = this.table.row(rows[i]);
		var data = row.data();
		data[2] = view_indexes[data[0]];
		row.data(data);
	}
	
	this.table.order([ 2, 'asc' ]).draw();
	localStorage['sorting_arr_' + this.currentParentNode.original.id] = JSON.stringify(view_indexes);
	localStorage['sorting_type_' + this.currentParentNode.original.id] = JSON.stringify([ 2, 'asc' ]);
}

ListView.prototype.selectById = function(node_id) {
	if (this.mode == 'tree')
	{
		this.tree.deselect_all(true);
		this.tree.select_node(node_id);
	}
	else if (this.mode == 'table')
	{
		var _this = this;
		var e = this.table.row(function(idx, data, node) {
			return data[0] === node_id;
		});
		_this.selectRow(e);
	}
}

$.extend($.fn.dataTableExt.oSort, {
	"date-uk-pre": function (a) {
		var x;
		try {
			var dateA = a.replace(/ /g, '').split("/");
			var day = parseInt(dateA[1], 10);
			var month = parseInt(dateA[0], 10);
			var year = parseInt(dateA[2], 10);
			var date = new Date(year, month - 1, day)
			x = date.getTime();
		} catch (err) {
			x = new Date().getTime();
		}

		return x;
	},

	"date-uk-asc": function (a, b) {
		return a - b;
	},

	"date-uk-desc": function (a, b) {
		return b - a;
	}
});
$.fn.dataTable.ext.search.push(
	function(settings, data, dataIndex) {
		var filters = listView.getFilters();
		for (var i in filters)
		{
			
			if (filters[i].applyed)
			{
				$('thead ' + filters[i].selector).addClass('filtered');
			}
			else
			{
				$('thead ' + filters[i].selector).removeClass('filtered');
			}
			
			var checked = filters[i].func(data[filters[i].dataIndex]);
			if (!checked)
			{
				return false;
			}
		}
		
		return true;
	}
);

$(document).ready(function() {
	$(document).on('click', function(e) {
		if (!$(e.target).parents('.filter_tooltip, .ui-widget').length && !$(e.target).is('.filter_tooltip'))
		{
			$('.filter_tooltip').hide();
		}
	});
	$('.apply_filters').on('click', function() {
		listView.table.search();
		listView.table.draw();
		var filters = listView.getFilters(true);
		localStorage['filters_' + listView.currentParentNode.original.id] = JSON.stringify(filters);
		$('.filter_tooltip').hide();
	});
	$('.clear_filter').on('click', function() {
		var tooltip = $(this).parents('.filter_tooltip');
		tooltip.find('input[type="checkbox"]').prop('checked', false);
		tooltip.find('input').val('');
	});
});

