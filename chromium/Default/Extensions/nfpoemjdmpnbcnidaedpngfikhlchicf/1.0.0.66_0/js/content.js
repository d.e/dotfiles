function getMetaTag(dom, selectors)
{
	var e;
	for (var i = 0; i < selectors.length; i++)
	{
		e = dom.querySelector(selectors[i]);
		if (e)
		{
			switch (e.nodeName)
			{
				case "META":
					return e.getAttribute('content') || e.getAttribute('value');
					break;
				case "TITLE":
					return e.textContent;
					break;
				case "TIME":
					if (e.getAttribute('datetime'))
					{
						return e.getAttribute('datetime');
					}
					return e.textContent;
				default:
					if (e.children.length > 0)
					{
						return e.children[0].textContent;
					}
					return e.textContent;
					break;
			}
		}
	}
	return null;
}

function filterImages(dom) {
	var list = dom.querySelectorAll('img');
	var unique = [];
	for (var i = 0; i < list.length; i++)
	{
		var k = list[i];
		var u = true;
		for (var j = 0; j < unique.length; j++)
		{
			if (unique[j].src == k.src)
			{
				u = false;
				break;
			}
		}
		if (u)
		{
			unique.push(k);
		}
	}
	if (unique.length == 0)
	{
		return unique;
	}
	unique.sort(function(a, b) { 
		if (a.className == b.className)
		{
			return 0;
		}
		return a.className > b.className ? -1 : 1; 
	});
	var i = 1;
	var result = [];
	var rest = [];
	result.push(unique[0]);
	while (i < unique.length)
	{
		if (unique[i - 1].className != unique[i].className)
		{
			result.push(unique[i]);
		}
		i++;
	}
	return result;
}

function normalizeUrl(url, scheme, prepath) {
	if (/^\/\//.test(url))
	{
		return scheme + url;
	}
	if (/^\/[^\/]/.test(url))
	{
		return prepath + url;
	}
	return url;
}

function parsePage(dom)
{
	var data = {};

	function findProperty(property, selectors)
	{
		var p = getMetaTag(dom, selectors);
		if (p !== null)
		{
			data[property] = p;
			if (typeof data[property] == 'string') data[property] = data[property].trim();
		}
	}

	findProperty('title', ['*[itemscope] *[itemprop="headline"]', 'title', 'meta[property="og:title"]']);
	findProperty('description', ['*[itemscope] *[itemprop="description"]', '*[itemscope] *[itemprop="about"]', 'meta[name="description"]', 'meta[property="og:description"]']);
	findProperty('publisher', ['*[itemscope] *[itemprop="publisher"]', 'meta[property="og:site_name"']);
	findProperty('author', ['*[itemscope] *[itemprop="author"]']);
	findProperty('pub_date', ['*[itemscope] *[itemprop="dateCreated"]', '*[itemscope] *[itemprop="datePublished"]']);
	findProperty('twitter', ['meta[name="twitter:site"]', 'meta[name="twitter:creator"]']);

	var primaryImage = getMetaTag(dom, ['*[itemscope] *[itemprop="image"]', 'meta[property="og:image"]', 'meta[property="twitter:image"]']);
	var images = [];
	var imgs = filterImages(dom);
	
	for (var i = 0; i < imgs.length; i++)
	{
		images.push(normalizeUrl(imgs[i].src, document.location.protocol, document.location.origin));
	}
	if (primaryImage)
	{
		if (images.indexOf(primaryImage) > 0)
		{
			images.splice(images.indexOf(primaryImage), 1);
		}
		images.unshift(primaryImage);
	}

	if (data.pub_date)
	{
		data.pub_date = (new Date(data.pub_date)).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
	}
	
	data.images = images;

	return data;
}

parsePage(document);