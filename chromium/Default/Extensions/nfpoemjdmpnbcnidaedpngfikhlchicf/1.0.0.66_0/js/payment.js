var InAppPayments = {
	myGoods: [],
	allGoods: [],
	getAllGoods: function(callback) {
	//	if (!InAppPayments.allGoods || InAppPayments.allGoods.length <= 0)
		{
			InAppPayments.updateAvailableGoods(function(error) {
				callback && callback(InAppPayments.allGoods, error);
			});
		}
	/*	else
		{
			callback && callback(InAppPayments.allGoods);
		}*/
	},
	buyGoods: function(goods_id, onSucces, onError) {
		google.payments.inapp.buy({
			'parameters': {'env': 'prod'},
			'sku': goods_id,
			'success': function(e) {
				onSucces && onSucces(goods_id);
				updateMyGoods();
			},
			'failure': function(e) {
			//	console.error(e);
				if (e.checkoutOrderId)
				{
					onSucces && onSucces(goods_id);
					chrome.storage.sync.set({
						'innap_buyed': {
							data: new Date().getTime(),
							goods_id: goods_id
						}
					}, function() {
						InAppPayments.updateMyGoods();
					});
				}
				else
				{
					onError && onError(goods_id);
				}
			}
		});
	},
	updateMyGoods: function(callback) {
		chrome.storage.sync.get('innap_buyed', function(ss) {
			google.payments.inapp.getPurchases({
				'parameters': {'env': 'prod'},
				'success': function(e) {
					if (e.response)
					{
						if (!e.response.details)
						{
							e.response.details = [];
						}
						InAppPayments.myGoods = [];
						var saved_license = {
							state: 'ACTIVE',
							sku: (ss['innap_buyed'] && ss['innap_buyed']['data'] > new Date().getTime() - 86400000) ? ss['innap_buyed']['goods_id'] : null
						};
						var has_license = false;
						if (saved_license.sku)
						{
							if (e.response.details.length)
							{
								for (var i = 0; i < e.response.details.length; i++)
								{
									var license = e.response.details[i];
									if (saved_license.sku === license.sku)
									{
										license.state = 'ACTIVE';
									}
								}
							}
							else
							{
								e.response.details.push(saved_license);
							}e.response.details.push(saved_license);
						}
						for (var i = 0; i < e.response.details.length; i++)
						{
							var license = e.response.details[i];
							if (license.state == 'ACTIVE' && (license.sku == 'm_subscription' || license.sku == 'y_subscribe'))
							{
								InAppPayments.myGoods.push(license);
								for (var j = 0; j < InAppPayments.allGoods.length; j++)
								{
									if (InAppPayments.allGoods[j].sku == license.sku)
									{
										InAppPayments.allGoods[j].buyed = true;
									}
								}
								has_license = true;
							}
						}
						Payment.setPaid(has_license);
						callback && callback();
					}
				},
				'failure': function(e) {
				//	console.error(e);
				}
			});
		});
	},
	updateAvailableGoods: function(callback) {
		google.payments.inapp.getSkuDetails({
			'parameters': {'env': 'prod'},
			'success': function(e) {
				if (e.response && e.response.details)
				{
					InAppPayments.allGoods = [];
					for (var j = 0; j < e.response.details.inAppProducts.length; j++)
					{
						if (e.response.details.inAppProducts[j].sku == 'm_subscription' ||
						   e.response.details.inAppProducts[j].sku == 'y_subscribe')
						{
							InAppPayments.allGoods.push(e.response.details.inAppProducts[j]);
						}
					}
					InAppPayments.updateMyGoods(callback);
				}
			},
			'failure': function(e) {
				InAppPayments.allGoods = [];
				InAppPayments.myGoods = [];
				Payment.setPaid(false);
				if (e.response)
				{
					callback && callback(e.response.errorType);
				}
			}
		});
	}
}