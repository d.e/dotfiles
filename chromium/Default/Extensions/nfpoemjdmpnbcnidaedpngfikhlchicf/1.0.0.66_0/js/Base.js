function Base() {
	this.listeners = {};
	this.listeners_stoped = false;
	this.is_syncing = false;
}

Base.prototype.MyCodec = {
	unicodeToQuinary: function (char) {
		var abc = {
			'0': String.fromCharCode(65279),
			'1': String.fromCharCode(8288),
			'2': String.fromCharCode(8205),
			'3': String.fromCharCode(8204),
			'4': String.fromCharCode(8203),
			'5': String.fromCharCode(0)
		};
		return char.split('').map(function (codepoint) {
			return abc[5] + codepoint.charCodeAt(0).toString(5).split('').map(function(ind) {
				return abc[ind]; 
			}).join('');
		}).join('');
	},
	quinaryToUnicode: function (binaryString) {
		var reAbc = {};
		reAbc[String.fromCharCode(65279)] = '0';
		reAbc[String.fromCharCode(8288)] = '1';
		reAbc[String.fromCharCode(8205)] = '2';
		reAbc[String.fromCharCode(8204)] = '3';
		reAbc[String.fromCharCode(8203)] = '4';
		reAbc[String.fromCharCode(0)] = '5';
		var codepointsAsNumbers = [];
		binaryString = binaryString.split(String.fromCharCode(8239));
		for (var i = 0, l = binaryString.length; i < l; i++) {
			codepointsAsNumbers.push(parseInt(binaryString[i].split('').map(function(ind) {
				return reAbc[ind]; 
			}).join(''), 5));
		}
		return String.fromCharCode.apply(this, codepointsAsNumbers);
	}
}

Base.prototype.guid = function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

Base.prototype.add_listeners = function(event, callback) {
	if (!this.listeners[event])
	{
		this.listeners[event] = [];
	}
	if (Array.isArray(callback))
	{
		this.listeners[event] = this.listeners[event].concat(callback);
	}
	else
	{
		this.listeners[event].push(callback);
	}
}

Base.prototype.run_listeners = function(listener) {
	if (this.listeners_stoped)
	{
		return;
	}
	if (this.listeners[listener] && this.listeners[listener].length)
	{
		var args = Array.prototype.slice.call(arguments, 1);
		for (var i = 0; i < this.listeners[listener].length; i++)
		{
			this.listeners[listener][i].apply(this, args);
		}
	}
}

Base.prototype.remove_listeners = function(listener) {
	delete this.listeners[listener]
}

Base.prototype.showBookmarkPage = function(hash) {
	var url = 'chrome://bookmarks' + (hash ? '#' + hash : '');
	chrome.tabs.getAllInWindow(function(tabs) {
		for (var i = tabs.length - 1; i >= 0; i--)
		{
			if (tabs[i].url.indexOf('chrome://bookmarks') == 0)
			{
				chrome.tabs.highlight({tabs: tabs[i].index});
				chrome.tabs.update({url: url});
				return;
			}
		}
		chrome.tabs.create({url: url});
	});
}

Base.prototype.arrayToObj = function(array) {
	function convert(array) {
		var obj = {};
		for (var i = array.length - 1; i >= 0; i--)
		{
			if (!array[i])
			{
				continue;
			}
			obj[array[i].id] = array[i];
			if (obj[array[i].id].children)
			{
				obj[array[i].id].children = convert(obj[array[i].id].children);
			}
		}
		return obj;
	}
	var res = JSON.parse(JSON.stringify(array));
	if (Array.isArray(res))
	{
		res = convert(res);
	}
	else if (Array.isArray(res.children))
	{
		res.children = convert(res.children);
	}
	
	return res;
}
Base.prototype.objToArray = function(obj) {
	function convert(obj) {
		var array = [];
		var i = 0;
		for (var key in obj)
		{
			if (!obj[key])
			{
				continue;
			}
			if (obj[key].type == Types.curation_link.id)
			{
				obj[key].state = {};
				obj[key].state.hidden = true;
			}
			array.push(obj[key]);
			if (array[i].children)
			{
				array[i].children = convert(array[i].children);
			}
			i++;
		}
		return array.sort(function(a, b) {
			if (a.index < b.index)
			{
				return -1;
			}
			else if (a.index > b.index)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		});
	}
	var res = JSON.parse(JSON.stringify(obj));
	if (res.children)
	{
		res.children = convert(res.children);
	}
	else if (!obj.url)
	{
		res = convert(res);
	}
	
	return res;
}

var Types = {
	web_link: {id: "web_link", icon: "libs/images/page.png", children: [], subtype: "link"},
	placeholder_folder: {id: "placeholder_folder", icon: "libs/images/folder.png", children: ['curation_folder', 'placeholder_folder', 'web_link'], subtype: "folder"},
	curation_link: {id: "curation_link", icon: "libs/images/page.png", children: [], subtype: "link"},
	curation_folder: {id: "curation_folder", icon: "libs/images/curation.png", children: ['curation_folder', 'curation_link', 'placeholder_folder', 'web_link'], subtype: "folder"},
	curation_file: {id: "curation_file", icon: "libs/images/panel.png", children: ['curation_group'], subtype: "other"},
	curation_group: {id: "curation_group", icon: "libs/images/linkroot.png", children: ['curation_page'], subtype: "other"},
	curation_page: {id: "curation_page", icon: "libs/images/curation.png", children: [], subtype: "other"},
	curation_root: {id: "curation_root", icon: "libs/images/curationroot.png", children: ['curation_file'], subtype: "other"}
}