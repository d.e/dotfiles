var bm_ports = {};
var sources = {};
var toolbarInspector;
sources[StandartBookmarks._id] = new StandartBookmarks();
sources[CurationFolders._id] = new CurationFolders();
sources[CurationTree._id] = new CurationTree();
toolbarInspector = new ToolbarInspector();

var temp_saved_data;
var initialized = false;

sources[CurationFolders._id].add_listeners("on_initalized", function() {
	sources[CurationTree._id].forEach(function(node) {
		if (node.type == Types.curation_page.id)
		{
			var folder = sources[CurationFolders._id].getNodeBy('id', node.curation_folder_id);
			if (!folder)
			{
				folder = sources[CurationFolders._id].getNodeBy('last_id', node.curation_folder_id);
				if (folder)
				{
					node.curation_folder_id = folder.id;
				}
			}
		}
	});
	
	chrome.tabs.getAllInWindow(function(tabs) {
		for (var i = tabs.length - 1; i >= 0; i--)
		{
			if (sources[CurationFolders._id].urlExist(tabs[i].url))
			{
				markBrowserIcon(true, tabs[i].id);
			}
			else
			{
				markBrowserIcon(false, tabs[i].id);
			}
			
			if (/chrome\:\/\/bookmarks/.test(tabs[i].url)
			   || /chrome-extension:\/\/\w+\/bookmarks.html/.test(tabs[i].url))
			{
				var connected = false;
				for (var port in bm_ports)
				{
					if (bm_ports[port].sender.tab.id == tabs[i].id)
					{
						connected = true;
						break;
					}
				}
				
				if (!connected || initialized)
				{
					chrome.tabs.update(tabs[i].id, {url: "chrome://bookmarks"});
				}
			}
		}
	});

	if (!initialized)
	{
		toolbarInspector.add_listeners("onSyncBegan", function() {
			sources[CurationFolders._id].onSyncBegan();
			Sync.enableSync(true);
		});
		toolbarInspector.add_listeners("onSyncEnded", function() {
			sources[CurationFolders._id].onSyncEnded();
		//	Sync.syncTimeoutDelayFolders = 120000;
			if (Sync.enabled)
			{
				sources[CurationTree._id].startSync(true);
				sources[CurationFolders._id].startSync(true);
			}
			else
			{
				sources[CurationTree._id].stopSync();
				sources[CurationFolders._id].stopSync();
			}
		});
		toolbarInspector.add_listeners("onCreated", function(source_id, id, bookmarkTreeNode) {
			sources[source_id] && sources[source_id].onCreated(id, bookmarkTreeNode);
		});
		toolbarInspector.add_listeners("onRemoved", function(source_id, id, info) {
			sources[source_id] && sources[source_id].onRemoved(id, info);
		});
		toolbarInspector.add_listeners("onChanged", function(source_id, id, info) {
			sources[source_id] && sources[source_id].onChanged(id, info);
		});
		toolbarInspector.add_listeners("onMoved", function(source_id, id, info) {
			sources[source_id] && sources[source_id].onMoved(id, info);
		});
		
		initialized = true;
		
		if (toolbarInspector.is_syncing != undefined) {
			sources[CurationFolders._id].start();
		}
	}
	else
	{
		toolbarInspector.reinit();
	}
});

sources[StandartBookmarks._id].add_listeners("onCreated", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onCreated",
		data: data
	});
});
sources[StandartBookmarks._id].add_listeners("onRemoved", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onRemoved",
		data: data
	});
});
sources[StandartBookmarks._id].add_listeners("onChanged", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onChanged",
		data: data
	});
});
sources[StandartBookmarks._id].add_listeners("onMoved", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onMoved",
		data: data
	});
});
sources[StandartBookmarks._id].add_listeners("onChildrenReordered", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onChildrenReordered",
		data: data
	});
});

sources[CurationFolders._id].add_listeners("onCreated", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onCreated",
		data: data
	});
	findTabWithUrl(data.node.url, function(tab) {
		if (tab)
		{
			markBrowserIcon(true, tab.id);
		}
	});
});
sources[CurationFolders._id].add_listeners("onRemoved", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onRemoved",
		data: data
	});
	function clearNode(node) {
		findTabWithUrl(node.url, function(tab) {
			if (tab)
			{
				markBrowserIcon(false, tab.id);
			}
		});
		sources[CurationTree._id].removeLinkedPages(node.id);
		if (node.children)
		{
			for (var ind in node.children)
			{
				clearNode(node.children[ind]);
			}
		}
	}
	
	clearNode(data.node); 
});
sources[CurationFolders._id].add_listeners("onChanged", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onChanged",
		data: data
	});
});
sources[CurationFolders._id].add_listeners("onMoved", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onMoved",
		data: data
	});
});
sources[CurationFolders._id].add_listeners("onChildrenReordered", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onChildrenReordered",
		data: data
	});
});
sources[CurationFolders._id].add_listeners("onLoadingStart", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onLoadingStart"
	});
});
sources[CurationFolders._id].add_listeners("onLoadingEnd", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onLoadingEnd"
	});
});

sources[CurationTree._id].add_listeners("onCreated", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onCreated",
		data: data
	});
});
sources[CurationTree._id].add_listeners("onRemoved", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onRemoved",
		data: data
	});
});
sources[CurationTree._id].add_listeners("onChanged", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onChanged",
		data: data
	});
});
sources[CurationTree._id].add_listeners("onMoved", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onMoved",
		data: data
	});
});
sources[CurationTree._id].add_listeners("onChildrenReordered", function(data) {
	for (var i in bm_ports) bm_ports[i].postMessage({
		type: "onChildrenReordered",
		data: data
	});
});


chrome.runtime.onConnect.addListener(function(port) {
	bm_ports[port.sender.tab.id] = port;
	port.onMessage.addListener(listener);
	port.onDisconnect.addListener(function(port) {
		delete bm_ports[port.sender.tab.id];
	});
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
//	if (changeInfo.status == "complete")
	{
		if (sources[CurationFolders._id].urlExist(tab.url))
		{
			markBrowserIcon(true, tabId);
		}
		else
		{
			markBrowserIcon(false, tabId);
		}
	}
});

chrome.runtime.onMessage.addListener(listener);

function listener(msg, sender, callback) {
	switch (msg.type) 
	{
		case "get_all_nodes": 
			function send_nodes() {
				if (!initialized)
				{
					setTimeout(send_nodes, 100);
					return;
				}
				var source_promises = [];
				for (var id in sources)
				{
					source_promises.push(sources[id].get());
				}
				Promise.all(source_promises).then(function(view_jsons) {
					var view_json = [];
					for (var i = 0; i < view_jsons.length; i++)
					{
						view_json = view_json.concat(view_jsons[i]);
					}
					sender.postMessage({type: "all_nodes", data: view_json});
					if (temp_saved_data) sender.postMessage({
						type: "temp_saved_data",
						data: temp_saved_data
					});
				});
			}
			send_nodes();
			break;
		case "create":
			sources[msg.data.source_id] && sources[msg.data.source_id].create(msg.data.node, msg.data.pos, msg.data.meta);
			break;
		case "remove":
			sources[msg.data.source_id] && sources[msg.data.source_id].remove(msg.data.node, msg.data.meta);
			break;
		case "change":
			sources[msg.data.source_id] && sources[msg.data.source_id].change(msg.data.node, msg.data.no_callback);
			break;
		case "move":
			sources[msg.data.source_id] && sources[msg.data.source_id].move(msg.data.node, msg.data.pos);
			break;
		case "export":
			var content;
			var file_name = msg.data.file_name;
			if (msg.data.file_id)
			{
				content = getCurationFileXML(msg.data.file_id);
				file_name += '.xml';
			}
			else if (msg.data.nodes)
			{
				content = getCurationLinksJSON(msg.data.nodes);
				file_name += '.oac';
			}
			
			var data = new Blob([content], {type: 'text/plain'});
			var file = window.URL.createObjectURL(data);
			
		/*	var element = document.createElement('a');
			element.setAttribute('href', file);
			element.setAttribute('download', file_name);
			element.style.display = 'none';
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);*/
			chrome.downloads.download({
				url: file, 
				filename: file_name
			}, 
			function() {});
			break;
		case "import":
			var txt = msg.data.file_text;
			try
			{
				txt = JSON.parse(txt);
				if (txt && txt.data)
				{
					var tree = generateCurationTreeFromJSON(txt.data);
				/*	if (!Payment.paid)
					{
						var tree = getFirstFolderWithoutSubfolders(tree);
					}*/
					var parent_node;
					if (msg.data.parent.guid)
					{
						parent_node = {
							guid: msg.data.parent.guid,
							children: tree
						};
					}
					else
					{
						parent_node = tree;
					}
					sources[CurationFolders._id].create(parent_node);
				}
			}
			catch (e) {}
			break;
		case "get_node_for_popup":
			var node;
			if (!msg.new)
			{
				if (msg.node_id)
				{
					node = sources[CurationFolders._id].getNodeById(msg.node_id);
				}
				else if (msg.tab_url)
				{
					node = sources[CurationFolders._id].getNodeByUrl(msg.tab_url);
				}
			}
			var additional_data = sources[CurationFolders._id].getAdditionalData(node ? node.parentId : null);
			if (node)
			{
				chrome.runtime.sendMessage({type: "node_for_popup", node: node, exist: true, additional_data: additional_data});
			}
			else
			{
				function sendResults(results) {
					node = {
						source_id: CurationFolders._id,
						url: msg.tab_url
					};
					if (results && results.length && results[0])
					{
						node.title = results[0].title;
						node.text = results[0].title;
						node.description = results[0].description;
						node.publisher = results[0].publisher;
						node.author = results[0].author;
						node.pub_date = results[0].pub_date;
						node.twitter = results[0].twitter;
						additional_data.images = results[0].images;
					}
					if (msg.sample)
					{
						for (var key in msg.sample)
						{
							if (msg.sample[key]) node[key] = msg.sample[key];
						}
					}
					var parent;
					if (msg.parent)
					{
						parent = msg.parent;
					}
					else
					{
						parent = sources[CurationFolders._id].getTree();
					}
					chrome.runtime.sendMessage({
						type: "node_for_popup", 
						node: node, 
						exist: false, 
						parent: parent,
						additional_data: additional_data
					});
				}
				if (msg.tab_url && msg.tab_url.indexOf('http') == 0)
				{
					findTabWithUrl(msg.tab_url, function(tab) {
						if (tab)
						{
							chrome.tabs.executeScript(tab.id, {
								file: "js/content.js",
							}, sendResults);
						}
						else
						{
							sendResults();
						}
					});
				}
				else
				{
					sendResults();
				}
			}
			
			break;
		case 'get_additional_data':
			if (!msg.page)
			{
				chrome.runtime.sendMessage({
					type: "additional_data", 
					additional_data: sources[CurationFolders._id].getAdditionalData(msg.id)
				});
			}
			else
			{
				sender.postMessage({
					type: "additional_data", 
					additional_data: sources[CurationFolders._id].getAdditionalDataForNode(msg.id)
				});
			}
			break;
		case 'temp_save_data':
			temp_saved_data = msg.data;
			for (var i in bm_ports) bm_ports[i].postMessage({
				type: "temp_saved_data",
				data: temp_saved_data
			});
			break;
		case 'getAllInAppGoods':
			InAppPayments.getAllGoods(function(goods, error) {
				if (error)
				{
					for (var i in bm_ports) bm_ports[i].postMessage({
						type: "needLogin"
					});
				}
				else
				{
					for (var i in bm_ports) bm_ports[i].postMessage({
						type: "allInAppGoods",
						data: goods
					});
				}
			});
			break;
		case 'buyInAppGoods':
			InAppPayments.buyGoods(msg.data.goods_id, function(goods_id) {
				for (var i in bm_ports) bm_ports[i].postMessage({
					type: "buyInAppGoods",
					data: {
						status: 'ok',
						goods_id: goods_id
					}
				});
				Sync.Api.sendSubscriptions();
			}, function(goods_id) {
				for (var i in bm_ports) bm_ports[i].postMessage({
					type: "buyInAppGoods",
					data: {
						status: 'error',
						goods_id: goods_id
					}
				});
				Sync.Api.sendSubscriptions();
			});
			break;
	}
}

chrome.identity.onSignInChanged.addListener(function() {
	InAppPayments.updateAvailableGoods(function() {
		listener({type: 'getAllInAppGoods'});
	});
	Sync.authorize(function() {
	//	Sync.syncTimeoutDelayFolders = 30000;
		if (Sync.enabled)
		{
			sources[CurationTree._id].startSync();
			sources[CurationFolders._id].startSync();
			Sync.Api.sendSubscriptions();
		}
		else
		{
			sources[CurationTree._id].stopSync();
			sources[CurationFolders._id].stopSync();
		}
	});
});

Sync.Api.sendSubscriptions();

function markBrowserIcon(inCurations, tabId) {
	chrome.browserAction.setIcon({
		path: inCurations ? "icons/icon-32.png" : "icons/icon_black.png",
		tabId: tabId
	});
}

function findTabWithUrl(url, callback) {
	chrome.tabs.getAllInWindow(function(tabs) {
		for (var i = tabs.length - 1; i >= 0; i--)
		{
			if (decodeURIComponent(tabs[i].url) == decodeURIComponent(url))
			{
				callback(tabs[i]);
				return;
			}
		}
		callback(null);
	});
}

function getCurationFileXML(file_id) {
	function clean(str) {
		str = str || '';
		return str.replace(/\&/g, '&amp;').replace(/\"/g, "&quot;").replace(/\'/g, "&apos;").replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
	}
	function clearHTML(text)
	{
		if (!text)
		{
			return "";
		}
		return clean(text.replace(/<script.*\/script>/gi, '').replace(/\<[^\>]+\>/g, ''));
	}

	var file = sources[CurationTree._id].getNodeById(file_id);
	var xml = '<?xml version="1.0" encoding="UTF-8"?>\n';
	xml += '<file>\n<div itemscope="" itemtype="http://schema.org/WebPage">\n' + '\t<span itemprop="about">' + clean(file.text) + '</span>\n' + '\t<span itemprop="comment">' + clean(file.comment) + '</span>\n' + '\t<span itemprop="encoding">' + file.guid + '</span>\n' + '\t<span itemprop="publisher">' + clean(file.publisher) + '</span>\n' + '\t<span itemprop="author">' + clean(file.author) + '</span>\n' + '\t<span itemprop="keywords">' + clean(file.tags) + '</span>\n';

	for (var i in file.children)
	{
		var group = file.children[i];
		xml += '\t<group>\n\t<div itemscope="" itemtype="http://schema.org/WebPageElement">\n' + '\t\t<span itemprop="about">' + clean(group.text) + '</span>\n' + '\t\t<span itemprop="comment">' + clean(group.comment) + '</span>\n' + '\t\t<span itemprop="encoding">' + group.guid + '</span>\n' + '\t\t<span itemprop="keywords">' + clean(group.tags) + '</span>\n';

		for (var j in group.children)
		{
			var page = group.children[j];
			xml += '\t\t<page>\n\t\t<div itemscope="" itemtype="http://schema.org/WebPageElement">\n' + '\t\t\t<span itemprop="about">' + clean(page.text) + '</span>\n' + '\t\t\t<span itemprop="comment">' + clean(page.comment) + '</span>\n' + '\t\t\t<span itemprop="encoding">' + page.guid + '</span>\n' + '\t\t\t<span itemprop="keywords">' + clean(page.tags) + '</span>\n';

			var folder = sources[CurationFolders._id].getNodeById(page.curation_folder_id);
			if (!folder) {
				folder = sources[CurationFolders._id].getNodeBy('guid', page.curation_folder_guid);
				if (!folder) {
					folder = sources[CurationFolders._id].getNodeBy('title', page.title);
				}
			}

			if (folder)
			for (var k in folder.children)
			{
				var link = folder.children[k];
				if (link.type != Types.curation_link.id)
				{
					continue;
				}
				var date = new Date(link.pub_date);
				var type = link.article_type == 'news_article' ? 'NewsArcticle' : 'Article';
				xml += '\t\t\t<link>\n\t\t\t<div itemscope="" itemtype="http://schema.org/' + type + '">\n' + '\t\t\t\t<meta itemscope="" itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="' + clean(link.url) + '"/>\n' + '\t\t\t\t<h2 itemprop="headline">' + clean(link.text) + '</h2>\n' + '\t\t\t\t<h3 itemprop="author" itemscope="" itemtype="https://schema.org/Person">\n' + '\t\t\t\t\tBy <span itemprop="name">' + clean(link.author) + '</span>\n' + '\t\t\t\t</h3>' + '\t\t\t\t<span itemprop="description">' + clearHTML(link.description) + '</span>\n';
				if (link.image_url)
				{
					xml += '\t\t\t\t<div itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">\n' + '\t\t\t\t\t<img src="' + clean(link.image_url) + '"/>\n' + '\t\t\t\t\t<meta itemprop="url" content="' + clean(link.image_url) + '"/>\n' + '\t\t\t\t</div>\n';
				}
				if (link.twitter)
				{
					xml += '\t\t\t\t<meta name="twitter:site" content="' + link.twitter + '">\n';
				}
				xml += '\t\t\t\t<div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">\n' + '\t\t\t\t\t<meta itemprop="name" content="' + clean(link.publisher) + '"/>\n' + '\t\t\t\t</div>' // end of publisher
					+ '\t\t\t\t<meta itemprop="datePublished" content="' + clean(date.toUTCString()) + '"/>\n' + '\t\t\t\t<meta itemprop="dateModified" content="' + clean(date.toUTCString()) + '"/>\n';
				if (link.rating)
				{
					xml += '\t\t\t\t<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">\n' + '\t\t\t\t\t<span itemprop="ratingValue">' + link.rating + '</span>\n' + '\t\t\t\t</div>';
				}
				xml += '\t\t\t\t<span itemprop="keywords">' + clean((link.tags || []).join(',')) + '</span>\n' + '\t\t\t</div>\n\t\t\t</link>\n';
			}

			xml += '\t\t</div>\n\t\t</page>\n';
		}

		xml += '\t</div>\n\t</group>\n';
	}

	xml += '</div>\n</file>';

	return xml;
}

function getCurationLinksJSON(nodes) {
	var result_json = {
		"extensionVersion": chrome.runtime.getManifest().version,
		"data": {}
	};
	function add_node_to_json_with_parents(node) {
		var parent = sources[CurationFolders._id].getNodeById(node.parentId);
		if (!node || !parent || node.parentId == '1') return;
		if (!result_json.data[node.guid])
		{
			var tmp_node = JSON.parse(JSON.stringify(node));
			delete tmp_node.id;
			delete tmp_node.parentId;
			tmp_node.children = [];
			tmp_node.parent = parent.guid;
			result_json.data[node.guid] = tmp_node;
			if (!result_json.data[parent.guid])
			{
				add_node_to_json_with_parents(parent);
			}
			if (result_json.data[parent.guid] &&
				result_json.data[parent.guid].children.indexOf(node.guid) < 0)
			{
				result_json.data[parent.guid].children.push(node.guid);
			}
		}
    }
	function add_node_to_json_without_parents(node) {
		if (!result_json.data[node.guid])
		{
			var tmp_node = JSON.parse(JSON.stringify(node));
			delete tmp_node.id;
			delete tmp_node.parentId;
			tmp_node.children = [];
			result_json.data[node.guid] = tmp_node;
		}
    }
	function add_node_childrens_to_json(node) {
		for (var i in node.children)
		{
			if (!result_json.data[node.children[i].guid])
			{
				var tmp_node = JSON.parse(JSON.stringify(node.children[i]));
				delete tmp_node.id;
				delete tmp_node.parentId;
				tmp_node.children = [];
				tmp_node.parent = node.guid;
				result_json.data[tmp_node.guid] = tmp_node;
				result_json.data[node.guid].children.push(tmp_node.guid);
				add_node_childrens_to_json(node.children[i]);
			}
		}
	}
	if (Array.isArray(nodes))
	{
		if (nodes.length == 1 && nodes[0].parentId == '1')
		{
			var tmp_nodes = [];
			var nodes = sources[CurationFolders._id].getNodeById(nodes[0].id)
			for (var ch in nodes.children)
			{
				tmp_nodes.push(nodes.children[ch]);
			}
			nodes = tmp_nodes;
		}
		for (var i = 0; i < nodes.length; i++)
		{
			nodes[i] = sources[CurationFolders._id].getNodeById(nodes[i].id);
			add_node_to_json_without_parents(nodes[i]);
			add_node_childrens_to_json(nodes[i]);
		}
	}
	
	return JSON.stringify(result_json);
}

function getFirstFolderWithoutSubfolders(nodes) {
	function removeSubfolders(node) {
		var tmp_children = [];
		for (var i = 0; i < node.children.length; i++)
		{
			if (node.children[i].type == Types.curation_link.id || node.children[i].type == Types.web_link.id)
			{
				tmp_children.push(node.children[i]);
			}
		}
		node.children = tmp_children;
		return node;
	}
	
	for (var i = 0; i < nodes.length; i++)
	{
		if (nodes[i].type == Types.curation_link.id || nodes[i].type == Types.web_link.id)
		{
			return removeSubfolders({children: nodes}).children;
		}
	}
	
	if (nodes.length > 0)
	{
		if (nodes.length == 1 && nodes[0].title == CurationFolders.title_for_bookmarks && nodes[0].children.length > 0)
		{
			return [removeSubfolders(nodes[0].children[0])];
		}
		else
		{
			return [removeSubfolders(nodes[0])];
		}
	}
	else
	{
		return [];
	}
}

function generateCurationTreeFromJSON(data) {
	var tree = [];
	
	function findChilds(parent) {
		delete parent.parent;
		delete parent.index;
		if (parent.children && parent.children.length)
		{
			for (var i = 0; i < parent.children.length; i++)
			{
				if (data[parent.children[i]])
				{
					parent.children[i] = data[parent.children[i]];
					findChilds(parent.children[i]);
				}
			}
		}
	}
	
	for (var i in data)
	{
		if (!data[i].parent || !data[data[i].parent])
		{
			data[i].parentId = CurationFolders.curation_toolbar_id;
			tree.push(data[i]);
		}
	}
	
	for (var i = 0; i < tree.length; i++)
	{
		findChilds(tree[i]);
	}
	
	return tree;
}