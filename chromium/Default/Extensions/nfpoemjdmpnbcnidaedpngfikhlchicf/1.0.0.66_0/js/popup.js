var hash;
var curation_folders;
var current_tab_url;
var tagsCustomControl;

function showPanel(id) {
	$('.panel').hide();
	$('#' + id).show();
}

function getCurationCounts(selectedId) {
	var folders_count = 0;
	var links_in_selected_folder = 0;
	function loop(node)
	{
		if (node.type == Types.curation_folder.id)
		{
			folders_count++;
		}
		else if (node.type == Types.curation_link.id && node.parentId == selectedId)
		{
			links_in_selected_folder++;
		}
		if (node.children) for (var i in node.children)
		{
			loop(node.children[i]);
		}
	}
	
	curation_folders && loop(curation_folders);
	return {
		folders_count: folders_count,
		links_in_selected_folder: links_in_selected_folder
	};
}

function fillParentSelect(selectDom, node, parentId) {
	function add_item(prefix, node_item) {
		if (node_item.type != Types.web_link.id && node_item.type != Types.curation_link.id)
		{
			selectDom.append($('<option value="' + node_item.id + '" ' + (node_item.type == Types.placeholder_folder.id ? 'disabled' : '') + ' data_text="' + prefix + node_item.title + '">' + prefix + node_item.title + '</option>'));
			for (var i in node_item.children)
			{
				add_item(prefix + '&nbsp;&nbsp;&nbsp;&nbsp;', node_item.children[i]);
			}
		}
	}
	
	add_item('', node);
	if (parentId)
	{
		selectDom.val(parentId);
	}
	else if (hash && hash.parentId)
	{
		selectDom.val(hash.parentId);
	}
		
	$('#_parent_list').text($("#parent_list option:selected").text().trim());
}

function objToMas(obj) {
	var res = [];
	for (var key in obj)
	{
		res.push(key.trim());
	}
	
	res.sort(function(a, b) {
		return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
	});
	return res;
}
function objTagsToMas(obj) {
	var res = [];
	for (var key in obj)
	{
		var new_item = key.toLowerCase();
		if (res.indexOf(new_item) < 0)
		{
			res.push(new_item);
		}
	}
	
	res.sort();
	return res;
}

function setImages(images, selected) {
	$('#images .panel-body').html('');
	if (Array.isArray(images))
	{
		for (var i = 0; i < images.length && i < 10; i++)
		{
			if (!images[i])
			{
				continue;
			}
			image_dom = $('<img src="' + images[i] + '">');
			if (images[i] == selected)
			{
				image_dom.addClass('selected');
			}
			image_dom.on('load', function() {
				if (this.naturalWidth < 49 || this.naturalHeight < 49)
				{
					$(this).remove();
					if (!$('#images img').length)
					{
						$('#images').hide();
					}
				}
			});
			$('#images .panel-body').append(image_dom);
		}
	}
	$('#images img').on('click', function() {
		$('#images img.selected').removeClass('selected');
		$(this).addClass('selected');
		$('#image_url').val($(this).attr('src'));
		$('#images').hide();
	});
	$('#images').hide();
}

function scanImages(url) {
	
	function getMetaTag(dom, selectors)
	{
		var e;
		for (var i = 0; i < selectors.length; i++)
		{
			e = dom.querySelector(selectors[i]);
			if (e)
			{
				switch (e.nodeName)
				{
					case "META":
						return e.getAttribute('content') || e.getAttribute('value');
						break;
					case "TITLE":
						return e.textContent;
						break;
					default:
						return e.textContent;
						break;
				}
			}
		}
		return null;
	}

	function filterImages(dom) {
		var list = dom.querySelectorAll('img');
		var unique = [];
		for (var i = 0; i < list.length; i++)
		{
			var k = list[i];
			var u = true;
			for (var j = 0; j < unique.length; j++)
			{
				if (unique[j].src == k.src)
				{
					u = false;
					break;
				}
			}
			if (u)
			{
				unique.push(k);
			}
		}
		if (unique.length == 0)
		{
			return unique;
		}
		unique.sort(function(a, b) { 
			if (a.className == b.className)
			{
				return 0;
			}
			return a.className > b.className ? -1 : 1; 
		});
		var i = 1;
		var result = [];
		var rest = [];
		result.push(unique[0]);
		while (i < unique.length)
		{
			if (unique[i - 1].className != unique[i].className)
			{
				result.push(unique[i]);
			}
			i++;
		}
		return result;
	}

	function normalizeUrl(url, scheme, prepath) {
		if (/^\/\//.test(url))
		{
			return scheme + ':' + url;
		}
		if (/^\/[^\/]/.test(url))
		{
			return prepath + url;
		}
		return url;
	}

	function parsePage(dom, url_scheme, url_prepath)
	{
		var primaryImage = getMetaTag(dom, ['*[itemscope] *[itemprop="image"]', 'meta[property="og:image"]', 'meta[property="twitter:image"]']);
		var images = [];
		var imgs = filterImages(dom);

		for (var i = 0; i < imgs.length; i++)
		{
			images.push(normalizeUrl(imgs[i].getAttribute('src'), url_scheme, url_prepath));
		}
		if (primaryImage)
		{
			primaryImage = normalizeUrl(primaryImage, url_scheme, url_prepath);
			if (images.indexOf(primaryImage) > 0)
			{
				images.splice(images.indexOf(primaryImage), 1);
			}
			images.unshift(primaryImage);
		}
		
		return images;
	}

	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.onload = function() {
		var parser = new DOMParser();
  		var doc = parser.parseFromString(xhr.response, "text/html");
		var scheme = url.match(/^https?/);
		if (scheme && scheme.length)
		{
			scheme = scheme[0];
		}
		else
		{
			scheme = '';
		}
		var prepath = url.match(/^https?:\/\/[^\/]*/);
		if (prepath && prepath.length)
		{
			prepath = prepath[0];
		}
		else
		{
			prepath = '';
		}
		var images = parsePage(doc, scheme, prepath);
		setImages(images, $('#image_url').val());
		if ($('#images img').length)
		{
			$('#images').show();
		}
		$('#images_scan').attr('disabled', false);
	};
	xhr.onerror = function() {
		$('#images_scan').attr('disabled', false);
	}
	xhr.send();
}

function validUrl(url) {
	if (/^(https?|ftp|file|ws|wss|chrome):\/\//.test(url))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function addTag(tag) {
	if (!tag)
	{
		tag = $('#tags').val();
	}
	if (tag)
	{
        tag = tag.toLowerCase();
		var exist = $('.tag[tag_data="' + tag + '"]');
		if (!exist.length)
		{
			var tag_dom = $('<div class="tag" tag_data="' + tag + '">' + tag + '</div>').on('click', function() {
				tagsCustomControl.add($(this).text());
				$(this).remove();
				saveTmpData();
			})
			$('#tags').parent().before(tag_dom);
		}
		setTimeout(function() {
			$('#tags').val('');
		});
		tagsCustomControl.remove(tag);
	}
}

function setAdditionalInfo(parentId, info) {
	if (parentId === undefined)
	{
		if (hash && hash.parentId)
		{
			parentId = hash.parentId;
		}
	}
	
	selectInputControl($('#publisher'), objToMas(info.publishers[parentId]), function() {});
	selectInputControl($('#author'), objToMas(info.authors[parentId]), function(val) {
		if (info.authors[parentId])
		{
			var tw = info.authors[parentId][val];
			if (typeof tw == 'string')
			{
				$('#twitter').val(tw);
			}
		}
	});
	selectInputControl($('#twitter'), objToMas(info.twitters[parentId]), function() {});
	tagsCustomControl = selectInputControl($('#tags'), objTagsToMas(info.tags[parentId]), function() {}, function(val, select) {
		addTag(val);
		select.val(-1);
		saveTmpData();
	});
	$('#tags').on('keydown', function(e) {
		if (e.keyCode == 13 || e.keyCode == 9)
		{
			addTag();
			saveTmpData();
			if (e.keyCode == 9)
			{
				e.preventDefault();
			}
		}
	}).on('mousedown', function(e) {
		$(this).focus();
	});
}

function edit(node, type, info, parentList) {
	// -- title
	node.title && $('#title').val(node.title); $('#title').on('change', saveTmpData);
	node.text && $('#title').val(node.text);
	// -- parents list
	curation_folders = parentList;
	$('#parent_list').on('change', function() {
		$('#_parent_list').text($("#parent_list option:selected").text().trim());
		setAdditionalInfo($('#parent_list').val(), info);
		saveTmpData();
	});
	if (parentList)
	{
		fillParentSelect($('#parent_list'), parentList, node.parentId);
	}
	else
	{
		$('#parent_list').hide();
		$('#_parent_list').text(info.some_node.title);
	}// : $('#parent_list').closest(".row").hide();
	setAdditionalInfo(node.parentId || $('#parent_list').val(), info);
	// -- url
	node.url && ($('#url').val(node.url)); $('#url').on('change', saveTmpData);
	// -- image url
	node.image_url && $('#image_url').val(node.image_url); $('#image_url').on('change', saveTmpData);
	setImages(info.images, node.image_url);
	$('#image_url').on('focus', function() {
		if ($('#images img').length)
		{
			$('#images').show();
		}
	});
	$(document).on('click', function(e) {
		if (!$(e.target).is('#images, #image_url'))
		{
			$('#images').hide();
		}
	});
	$('#images_scan').on('click', function(e) {
		var url = $('#url').val();
		if (url.indexOf("http") == 0)
		{
			scanImages(url);
			$('#images_scan').attr('disabled', true);
		}
	});
	// -- description
	function makeLinksClickable() {
		var newlinks = $('.nicEdit-main').find('a:not(.markered)');
		newlinks.addClass('markered');
		newlinks.on('click', function() {
			window.open(this.href);
		});
	}
	
	node.description && $('#description').val(node.description);
	var description_editor = new nicEditor({maxHeight : 100}).panelInstance('description');
	description_editor.addEvent('blur', saveTmpData);
	var row = $('#description').closest('.row');
	row.find('.data>div')[1].classList.add('description_second_div');
	row.find('.data>div')[1].setAttribute('style', '');
	
	var observer = new MutationObserver(makeLinksClickable);
	observer.observe($('.nicEdit-main')[0], {childList: true});
	
	$('.nicEdit-main').on('paste', function(e) {
		var text = e.originalEvent.clipboardData.getData('text/plain');
		if (text)
		{
			e.originalEvent.preventDefault();
			document.execCommand("insertHTML", false, text);
		}
		makeLinksClickable();
	});
	makeLinksClickable();
	// -- publisher
	node.publisher && $('#publisher').val(node.publisher.trim());$('#publisher').change(); $('#publisher').on('change', saveTmpData);
	// -- author
	node.author && $('#author').val(node.author.trim());$('#author').change(); $('#author').on('change', saveTmpData);
	// -- pub. date
	$('#pub_date').datepicker({
		dateFormat: "m/d/yy"
	});
	if (node.pub_date)
	{
		$('#pub_date').datepicker("setDate", new Date(node.pub_date));
	}
	else
	{
		$('#pub_date').datepicker("setDate", new Date());
	}
	$('#pub_date').on('change', saveTmpData);
	// -- article type
	node.article_type && $('#article_type').val(node.article_type); $('#article_type').on('change', saveTmpData);
	// -- twitter
	function twitterFormat() {
		var val = $('#twitter').val() || '';
		val = val.replace(/[^a-zA-Z0-9_]/ig, '');
		if (!val.length || val[0] != '@')
		{
			val = '@' + val;
		}
		val = val.substr(0, 16);
		$('#twitter').val(val);
	}
	node.twitter && $('#twitter').val(node.twitter.trim()); 
	$('#twitter').change(); 
	$('#twitter').on('keyup', twitterFormat);
	$('#twitter').on('change', saveTmpData);
	twitterFormat();
	// -- tags
	if (typeof node.tags == 'string')
	{
		node.tags = node.tags.split(',');
	}
	if (node.tags)
	{
		for (var i = 0; i < node.tags.length; i++)
		{
			addTag(node.tags[i]);
		}
	}
	// -- rating
	$('#rating').val('');
	node.rating && $('#rating').val(node.rating); $('#rating').on('change', saveTmpData);

	$('.ui-autocomplete-input').focus(function() {
		$(this).data("uiAutocomplete").search($(this).val());
	});
	

	showPanel('edit');
	
	var startWindowHeight = $(window).height();
	var startHeight = $('#description').parent().find('.description_second_div').height();
	function resize(e) {
		var deltaH = $(window).height() - startWindowHeight;
		var row = $('#description').closest('.row');
		if (startHeight + deltaH > 50)
		{
			row.find('.nicEdit-main').attr('style', 'min-height:' + (startHeight + deltaH - 8) + 'px !important;margin: 4px;overflow: hidden;');
			row.find('.description_second_div').attr('style', 'max-height: ' + (startHeight + deltaH + 1) + 'px !important');
		}
	}
	$(window).on('resize', resize);
	
	if (window == window.top)
	{
		$('body').attr('style', 'width:800px; height: 600px');
	}
	
	$('#save').on('click', function() {
	//	Payment.getPaid(function() {
			var can_save = true;
			$('.error_mess').remove();
			$('.has-error').removeClass('has-error');
			// -- title
			node.title = $('#title').val();
			node.text = node.title;
			// -- parents list
			if ($('#parent_list').is(":visible"))
			{
				node.parentId = $('#parent_list').val();
				var counts = getCurationCounts(node.parentId);
				if (Payment.config.free_links_per_folder >= 0 && !Payment.paid)
				{
					if (counts.links_in_selected_folder >= Payment.config.free_links_per_folder)
					{
						$('#save').closest('.row').before($('<div class="row error_mess">' +
							Local['links_count_error'] +	
						'</div>'));
						can_save = false;
					}
				}
				if (!Payment.paid && counts.folders_count > Payment.config.free_folders_count)
				{
					$('#save').closest('.row').before($('<div class="row error_mess">' +
						Local['subscriptionError'] +	
					'</div>'));
					can_save = false;
					return;
				}
				if (!node.parentId)
				{
					$('#parent_list').closest('.form-group').addClass('has-error');
					can_save = false;
				}
			}
			// -- url
			node.url = $('#url').val();
			if (!validUrl(node.url))
			{
				$('#url').closest('.form-group').addClass('has-error');
				can_save = false;
			}
			// -- image url
			node.image_url = $('#image_url').val();
			// -- description
			node.description = description_editor.instanceById('description').getContent();
			// -- publisher
			node.publisher = $('#publisher').val();
			// -- author
			node.author = $('#author').val();
			// -- pub. date
			node.pub_date = new Date($('#pub_date').val()).getTime();
			// -- article type
			node.article_type = $('#article_type').val();
			// -- twitter
			node.twitter = $('#twitter').val();
			// -- tags
			var tags = [];
			$('#tags').parent().parent().find('.tag').each(function(i, el) {
				tags.push($(el).text());
			});
			var not_saved_tag = $('input#tags').val();
			not_saved_tag && tags.push(not_saved_tag);
			node.tags = tags;
			// -- rating
			node.rating = $('#rating').val();
			node.type = Types.curation_link.id;
			if (can_save)
			{
				chrome.runtime.sendMessage(null, {
					type: type, 
					data: {
						source_id: node.source_id,
						node: node,
						pos: (hash && hash.sample_node) ? hash.sample_node.pos : undefined
					}
				});
				window.close();
				window.top.postMessage("done", "*");
				window.top.postMessage("close", "*");
				if (window.top == window) localStorage["last_parent_id"] = node.parentId;
			}
	//	});
	});

	$('#save').text(Local['save']);
	$('#remove').text(Local['cancel']);
	
	$('#remove').on('click', function() {
	/*	chrome.runtime.sendMessage(null, {
			type: "remove", 
			data: {
				source_id: node.source_id,
				node: node
			}
		});*/
		window.close();
		window.top.postMessage("close", "*");
	});
}

function saveTmpData() {
	var tmp_data = {};
	// -- title
	tmp_data.title = $('#title').val();
	tmp_data.text = $('#title').val();
	// -- parents list
	if ($('#parent_list').is(":visible"))
	{
		tmp_data.parentId = $('#parent_list').val();
	}
	// -- url
	tmp_data.url = $('#url').val();
	// -- image url
	tmp_data.image_url = $('#image_url').val();
	// -- description
	tmp_data.description = nicEditors.findEditor('description').getContent();
	// -- publisher
	tmp_data.publisher = $('#publisher').val();
	// -- author
	tmp_data.author = $('#author').val();
	// -- pub. date
	tmp_data.pub_date = new Date($('#pub_date').val()).getTime();
	// -- article type
	tmp_data.article_type = $('#article_type').val();
	// -- twitter
	tmp_data.twitter = $('#twitter').val();
	// -- tags
	var tags = [];
	$('#tags').parent().parent().find('.tag').each(function(i, el) {
		tags.push($(el).text());
	});
	var not_saved_tag = $('input#tags').val();
	not_saved_tag && tags.push(not_saved_tag);
	tmp_data.tags = tags;
	// -- rating
	tmp_data.rating = $('#rating').val();
	tmp_data.type = Types.curation_link.id;
	
	if (current_tab_url)
	{
		localStorage[current_tab_url] = JSON.stringify(tmp_data);
	}
}

$(document).ready(function() {
	if (window != window.top)
	{
		$('body').attr('style', 'width:100%');
	}
	else 
	{
		$('body>.form-horizontal').attr('style', 'height:575px;overflow-y:auto;overflow-x:hidden');
		$('body>.form-horizontal>fieldset').attr('style', 'width: calc(100% - 20px);');
	}
	$('#title').closest('.row').find('.title').text(Local["headline"]);
	$('#parent_list').closest('.row').find('.title').text(Local["folder"]);
	$('#url').closest('.row').find('.title').text(Local["url"]);
	$('#image_url').closest('.row').find('.title').text(Local["image_url"]);
	$('#images_scan').text(Local["scan_images"]);
	$('#description').closest('.row').find('.title').text(Local["description"]);
	$('.publisher').text(Local["publisher"]);
	$('.author').text(Local["author"]);
	$('.pub_date').text(Local["pub_date"]);
	$('.article_type').text(Local["type"]);
	$('.twitter').text(Local["twitter"]);
	$('#tags').closest('.row').find('.title').text(Local["tags"]);
	$('.rating').text(Local["rating"]);
	$('#remove').text(Local["remove"]);
	$('#save').text(Local["save"]);
	
	if (document.location.hash)
	{
		hash = document.location.hash.substring(1);
		try
		{
			hash = JSON.parse(decodeURIComponent(window.atob(hash)));
		}
		catch (e) { }
	}
	
	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		switch (message.type)
		{
			case "node_for_popup":
				if (message.exist)
				{
					edit(message.node, 'change', message.additional_data);
				}
				else
				{
					edit(message.node, 'create', message.additional_data, message.parent);
				}
				break;
			case "additional_data":
				if (hash.node)
				{
					edit(hash.node, 'change', message.additional_data);
				}
				break;
		}
	});
	
	setTimeout(function() {
		if (!hash)
		{
			chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
				if (tabs.length)
				{
					current_tab_url = tabs[0].url;
					var tmp_data = localStorage[current_tab_url];
					if (tmp_data)
					{
						try {
							tmp_data = JSON.parse(tmp_data);
						} catch(e) {
							tmp_data = {}
						}
					}
					else
					{
						tmp_data = {};
					}
					localStorage["last_parent_id"] && (tmp_data.parentId = localStorage["last_parent_id"]);
					chrome.runtime.sendMessage(null, {
						type: 'get_node_for_popup',
						tab_url: current_tab_url,
						sample: tmp_data
					});
				}
			});
		}
		else
		{
			if (hash.node)
			{
				chrome.runtime.sendMessage(null, {
					type: 'get_additional_data',
					id: hash.node.parentId
				});
			}
			else
			{
				chrome.runtime.sendMessage(null, {
					type: 'get_node_for_popup',
					tab_url: hash.url,
					sample: hash.sample_node,
					new: hash.new
				});
			}
		}
	}, 100);
	
	showPanel('loading');
});
