
var activeVideos = {};
var lastUrl;

// Add context menu item
chrome.runtime.onInstalled.addListener(function() {
  var title = "Play Picture-in-Picture";
  var id = chrome.contextMenus.create({
    "title": title,
    "contexts": ["link"],
    "id": "ytpip",
    "targetUrlPatterns": ["https://*.youtube.com/watch?v=*"]
  });
});

chrome.contextMenus.onClicked.addListener(onClickHandler);

function onClickHandler(info, tab) {
  var url = info.linkUrl;
  // console.log("BG: Context Menu: "+url);
  activeVideos[tab.id] = getVideoId(url);
  // Pass the YouTube video ID to the content script
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      {
        message: "start_pip",
        videoId: activeVideos[tab.id]
      }
    );
  });
}

// Listen for messages from the content script
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    switch(request.message) {
      case "video_closed":
        // Unsent currentVideoId because the video was closed
        // console.log("BG: Current video closed");
        activeVideos[sender.tab.id] = undefined;
        break;
      case "page_unload":
        // Reset this tab because page was reloaded or navigated away from
        // console.log("BG: Page unloaded / session clear");
        activeVideos[sender.tab.id] = undefined;
        break;
      default:
        // Pass
    }
  }
);

// Listen for when a Tab changes state.
chrome.webNavigation.onHistoryStateUpdated.addListener(function(details){
  var url = details.url;
  var tabId = details.tabId;
  // console.log("BG: ---------- History State Updated ---------");
  // console.log("BG: Last URL: "+lastUrl);
  // console.log("BG: New URL: " + url);
  // console.log("BG: Last Video ID: " + activeVideos[tabId]);
  // console.log("BG: Tab: " + tabId);

  // TODO: Address reloads

  if ((isVideoPage(url) || isTimeContinuePage(url)) && !(isTimeContinuePage(lastUrl))) {
    // console.log("BG: Video page");
    var thisVideoId;
    var startTime = 0;

    if (isTimeContinuePage(url)) {
      var options = getTimeUrlOptions(url);
      thisVideoId = options.videoId;
      startTime = options.startTime;
    } else {
      thisVideoId = getVideoId(url);
    }

    // console.log("BG: Start time: "+startTime);

    // Either no video is playing or the currently playing video
    // is different from the one whose page was just opened,
    // so close the PIP and start the new video inline in the page
    if (!activeVideos[tabId] || activeVideos[tabId] != thisVideoId) {
      // console.log("BG: Starting Inline player");
      chrome.tabs.sendMessage(
        tabId,
        {
          message: "start_inline",
          videoId: thisVideoId,
          startTime: startTime
        },
        function(response) {
          if (response && response.status == "inline_player_started") {
            // console.log("BG: Video ID updated: " + thisVideoId);
            activeVideos[tabId] = thisVideoId;
          }
        }
      );
    } else {
      if (!isVideoPage(lastUrl)) {
        // A video is playing and it is the same video, so just expand
        // the PIP back into the inline player without disrupting playback
        // TODO
        // console.log("BG: Same video");
        chrome.tabs.sendMessage(
          tabId,
          {
            message: "continue_inline"
          }
        );
      }

    }
    /*
    // Listen for scrolling, since a video page is open
    // console.log("BG: Scroll on");
    chrome.tabs.sendMessage(
      tabId,
      {
        message: "scroll_on"
      }
    );
    */

  } else {
    // console.log("BG: Non-video page");
    /*
    // Always turn off scroll detection
    // console.log("BG: Scroll off");
    chrome.tabs.sendMessage(
      tabId,
      {
        message: "scroll_off"
      }
    );
    */
    // Shrink current video and keep playing if a video is playing
    if (isVideoPage(lastUrl) && activeVideos[tabId]) {
      // console.log("BG: Continuing playback");
      chrome.tabs.sendMessage(
        tabId,
        {
          message: "make_pip"
        }
      );
    }

  }

  lastUrl = url;

});

var re = /^(https:\/\/(www\.)?youtube\.com\/watch\?v=)([\w-]+)(&)*/;
/**
 * @param {string} url A well-formed YouTube video url
 * @return {string} The ID of the YouTube video
 */
function getVideoId(url) {
  // $1 == https://www.youtube.com/watch?v=
  // $2 == www.
  // $3 == [The video ID]
  var results = url.match(re);
  // console.log("BG: getVideoId: " + results[3]);
  return results[3];
}

/**
 * @param {string} url Any URLl
 * @return {boolean} True if it is a YouTube video page
 */
function isVideoPage(url) {
  return re.test(url);
}

var timere = /^(https:\/\/(www\.)?youtube\.com\/watch\?time_continue=)([\d]+)(&v=)([\w-]+)(&)*/;
/**
 * @param {string} url A well-formed YouTube time continue URL
 * @return {string} The timestamp at which the video should continue
 */
function getTimeUrlOptions(timeContinueUrl) {
  // $1 == https://www.youtube.com/watch?v=
  // $2 == www.
  // $3 == [The timestamp]
  var results = timeContinueUrl.match(timere);
  // console.log("BG: getVideoId: " + results[3]);
  return {
    startTime: results[3],
    videoId: results[5]
  };
}
/**
 * @param {string} url Any URLl
 * @return {boolean} True if it is a page that continues a video from a given time
 */
function isTimeContinuePage(url) {
  return timere.test(url);
}
