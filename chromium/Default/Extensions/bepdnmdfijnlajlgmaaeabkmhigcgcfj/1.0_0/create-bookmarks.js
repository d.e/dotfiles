BOOKMARKS = [
  {
    'title': '小编推荐应用',
    'url': 'http://www.google.cn/chrome/intl/zh-CN/apps.html'
  }, {
    'title': '常用网站',
    'children': [
      {
        'title': '轻松上手',
        'url': 'http://www.google.com/chrome/intl/zh-CN/welcome.html'
      }, {
        'title': '淘宝网 - 淘！我喜欢',
        'url': 'http://www.taobao.com/'
      }, {
        'title': '淘宝商城正式更名天猫,品质共享为你而变 tmall.com',
        'url': 'http://www.tmall.com/'
      }, {
        'title': '京东网上商城-综合网购首选，正品行货，机打发票，售后上门取件，省钱又放心',
        'url': 'http://www.360buy.com/'
      }, {
        'title': '当当网—网上购物中心：图书、母婴、美妆、家居、数码、家电、服装、鞋包等，正品低价，货到付款',
        'url': 'http://dangdang.com/'
      }, {
        'title': '太平洋电脑网_中国第一专业IT门户网站',
        'url': 'http://www.pconline.com.cn/'
      }, {
        'title': '中关村在线 - 大中华区最具商业价值的IT专业网站 - The most valuable and professional IT business website in Greater China',
        'url': 'http://www.zol.com.cn/'
      }, {
        'title': '大众点评网_美食，生活，优惠券',
        'url': 'http://www.dianping.com/citylist'
      }, {
        'title': '北京美食，北京餐厅，好吃又便宜的美食餐厅预订尽在饭统网',
        'url': 'http://www.fantong.com/beijing/'
      }, {
        'title': '163网易免费邮--中文邮箱第一品牌',
        'url': 'http://mail.163.com/'
      }, {
        'title': 'Gmail：来自 Google 的电子邮件',
        'url': 'http://www.gmail.com/'
      }, {
        'title': 'Hotmail 登录',
        'url': 'http://www.hotmail.com/'
      }, {
        'title': '优酷-中国第一视频网站,提供视频播放,视频发布,视频搜索 - 优酷视频',
        'url': 'http://www.youku.com/'
      }, {
        'title': '土豆网_每个人都是生活的导演_在线视频观看,原创视频上传,海量视频搜索',
        'url': 'http://www.tudou.com/'
      }, {
        'title': '265上网导航 - 最多中国人使用的电脑主页',
        'url': 'http://www.265.com/'
      }, {
        'title': '网易',
        'url': 'http://www.163.com/'
      }, {
        'title': '搜狐-中国最大的门户网站',
        'url': 'http://www.sohu.com/'
      }, {
        'title': '新浪首页',
        'url': 'http://www.sina.com.cn/'
      }, {
        'title': '新浪微博-随时随地分享身边的新鲜事儿',
        'url': 'http://weibo.com/'
      }, {
        'title': '腾讯首页',
        'url': 'http://www.qq.com/'
      }
    ]
  }
];

var createBookmark = function(parentId, bookmark) {
  chrome.bookmarks.getChildren(parentId, function(nodes) {
    var nodeId = null;
    for (var i = 0; i < nodes.length; i++) {
      var node = nodes[i];
      if (node.title == bookmark.title &&
          ((!node.url && (!bookmark.url || bookmakr.url == '')) ||
           node.url == bookmark.url)) {
        nodeId = node.id;
        break;
      }
    }
    if (!nodeId) {
      chrome.bookmarks.create({
        'parentId': parentId,
        'title': bookmark.title,
        'url': bookmark.url
      }, function(node) {
        if (bookmark.children) {
          for (var i = 0; i < bookmark.children.length; i++) {
            child = bookmark.children[i];
            createBookmark(node.id, child);
          }
        }
      });
    }
  });
};

for (var i = 0; i < BOOKMARKS.length; i++) {
  var bookmark = BOOKMARKS[i];
  createBookmark('1', bookmark);
}
