//Code to send instrumentation ping when extension installed.
var pingURL;
chrome.runtime.onInstalled.addListener(function () {
    //console.log("onInstalled");    
});
chrome.management.onEnabled.addListener(function (ExtensionInfo) {
    //console.log("onEnabled");    
    if(ExtensionInfo.id != chrome.runtime.id)
	return;
    if (!localStorage["BingDefaultsSet"]) {
        localStorage["BingDefaultsSet"] = "done";

	//alert('Public: Rewards Extension');
	chrome.management.getAll(function(info) {
		var appCount = 0;
		for (var i = 0; i < info.length; i++) {
		//alert('Public: ' + info[i].shortName + '        ' + chrome.runtime.id);
		  if (info[i].shortName == "Bing Rewards" && info[i].id!=chrome.runtime.id) {
			//alert('Public: Found Another extension!');
			chrome.management.setEnabled(info[i].id, false);
		  }
		}
	});
    }
});

